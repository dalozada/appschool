import { AbstractControl} from '@angular/forms';

export class MyValidators {
  static isDavid(control: AbstractControl) {
  const value = control.value;
  if (value !== 'david') {
    console.log(value);
    return { isDavid: true };
  }
  return null;
}
}
