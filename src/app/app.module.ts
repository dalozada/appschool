import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { MyDatePickerModule } from 'mydatepicker';


import { P404Component } from './error/404.component';
import { P500Component } from './error/500.component';
// Import routing module
import { AppRoutingModule } from './app.routing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JornadaTabComponent } from './config-pensum/jornada-tab/jornada-tab.component';




@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule,
    MyDatePickerModule,
  ],
  declarations: [
     AppComponent,
    P404Component,
    P500Component,
    JornadaTabComponent,



  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
