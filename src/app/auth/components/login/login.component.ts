import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, EmailValidator} from '@angular/forms';
import { AuthService } from './../../services/auth.service';
import { UserModel } from './../../models/user.model';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  user: UserModel;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
    ) {
      this.buildFormLogin();
     }

  ngOnInit(): void {
  }

  private buildFormLogin() {
    this.formLogin = this.formBuilder.group({
      usuario: ['', [ Validators.required ]],
      password: ['', [ Validators.required ]]
    });
  }
  get usuarioField() {
    return this.formLogin.get('usuario');
  }
  get passwordField() {
    return this.formLogin.get('password');
  }
  onLogin() {
     if (this.formLogin.valid) {

      this.user = {
        usuario: this.formLogin.value.usuario,
        contrasena: this.formLogin.value.password,
      };
       this.authService.login(this.user.usuario, this.user.contrasena)
       .subscribe(data => {
         console.log(data);
           this.router.navigate(['/admin/home']);
        },
         error => console.log(error)
         // this.openMessage(error.message);
        );
     }
    }
}
