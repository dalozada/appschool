import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { UserModel } from './../models/user.model';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userModel: UserModel;
  url = 'http://localhost:8080/virtualt/login';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
       'Access-Control-Allow-Origin': '*',
    })
  };

  constructor(
    private httpClient: HttpClient
    ) { }


  login(user: string, password: string)  {
    const newUser: UserModel = {
      usuario: user,
      contrasena: password
    };
    console.log(newUser);
    return this.httpClient.post(this.url, newUser, this.httpOptions)
    .pipe(map(data => data));
  }

}
