import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { TipoACtividadModel } from '../../models/tipoActividad.model';
import { TipoActividadService } from '../../services/tipo-actividad.service';
import { debounceTime } from 'rxjs/operators';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-tipo-actividad',
  templateUrl: './tipo-actividad.component.html',
  styleUrls: ['./tipo-actividad.component.css']
})
export class TipoActividadComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formTipoA: FormGroup;
  modalRef: BsModalRef;
  tipoActividad: TipoACtividadModel;
  objTipoA: TipoACtividadModel[] = [];
  alerts: any[] = [];
  tipoIdUpdate: number;

  constructor(
    private tipoActividadService: TipoActividadService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
  ) {
    this.tipoActividad = {
      id: null,
      tipoActividad: '',
      descripcion: '',
    };
    this.buildForm();
  }

  ngOnInit(): void {
    this.tipoActividadService.traerTipoActividad()
      .subscribe(tipoA => {
        this.objTipoA = tipoA;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }
  get nameTipoAField() {
    return this.formTipoA.get('tipoActividad');
  }
  get descripcionTipo() {
    return this.formTipoA.get('descripcion');
  }
  openModalTipoA() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
  closeModal() {
    this.formTipoA.reset();
    this.modalRef.hide();

  }
  openModalTipoAUpdate(tipoAObj: TipoACtividadModel) {
    this.tipoActividad = {
      id: tipoAObj.id,
      tipoActividad: tipoAObj.tipoActividad.toUpperCase(),
      descripcion: tipoAObj.descripcion,
    };
    this.tipoIdUpdate = tipoAObj.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formTipoA = this.formBuilder.group({
      id: [0],
      tipoActividad: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
    });
    this.formTipoA.valueChanges
      .pipe(
        debounceTime(350),
      )
      .subscribe(data => {
      });
  }

  guardarTipoContrato(tipoA: TipoACtividadModel) {

    // const pos = this.objTipoA.indexOf(tipoA);
    const pos = this.objTipoA.indexOf(this.objTipoA.find( tip =>tip.id === this.tipoIdUpdate));

    if (this.formTipoA.valid) {
      if (this.tipoActividad.id === null) {

        this.tipoActividadService.crearTipoActividad(this.formTipoA.value)
          .subscribe(newTipoA => {
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! El registro fue creado con éxito.',
              timeout: 5000,
              msgStr: 'Proceso Exitoso!'
            });
            this.formTipoA.reset();
            this.objTipoA.push(newTipoA);
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser creado.',
                timeout: 5000,
                msgStr: 'Ups!'
              });
            });

      } else {
        this.tipoActividadService.actualizarTipoActividad(this.tipoActividad)
          .subscribe(tipoAUpdate => {
            this.objTipoA[pos] = <TipoACtividadModel>tipoAUpdate;
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
              timeout: 2000,
              msgStr: 'Proceso Exitoso!'
            });
            this.closeModal();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser actualizado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }


}
