import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTipoActividadComponent } from './list-tipo-actividad.component';

describe('ListTipoActividadComponent', () => {
  let component: ListTipoActividadComponent;
  let fixture: ComponentFixture<ListTipoActividadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTipoActividadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTipoActividadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
