import { Component, OnInit, Output, Input, ViewChild,EventEmitter } from '@angular/core';
import { TipoACtividadModel } from '../../models/tipoActividad.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TipoActividadService } from '../../services/tipo-actividad.service';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-list-tipo-actividad',
  templateUrl: './list-tipo-actividad.component.html',
  styleUrls: ['./list-tipo-actividad.component.css']
})
export class ListTipoActividadComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<TipoACtividadModel>();
  @Input() tipoActividad: TipoACtividadModel[] = [];
  tipoIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private tipoActividadService: TipoActividadService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(tipoId:number) {
    this.dangerModal.show();
    this.tipoIdDelete = tipoId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(tipoActividad:TipoACtividadModel ) {
    this.openModalUpdate.emit(tipoActividad);
  }

  onDeleteTipoA() {

    //  const pos = this.tipoActividad.indexOf(tipoActividad);
    const pos = this.tipoActividad.indexOf(this.tipoActividad.find( tipo => tipo.id === this.tipoIdDelete));

    //  this.tipoActividadService.eliminarTipoActividad(tipoActividad.id)
    this.tipoActividadService.eliminarTipoActividad(this.tipoIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: '¡Proceso exitoso! El registro fue eliminado con éxito.',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.tipoActividad.splice(pos, 1);
       this.dangerModal.hide();

      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: '¡Ups! El registro no puede ser eliminado.',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
