import { Injectable } from '@angular/core';
import { TipoACtividadModel } from '../models/tipoActividad.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TipoActividadService {
  tipoACtividad: TipoACtividadModel;
  url = 'http://localhost:8080';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerTipoActividad() {
    // alert('---');
     return this.httpClient.get<TipoACtividadModel[]>(this.url + '/tipo_actividades', this.httpOptions)
     .pipe(map(data => data));
  }




  crearTipoActividad(tipoACtividad: TipoACtividadModel ) {

    tipoACtividad.tipoActividad = tipoACtividad.tipoActividad.toUpperCase();
    tipoACtividad.descripcion = tipoACtividad.descripcion.toUpperCase();
    return this.httpClient.post<TipoACtividadModel>(this.url + '/tipo_actividad', tipoACtividad);
  }


  eliminarTipoActividad(tipoActividadId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.delete(this.url + '/tipo_actividad/'+tipoActividadId, this.httpOptions);
  }

  actualizarTipoActividad(tipoACtividad: TipoACtividadModel) {
    tipoACtividad.tipoActividad = tipoACtividad.tipoActividad.toUpperCase();
    tipoACtividad.descripcion = tipoACtividad.descripcion.toUpperCase();
    return this.httpClient.put(this.url + '/tipo_actividad/'+tipoACtividad.id,tipoACtividad, this.httpOptions);
  }

}
