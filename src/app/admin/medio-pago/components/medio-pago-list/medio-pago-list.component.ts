import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { MedioPagoModel } from '../../models/medio-pago.model';
import {MedioPagoService} from './../../services/medio-pago.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-medio-pago-list',
  templateUrl: './medio-pago-list.component.html',
  styleUrls: ['./medio-pago-list.component.css']
})
export class MedioPagoListComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<MedioPagoModel>();
  @Input() mpagos: MedioPagoModel[] = [];
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  mpagoIdDelete: number;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private medioPagoService: MedioPagoService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(mpagoId: number) {
    this.mpagoIdDelete = mpagoId;
    this.dangerModal.show();
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(mPago: MedioPagoModel) {
    this.openModalUpdate.emit(mPago);
  }

  onDeleteMedioPago(mPago: MedioPagoModel) {

    const pos = this.mpagos.indexOf(this.mpagos.find( mp => mp.id === this.mpagoIdDelete));
     this.medioPagoService.eliminarMedioPago(this.mpagoIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El medio de pago fue eliminado exitosamente',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.mpagos.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El medio de pago no puede ser eliminado',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
