import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {MedioPagoModel} from './../../models/medio-pago.model';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';
import { MedioPagoService } from '../../services/medio-pago.service';

@Component({
  selector: 'app-medio-pago',
  templateUrl: './medio-pago.component.html',
  styleUrls: ['./medio-pago.component.css']
})
export class MedioPagoComponent implements OnInit {

  @ViewChild('template') private templatePr: TemplateRef<any>;
  formMedioPago: FormGroup;
  modalRef: BsModalRef;
  mPago: MedioPagoModel;
  objmPagos: MedioPagoModel[] = [];
  alerts: any[] = [];
  mPagoIdUpdate: number;
  constructor(
    private mPagoService: MedioPagoService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.mPago = {
        id: null,
        detalleMedioPago: '',
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.mPagoService.traerMedioPagos()
    .subscribe(mpagos => {
      this.objmPagos = mpagos;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexión',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });
  }
  get detalleMedioPagoField() {
    return this.formMedioPago.get('detalleMedioPago');
  }
  openModalMedioPago() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.formMedioPago.reset();
  this.modalRef.hide();

}
  openModalMedioPagoUpdate(medioPagoObj: MedioPagoModel) {
    this.mPago = {
      id: medioPagoObj.id,
      detalleMedioPago: medioPagoObj.detalleMedioPago.toUpperCase(),
    };
    this.mPagoIdUpdate = medioPagoObj.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formMedioPago = this.formBuilder.group({
      id: [0],
      detalleMedioPago: ['', [Validators.required]],
    });
    this.formMedioPago.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarMedioPago() {
    this.formMedioPago.value.detalleMedioPago = this.formMedioPago.value.detalleMedioPago.toUpperCase();
    const pos = this.objmPagos.indexOf(this.objmPagos.find( mp => mp.id === this.mPagoIdUpdate));
    if (this.formMedioPago.valid) {
      if (this.mPago.id === null ) {

        this.mPagoService.crearMedioPago(this.formMedioPago.value)
       .subscribe( newMpago => {
        this.alerts.push({
          type: 'success',
          msg: 'El medio de pago fué creado exitosamente',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formMedioPago.reset();
        this.objmPagos.push(newMpago);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.mPagoService.actualizarMedioPago(this.formMedioPago.value)
         .subscribe( mpagoUpdate => {
          this.objmPagos[ pos ] = <MedioPagoModel>mpagoUpdate ;
           this.alerts.push({
            type: 'success',
            msg: 'La actualización se realizó con éxito',
            timeout: 2000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualización no pudo completarce',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
