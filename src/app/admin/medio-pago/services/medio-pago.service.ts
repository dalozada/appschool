import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import {MedioPagoModel} from './../models/medio-pago.model';
@Injectable({
  providedIn: 'root'
})
export class MedioPagoService {

  medioPagoModel: MedioPagoModel;
  url = 'http://localhost:8080/inventario';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerMedioPagos() {
     return this.httpClient.get<MedioPagoModel[]>(this.url + '/mediopago', this.httpOptions)
     .pipe(map(data => data));
  }

  crearMedioPago(mPago: MedioPagoModel ) {
    return this.httpClient.post<MedioPagoModel>(this.url + '/guardarmediopago', mPago);
  }


  eliminarMedioPago(mPagoId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.post(this.url + '/eliminarmediopago', mPagoId, this.httpOptions);
  }
  actualizarMedioPago(mPago: MedioPagoModel) {
    return this.httpClient.post(this.url + '/actualizarmediopago', mPago, this.httpOptions);
  }
}
