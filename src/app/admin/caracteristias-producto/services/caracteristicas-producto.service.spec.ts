import { TestBed } from '@angular/core/testing';

import { CaracteristicasProductoService } from './caracteristicas-producto.service';

describe('CaracteristicasProductoService', () => {
  let service: CaracteristicasProductoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CaracteristicasProductoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
