import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaracteristicasListComponent } from './caracteristicas-list.component';

describe('CaracteristicasListComponent', () => {
  let component: CaracteristicasListComponent;
  let fixture: ComponentFixture<CaracteristicasListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaracteristicasListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaracteristicasListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
