import { Injectable } from '@angular/core';
import { TipoContratoModel } from '../models/tipoContrato.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TipoContratoService {
  tipoContrato: TipoContratoModel;
  url = 'http://localhost:8080/colegio';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerTipoContrato() {
    // alert('---');
     return this.httpClient.get<TipoContratoModel[]>(this.url + '/tipocontrato', this.httpOptions)
     .pipe(map(data => data));
  }




  crearTipoContrato(tipoContrato: TipoContratoModel ) {

    tipoContrato.nombreTipocontrato = tipoContrato.nombreTipocontrato.toUpperCase();
    tipoContrato.descripcion = tipoContrato.descripcion.toUpperCase();
    return this.httpClient.post<TipoContratoModel>(this.url + '/guardartipocontrato', tipoContrato);
  }


  eliminarTipoContrato(tipoContratoId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.post(this.url + '/eliminartipocontrato', tipoContratoId, this.httpOptions);
  }
  actualizarTipoContrato(tipoContrato: TipoContratoModel) {
    tipoContrato.nombreTipocontrato = tipoContrato.nombreTipocontrato.toUpperCase();
    tipoContrato.descripcion = tipoContrato.descripcion.toUpperCase();
    return this.httpClient.post(this.url + '/actualizartipocontrato', tipoContrato, this.httpOptions);
  }
}
