import { Component, OnInit, Output, Input, ViewChild,EventEmitter } from '@angular/core';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';
import { TipoContratoModel } from '../../models/tipoContrato.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TipoContratoService } from '../../services/tipo-contrato.service';
import { TipoContratoComponent } from '../tipo-contrato/tipo-contrato.component';

@Component({
  selector: 'app-list-tipo-contrato',
  templateUrl: './list-tipo-contrato.component.html',
  styleUrls: ['./list-tipo-contrato.component.css']
})
export class ListTipoContratoComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<TipoContratoModel>();
  // @Input() tipoContrato: TipoContratoModel[] = [];
  tipoIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  tipoContrato: TipoContratoModel[] = [];
  @ViewChild(TipoContratoComponent) formTp: TipoContratoComponent;

  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private tipoContratoService: TipoContratoService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {
    this.tipoContratoService.traerTipoContrato()
    .subscribe(tipoC => {
      this.tipoContrato = tipoC;
    },
    error => {
      this.alerts.push({
        type: 'danger',
        msg: 'Error de conexion',
        timeout: 5000,
        msgStr: 'Ups!'
      });
  });

  }
  openModalDelete(tipoId:number) {
    this.dangerModal.show();
    this.tipoIdDelete = tipoId;
  }
  openModalCrear() {
    this.formTp.templatePr.show();
  }
  openModalActualizar(tipoContrato:TipoContratoModel ) {
    this.formTp.openModalTipoCUpdate(tipoContrato);
  }

  onDeleteTipoC() {

    //  const pos = this.tipoContrato.indexOf(tipoContrato);
    const pos = this.tipoContrato.indexOf(this.tipoContrato.find( tipo => tipo.id === this.tipoIdDelete));

     this.tipoContratoService.eliminarTipoContrato(this.tipoIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: '¡Proceso exitoso! El registro fue eliminado con éxito.',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.tipoContrato.splice(pos, 1);
       this.dangerModal.hide();

      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: '¡Ups! El registro no puede ser eliminado.',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
