import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTipoContratoComponent } from './list-tipo-contrato.component';

describe('ListTipoContratoComponent', () => {
  let component: ListTipoContratoComponent;
  let fixture: ComponentFixture<ListTipoContratoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTipoContratoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTipoContratoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
