import { Component, OnInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { TipoContratoModel } from '../../models/tipoContrato.model';
import { TipoContratoService } from '../../services/tipo-contrato.service';
import { debounceTime } from 'rxjs/operators';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-tipo-contrato',
  templateUrl: './tipo-contrato.component.html',
  styleUrls: ['./tipo-contrato.component.css']
})
export class TipoContratoComponent implements OnInit {

  // @ViewChild('template') private templatePr: TemplateRef<any>;
  // @ViewChild('formtemplate') public templatePr: ModalDirective;
  @ViewChild('formtemplate') public templatePr: ModalDirective;
  @Input() tiposC: TipoContratoModel[] = [];

  formTipoc: FormGroup;
  modalRef: BsModalRef;
  tipoContrato: TipoContratoModel;
  objTipoC: TipoContratoModel[] = [];
  alerts: any[] = [];
  tipoIdUpdate: number;

  constructor(
    private tipoContratoService: TipoContratoService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.tipoContrato = {
        id: null,
        nombreTipocontrato: '',
        descripcion: '',
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.tipoContratoService.traerTipoContrato()
    .subscribe(tipoC => {
      this.objTipoC = tipoC;
    },
    error => {
      this.alerts.push({
        type: 'danger',
        msg: 'Error de conexion',
        timeout: 5000,
        msgStr: 'Ups!'
      });
  });
  }
  get nameTipoCField() {
    return this.formTipoc.get('nombreTipocontrato');
  }
  get descripcionTipo() {
    return this.formTipoc.get('descripcion');
  }
  openModalTipoC() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.formTipoc.reset();
  this.templatePr.hide();

}
  openModalTipoCUpdate(tipoCObj: TipoContratoModel) {
     this.tipoContrato = {
      id: tipoCObj.id,
      nombreTipocontrato: tipoCObj.nombreTipocontrato.toUpperCase(),
      descripcion: tipoCObj.descripcion,
    };
    this.tipoIdUpdate = tipoCObj.id;
    this.templatePr.show();
    // this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formTipoc = this.formBuilder.group({
      id: [0],
      nombreTipocontrato: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
    });
    this.formTipoc.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarTipoContrato(tipoC: TipoContratoModel) {

    // const pos = this.objTipoC.indexOf(tipoC);
    const pos = this.tiposC.indexOf(this.tiposC.find( tip =>tip.id === this.tipoIdUpdate));

    if (this.formTipoc.valid) {
      if (this.tipoContrato.id === null ) {

        this.tipoContratoService.crearTipoContrato(this.formTipoc.value)
       .subscribe( newTipoC => {
        this.alerts.push({
          type: 'success',
          msg: '¡Proceso exitoso! El registro fue creado con éxito.',
          timeout: 5000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formTipoc.reset();
        this.tiposC.push(newTipoC);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: '¡Ups! El registro no puede ser creado.',
          timeout: 5000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.tipoContratoService.actualizarTipoContrato(this.tipoContrato)
         .subscribe( tipoCUpdate => {
          this.tiposC[ pos ] = <TipoContratoModel>tipoCUpdate ;
           this.alerts.push({
            type: 'success',
            msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
            timeout: 2000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();

        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: '¡Ups! El registro no puede ser actualizado.',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
