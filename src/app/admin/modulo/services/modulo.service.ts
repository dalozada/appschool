import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ModuloModel } from '../components/models/modulo.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ModuloService {
  modulo: ModuloModel;
  url = ' http://localhost:8080/virtualt';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerModulo() {
     return this.httpClient.get<ModuloModel[]>(this.url + '/modulos', this.httpOptions)
     .pipe(map(data => data));
  }

  public slider() {
    return this.httpClient.get(this.url + '/slider', this.httpOptions)
    .pipe(map(data => data));
  }

  public moduloByRolUsuario(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traermodulosusuario', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public moduloByRol(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traermodulo', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public info(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/permisos', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  crearModulo(modulo: ModuloModel ) {

    modulo.detalle = modulo.detalle.toUpperCase();
    modulo.descripcion = modulo.descripcion.toUpperCase();
    modulo.codigo = modulo.codigo;
    return this.httpClient.post<ModuloModel>(this.url + '/guardarmodulo', modulo);
  }

  eliminarModulo(moduloId: number) {
    return this.httpClient.post(this.url + '/eliminarmodulo', moduloId, this.httpOptions);
  }
  actualizarModulo(modulo: ModuloModel) {
    modulo.detalle = modulo.detalle.toUpperCase();
    modulo.descripcion = modulo.descripcion.toUpperCase();
    modulo.codigo = modulo.codigo;
    return this.httpClient.post(this.url + '/actualizarmodulo', modulo, this.httpOptions);
  }
}
