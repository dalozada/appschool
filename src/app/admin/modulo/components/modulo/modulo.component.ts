import { Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import { ModuloModel } from './../models/modulo.model';
import { ModuloService } from './../../services/modulo.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-modulo',
  templateUrl: './modulo.component.html',
  styleUrls: ['./modulo.component.css']
})
export class ModuloComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formModulo: FormGroup;
  modalRef: BsModalRef;
  modulo: ModuloModel;
  objModulo: ModuloModel[] = [];
  alerts: any[] = [];
  moduloIdUpdate: number;
  constructor(
    private moduloService: ModuloService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.modulo = {
        id: null,
        detalle: '',
        descripcion: '',
        codigo: '',
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.moduloService.traerModulo()
    .subscribe(modulo => {
      this.objModulo = modulo;
    },
    error => {
      this.alerts.push({
        type: 'danger',
        msg: 'Error de conexion',
        timeout: 5000,
        msgStr: 'Ups!'
      });
    });
  }
  get nameModuloField() {
    return this.formModulo.get('detalle');
  }
  get descripcionModulo() {
    return this.formModulo.get('descripcion');
  }
  get codigoModulo() {
    return this.formModulo.get('codigo');
  }
  openModalModulo() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.formModulo.reset();
  this.modalRef.hide();

}
  openModalModuloUpdate(moduloObj: ModuloModel) {
     this.modulo = {
      id: moduloObj.id,
      detalle: moduloObj.detalle.toUpperCase(),
      descripcion: moduloObj.descripcion.toUpperCase(),
      codigo: moduloObj.codigo,
    };
    this.moduloIdUpdate = moduloObj.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formModulo = this.formBuilder.group({
      id: [0],
      detalle: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      codigo: ['', [Validators.required]],
    });
    this.formModulo.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarModulo(modulo: ModuloModel) {

    // tslint:disable-next-line: no-shadowed-variable
    const pos = this.objModulo.indexOf(this.objModulo.find( modulo => modulo.id === this.moduloIdUpdate ));
    if (this.formModulo.valid) {
      if (this.modulo.id === null ) {

        this.moduloService.crearModulo(this.formModulo.value)
       .subscribe( newModulo => {
        this.alerts.push({
          type: 'success',
          msg: 'El regitro fue creado con éxito',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formModulo.reset();
        this.objModulo.push(newModulo);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.moduloService.actualizarModulo(this.modulo)
         .subscribe( moduloUpdate => {
          this.objModulo[ pos ] = <ModuloModel>moduloUpdate;
           this.alerts.push({
            type: 'success',
            msg: 'La actualización se ha realizado con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualización no pudo completarse',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
