import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { ModuloModel } from '../models/modulo.model';
import { ModuloService } from './../../services/modulo.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-modulo-list',
  templateUrl: './modulo-list.component.html',
  styleUrls: ['./modulo-list.component.css']
})
export class ModuloListComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<ModuloModel>();
  @Input() modulo: ModuloModel[] = [];
  moduloIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private moduloService: ModuloService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(moduloId: number) {
    this.dangerModal.show();
    this.moduloIdDelete = moduloId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(modulo: ModuloModel) {
    this.openModalUpdate.emit(modulo);
  }

  onDeleteModulo() {

     const pos = this.modulo.indexOf(this.modulo.find( modulo => modulo.id === this.moduloIdDelete));
     this.moduloService.eliminarModulo(this.moduloIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El registro fue eliminado con éxito',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.modulo.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El registro no puede ser eliminado',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
