import { Component, OnInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { ClasificacionModel } from './../../models/clasificacion.model';
import { ClasificacionService } from './../../sevices/clasificacion.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-clasificacion',
  templateUrl: './clasificacion.component.html',
  styleUrls: ['./clasificacion.component.css']
})
export class ClasificacionComponent implements OnInit {
  @ViewChild('formtemplate') public templatePr: ModalDirective;
  @Input() clasificaciones: ClasificacionModel[];
  formClasificacion: FormGroup;
  modalRef: BsModalRef;
  clasificacion: ClasificacionModel;
  alerts: any[] = [];
  clasifIdUpdate: number;
  constructor(
    private clasificacionService: ClasificacionService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.clasificacion = {
        id: null,
        detalleClasificacion: '',
        descripcionClasificacion: ''
      };
      this.buildForm();
    }

    ngOnInit(): void {
    }
    get detalleClasificacionField() {
      return this.formClasificacion.get('detalleClasificacion');
    }
    get descripcionClasificacionField() {
      return this.formClasificacion.get('descripcionClasificacion');
    }
    openModalClasificacion() {
      this.modalRef = this.modalService.show(this.templatePr);
    }
  closeModal() {
    this.formClasificacion.reset();
    this.templatePr.hide();

  }
    openModalClasificacionUpdate(clasifObj: ClasificacionModel) {
       this.clasificacion = {
        id: clasifObj.id,
        detalleClasificacion: clasifObj.detalleClasificacion.toUpperCase(),
        descripcionClasificacion: clasifObj.descripcionClasificacion.toUpperCase(),
      };
      this.clasifIdUpdate = clasifObj.id;
      this.templatePr.show();
    }
    private buildForm() {

      this.formClasificacion = this.formBuilder.group({
        id: [0],
        detalleClasificacion: ['', [Validators.required]],
        descripcionClasificacion: ['', [Validators.required]],
      });
      this.formClasificacion.valueChanges
      .pipe(
        debounceTime(350),
      )
      .subscribe(data => {
      });
    }

    guardarClasificacion() {
      this.formClasificacion.value.detalleClasificacion = this.formClasificacion.value.detalleClasificacion.toUpperCase();
     this.formClasificacion.value.descripcionClasificacion = this.formClasificacion.value.descripcionClasificacion.toUpperCase();
      const pos = this.clasificaciones.indexOf(this.clasificaciones.find( clas => clas.id === this.clasifIdUpdate));
      if (this.formClasificacion.valid) {
        if (this.formClasificacion.value.id === null ) {

          this.clasificacionService.crearClasificacion(this.formClasificacion.value)
         .subscribe( newClasif => {
          this.alerts.push({
            type: 'success',
            msg: 'El registro fué creado exitosamente',
            timeout: 2000,
            msgStr: 'Proceso Exitoso!'
          });
          this.clasificaciones.push(newClasif);
          this.formClasificacion.reset();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error al guardar la información',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });

        } else {
           this.clasificacionService.actualizarClasificacion(this.formClasificacion.value)
           .subscribe( clasifUpdate => {
             clasifUpdate.id = this.clasificacion.id;
            this.clasificaciones[ pos ] = <ClasificacionModel>clasifUpdate ;
             this.alerts.push({
              type: 'success',
              msg: 'La actualización se realizó con éxito',
              timeout: 1000,
              msgStr: 'Proceso Exitoso!'
            });
            this.closeModal();
          },
          error => {
            this.alerts.push({
              type: 'danger',
              msg: 'El registro no puede ser actualizado',
              timeout: 2000,
              msgStr: 'Ups!'
            });
          });
        }
      }

    }
    onClosed(dismissedAlert: AlertComponent): void {
      this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
    }


}
