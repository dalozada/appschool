import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClasificacionListComponent } from './clasificacion-list.component';

describe('ClasificacionListComponent', () => {
  let component: ClasificacionListComponent;
  let fixture: ComponentFixture<ClasificacionListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClasificacionListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClasificacionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
