import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { ClasificacionModel } from '../../models/clasificacion.model';
import { ClasificacionComponent } from '../../components/clasificacion/clasificacion.component';
import { ClasificacionService } from './../../sevices/clasificacion.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-clasificacion-list',
  templateUrl: './clasificacion-list.component.html',
  styleUrls: ['./clasificacion-list.component.css']
})
export class ClasificacionListComponent implements OnInit {
  @ViewChild(ClasificacionComponent) formClas: ClasificacionComponent;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  objClasif: ClasificacionModel[] = [];
  clasifIdDelete: number;
  pageActual: number = 1; 
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  
  constructor(
    private clasificacionService: ClasificacionService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {
    this.clasificacionService.traerClasificacion()
    .subscribe(clasif => {
      this.objClasif = clasif;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexión',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });
  }
  openModalDelete(clasifId: number) {
    this.dangerModal.show();
    this.clasifIdDelete = clasifId;
  }
  openModalCrear() {
    this.formClas.templatePr.show();
  }
  openModalActualizar(clasif: ClasificacionModel) {
    this.formClas.openModalClasificacionUpdate(clasif);
  }
  closeModal() {
    this.formClas.closeModal();
  }
  onDeleteCalsificacion() {

     const pos = this.objClasif.indexOf(this.objClasif.find( clf => clf.id === this.clasifIdDelete));
     this.clasificacionService.eliminarClasificacion(this.clasifIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El registro fue eliminado con éxito',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.objClasif.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El registro no puede ser eliminado',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
