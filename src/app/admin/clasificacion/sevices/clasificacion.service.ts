import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ClasificacionModel } from './../models/clasificacion.model';
@Injectable({
  providedIn: 'root'
})
export class ClasificacionService {

  clasificacionModel: ClasificacionModel;
  url = 'http://localhost:8080';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerClasificacion() {
     return this.httpClient.get<ClasificacionModel[]>(this.url + '/clasificaciones', this.httpOptions)
     .pipe(map(data => data));
  }

  crearClasificacion(clasif: ClasificacionModel ) {
    return this.httpClient.post<ClasificacionModel>(this.url + '/clasificacion', clasif, this.httpOptions);
  }


  eliminarClasificacion(clasifId: number) {
    const url = `${this.url}/clasificacion/${clasifId}`;
    return this.httpClient.delete(url);
  }
  actualizarClasificacion(clasif: ClasificacionModel) {
    const url = `${this.url}/clasificacion/${clasif.id}`;
    return this.httpClient.put<ClasificacionModel>(url, clasif);
  }
}
