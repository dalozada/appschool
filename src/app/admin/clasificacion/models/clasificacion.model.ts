export interface ClasificacionModel {
  id: number;
  detalleClasificacion: string;
  descripcionClasificacion: string;
}
