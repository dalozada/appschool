import { TestBed } from '@angular/core/testing';

import { RolPersonasService } from './rol-personas.service';

describe('RolPersonasService', () => {
  let service: RolPersonasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RolPersonasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
