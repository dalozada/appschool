import { Injectable } from '@angular/core';
import { EmpresaModel } from './../models/empresa.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AsignarRolEmpresaModel } from './../models/asignar-rol-empresa.model';


@Injectable({
  providedIn: 'root'
})
export class EmpresaService {
  empresa: EmpresaModel;
  url = 'http://localhost:8080/';
  httOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerEmpresaPrincipal() {
    return this.httpClient.get<EmpresaModel>(this.url + 'inventario/empresa_principal', this.httOptions)
    .pipe(map(data => data));
  }
  crearEmpresa(empresa: FormData) {
    return this.httpClient.post<EmpresaModel>(this.url + 'inventario/guardarempresa', empresa);
  }

  eliminarEmpresa(empresaId: number) {
    return this.httpClient.delete(this.url + 'inventario/eliminarempresa', this.httOptions);
  }

  actualizarEmpresa(empresa: EmpresaModel) {
    empresa.razonSocial = empresa.razonSocial;
    empresa.nit = empresa.nit;
    return this.httpClient.put<EmpresaModel>(this.url + 'inventario/actualizarempresas/' + empresa.id, empresa, this.httOptions);
  }
  crearRolEmpresa(rolEmpresa: AsignarRolEmpresaModel) {
    return this.httpClient.post<AsignarRolEmpresaModel>(this.url + 'asignacion_rol_empresa', rolEmpresa, this.httOptions);
  }
}
