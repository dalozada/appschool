import { Injectable } from '@angular/core';
import { MatriculaModel } from '../models/matricula.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MatriculaService {
  matricula: MatriculaModel;
  url = 'http://localhost:8080';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }


  public traerMatricula() {
    // alert('---');
     return this.httpClient.get<MatriculaModel[]>(this.url + '/contratos', this.httpOptions)
     .pipe(map(data => data));
  }
  public traerMatriculasEstudiante(identificacion: number) {
     return this.httpClient.get<MatriculaModel[]>(this.url + '/matriculas/persona/identificacion', this.httpOptions)
     .pipe(map(data => data));
  }

  crearMatricula(matricula: MatriculaModel) {
    // console.log(contrato,'----llego contrato---');
    matricula.fecha=matricula.fecha["formatted"]

    return this.httpClient.post<MatriculaModel>(this.url + '/matricula', matricula);
  }
  

  // eliminarPersona(personaId: number) {
  //   // const url = `${this.path}/${todoId}`;
  //   return this.httpClient.post(this.url + '/persona', personaId, this.httpOptions);
  // }

  actualizarMatricula(matricula: MatriculaModel) {


    return this.httpClient.put(this.url + '/matriculas/'+matricula.id, matricula, this.httpOptions);
  }
}
