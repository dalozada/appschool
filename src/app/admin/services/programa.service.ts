import { Injectable, ɵConsole } from '@angular/core';
import { ProgramaModel } from '../models/programa.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class ProgramaService {

  programa: ProgramaModel;
  url = 'http://localhost:8080/colegio';
    httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerProgramas() {
     return this.httpClient.get<ProgramaModel[]>(this.url + '/programas', this.httpOptions)
     .pipe(map(data => data));
  }

  crearPrograma(programa: ProgramaModel ) {
    return this.httpClient.post<ProgramaModel>(this.url + '/programas', programa);
  }


  eliminarPrograma(programaId: number) {
    const url = `${this.url}/programas/${programaId}`;
    return this.httpClient.delete(url);
  }
  actualizarPrograma(programa: ProgramaModel) {
    const url = `${this.url}/programas/${programa.id}`;
    return this.httpClient.put<ProgramaModel>(url, programa);
  }
  cambiarEstadoPrograma(idPrograma: number, idEstado: number) {
    const url = `${this.url}/programas/${idPrograma}/estado/${idEstado}`;
    return this.httpClient.put<ProgramaModel>(url, {idestado: idEstado });
  }
}
