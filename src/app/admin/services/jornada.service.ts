import { Injectable } from '@angular/core';
import { JornadaModel } from '../models/jornada.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class JornadaService {
  jornada: JornadaModel;
  url = 'http://localhost:8080/colegio';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient

  ) { }

  public traerJornadas() {
    // alert('---');
     return this.httpClient.get<JornadaModel[]>(this.url + '/jornadas', this.httpOptions)
     .pipe(map(data => data));
  }
  public traerJornadasPrograma(idPrograma: number) {
    const url = `${this.url}/jornadas/${idPrograma}`;
     return this.httpClient.get<JornadaService[]>(url, this.httpOptions)
     .pipe(map(data => data));
  }
  traerAsignacionJornadasPrograma(programaId: number ) {
    const url = `${this.url}/asignacion_jornada_programa/programa/${programaId}`;
    return this.httpClient.get<JornadaModel[]>(url, this.httpOptions);
  }
  agregarJornadaPrograma(asignarJornadas: object ) {
    const url = `${this.url}/asignacion_jornada_programa`;
    return this.httpClient.post<JornadaModel>(url, asignarJornadas);
  }
  crearJornada(jornada: JornadaModel ) {
    const url = `${this.url}/jornadas`;
    return this.httpClient.post<JornadaModel>(url, jornada);
  }
  eliminarJornada(jornadaId: number) {
    const url = `${this.url}/jornadas/${jornadaId}`;
    return this.httpClient.delete(url, this.httpOptions);
  }

  actualizarJornada(jornada: JornadaModel) {
    const url = `${this.url}/jornadas/${jornada.id}`;
    return this.httpClient.put(url, this.httpOptions);
  }
}
