import { Injectable } from '@angular/core';
import { DiaModel } from '../models/dia.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DiaService {
  dia: DiaModel;
  url = 'http://localhost:8080';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient

  ) { }

  public traerdias() {
     return this.httpClient.get<DiaModel[]>(this.url + '/dias', this.httpOptions)
     .pipe(map(data => data));
  }
  public traerDiasJornada(idjornada: number) {
    const url = `${this.url}/dias/${idjornada}`;
     return this.httpClient.get<DiaService[]>(url, this.httpOptions)
     .pipe(map(data => data));
  }
  creardia(dia: DiaModel ) {
    return this.httpClient.post<DiaModel>(this.url + '/dias', dia);
  }
  agregarDiaJornada(asignarDiaJornada: object ) {
    const url = `${this.url}/colegio/asignar_dia_jornadas`;
    return this.httpClient.post<object>(url, asignarDiaJornada);
  }
  eliminardia(diaId: number) {
    const url = `${this.url}/dias/${diaId}`;
    return this.httpClient.delete(url, this.httpOptions);
  }

  actualizardia(dia: DiaModel) {
    const url = `${this.url}/dias/${dia.id}`;
    return this.httpClient.put(url, this.httpOptions);
  }
}
