import { Injectable } from '@angular/core';
import { TipoProgramaModel } from '../models/tipo-programa.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TipoProgramaService {
  tipo: TipoProgramaModel;
  url = 'http://localhost:8080';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerTipoProgramas() {
     return this.httpClient.get<TipoProgramaModel[]>(this.url + '/tipo_programas', this.httpOptions)
     .pipe(map(data => data));
  }


}
