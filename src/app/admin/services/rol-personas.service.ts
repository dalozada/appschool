import { Injectable } from '@angular/core';
import { RolPersonaModel } from './../config-pensum/models/rol-persona.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class RolPersonasService {
  rolPersona: RolPersonaModel;
  url = 'http://localhost:8080';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient

  ) { }

  public traerDocentes() {
     return this.httpClient.get<RolPersonaModel[]>(this.url + '/contratos/nombre_rol/DOCENTE', this.httpOptions)
     .pipe(map(data => data));
  }


}
