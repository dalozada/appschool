import { Injectable } from '@angular/core';
import { Tipo_transaccionModel } from '../models/tipo_transaccion';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TipoTransaccionService {
  tipo_transaccion: Tipo_transaccionModel;
  url = 'http://localhost:8080';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerTipo_transaccion() {
     return this.httpClient.get<Tipo_transaccionModel[]>(this.url + '/tipo_transacciones', this.httpOptions)
     .pipe(map(data => data));
  }




  crearTipo_transaccion(tipo_transaccion: Tipo_transaccionModel) {

    tipo_transaccion.detalle = tipo_transaccion.detalle.toUpperCase();
    tipo_transaccion.descripcion = tipo_transaccion.descripcion.toUpperCase();
    return this.httpClient.post<Tipo_transaccionModel>(this.url + '/tipo_transaccion', tipo_transaccion);
  }


  eliminarTipo_transaccion(tipo_transaccionId: number) {
    return this.httpClient.delete(this.url + '/tipo_transaccion/' + tipo_transaccionId, this.httpOptions);
  }

  actualizarTipo_transaccion(tipo_transaccion: Tipo_transaccionModel) {
    tipo_transaccion.detalle = tipo_transaccion.detalle.toUpperCase();
    tipo_transaccion.descripcion = tipo_transaccion.descripcion.toUpperCase();
    return this.httpClient.put(this.url + '/tipo_transaccion/' + tipo_transaccion.id, tipo_transaccion, this.httpOptions);
  }

}
