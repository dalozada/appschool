import { Component, OnInit, Output, Input, ViewChild, EventEmitter } from '@angular/core';
import { Tipo_transaccionModel } from '../../models/tipo_transaccion';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TipoTransaccionService } from '../../services/tipo-transaccion.service';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-tipo-transaccion-list',
  templateUrl: './tipo-transaccion-list.component.html',
  styleUrls: ['./tipo-transaccion-list.component.css']
})
export class TipoTransaccionListComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<Tipo_transaccionModel>();
  @Input() tipo_transacciones: Tipo_transaccionModel[] = [];
  tipo_transaccionIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private tipo_transaccionService: TipoTransaccionService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(tipotranId: number) {
    this.dangerModal.show();
    this.tipo_transaccionIdDelete = tipotranId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(tipo_transaccion: Tipo_transaccionModel) {
    this.openModalUpdate.emit(tipo_transaccion);
  }

  onDeleteTipo_transaccion() {

    const pos = this.tipo_transacciones.indexOf(this.tipo_transacciones.find( men => men.id === this.tipo_transaccionIdDelete));
    this.tipo_transaccionService.eliminarTipo_transaccion(this.tipo_transaccionIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El registro fue eliminado con éxito.',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.tipo_transacciones.splice(pos, 1);
       this.dangerModal.hide();

      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El registro no puede ser eliminado.',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
