import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Tipo_transaccionModel } from '../../models/tipo_transaccion';
import { TipoTransaccionService } from '../../services/tipo-transaccion.service';
import { debounceTime } from 'rxjs/operators';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-tipo-transaccion',
  templateUrl: './tipo-transaccion.component.html',
  styleUrls: ['./tipo-transaccion.component.css']
})
export class TipoTransaccionComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formTipoTransaccion: FormGroup;
  modalRef: BsModalRef;
  tipo_transaccion: Tipo_transaccionModel;
  objTipo_transaccion: Tipo_transaccionModel[] = [];
  alerts: any[] = [];
  tipo_transaccionIdUpdate: number;

  constructor(
    private tipo_transaccionService: TipoTransaccionService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
  ) {
    this.tipo_transaccion = {
      id: null,
      detalle: '',
      descripcion: '',
    };
    this.buildForm();
  }

  ngOnInit(): void {
    this.tipo_transaccionService.traerTipo_transaccion()
      .subscribe(tipo_transaccion => {
        this.objTipo_transaccion = tipo_transaccion;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }
  get nameTipo_transaccionField() {
    return this.formTipoTransaccion.get('detalle');
  }
  get descripcionTipoField() {
    return this.formTipoTransaccion.get('descripcion');
  }
  openModalTipo_transaccion() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
  closeModal() {
    this.formTipoTransaccion.reset();
    this.modalRef.hide();

  }
  openModalTipo_transaccionUpdate(tipo_transaccionObj: Tipo_transaccionModel) {
    this.tipo_transaccion = {
      id: tipo_transaccionObj.id,
      detalle: tipo_transaccionObj.detalle.toUpperCase(),
      descripcion: tipo_transaccionObj.descripcion.toUpperCase(),
    };
    this.tipo_transaccionIdUpdate = tipo_transaccionObj.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formTipoTransaccion = this.formBuilder.group({
      id: [0],
      detalle: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
    });
    this.formTipoTransaccion.valueChanges
      .pipe(
        debounceTime(350),
      )
      .subscribe(data => {
      });
  }

  guardarTipo_transaccion(tipo_transaccion: Tipo_transaccionModel) {

    const pos = this.objTipo_transaccion.indexOf(this.objTipo_transaccion.find( tipo => tipo.id === this.tipo_transaccionIdUpdate));
    if (this.formTipoTransaccion.valid) {
      if (this.tipo_transaccion.id === null) {

        this.tipo_transaccionService.crearTipo_transaccion(this.formTipoTransaccion.value)
          .subscribe(newTipo_transaccion => {
            this.alerts.push({
              type: 'success',
              msg: 'El registro fue creado con éxito.',
              timeout: 2000,
              msgStr: 'Proceso Exitoso!'
            });
            this.formTipoTransaccion.reset();
            this.objTipo_transaccion.push(newTipo_transaccion);
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: 'Error al guardar la información',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });

      } else {
        this.tipo_transaccionService.actualizarTipo_transaccion(this.tipo_transaccion)
          .subscribe(tipo_transaccionUpdate => {
            this.objTipo_transaccion[pos] = <Tipo_transaccionModel>tipo_transaccionUpdate;
            this.alerts.push({
              type: 'success',
              msg: '¡La actualización se ha realizado con éxito.',
              timeout: 1000,
              msgStr: 'Proceso Exitoso!'
            });
            this.closeModal();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: 'La actualización no puedo completarse',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }


}
