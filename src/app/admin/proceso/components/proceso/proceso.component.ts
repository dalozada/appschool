import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { ProcesoModel } from './../../models/proceso.model';
import { ProcesoService } from './../../services/proceso.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';
@Component({
  selector: 'app-proceso',
  templateUrl: './proceso.component.html',
  styleUrls: ['./proceso.component.css']
})
export class ProcesoComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formProceso: FormGroup;
  modalRef: BsModalRef;
  proceso: ProcesoModel;
  objProceso: ProcesoModel[] = [];
  alerts: any[] = [];
  procesoIdUpdate: number;
  constructor(
    private procesoService: ProcesoService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.proceso = {
        id: null,
        nombreProceso: '',
        descripcion: '',
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.procesoService.traerProcesos()
    .subscribe(proceso => {
      this.objProceso = proceso;
    },
  error => {
    this.alerts.push({
      type: 'danger',
      msg: 'Error de conexion',
      timeout: 5000,
      msgStr: 'Ups!'
    });
  });
  }
  get nameProcesoField() {
    return this.formProceso.get('nombreProceso');
  }
  get descripcionProceso() {
    return this.formProceso.get('descripcion');
  }
  openModalProceso() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.formProceso.reset();
  this.modalRef.hide();

}
  openModalProcesoUpdate(procesoObj: ProcesoModel) {
     this.proceso = {
      id: procesoObj.id,
      nombreProceso: procesoObj.nombreProceso.toUpperCase(),
      descripcion: procesoObj.descripcion.toUpperCase(),
    };
    this.procesoIdUpdate = procesoObj.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formProceso = this.formBuilder.group({
      id: [0],
      nombreProceso: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
    });
    this.formProceso.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarProceso(proceso: ProcesoModel) {

    // tslint:disable-next-line: no-shadowed-variable
    const pos = this.objProceso.indexOf(this.objProceso.find( proceso => proceso.id === this.procesoIdUpdate));
    if (this.formProceso.valid) {
      if (this.proceso.id === null ) {

        this.procesoService.crearProceso(this.formProceso.value)
       .subscribe( newProceso => {
        this.alerts.push({
          type: 'success',
          msg: 'El registro fue creado con éxito',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formProceso.reset();
        this.objProceso.push(newProceso);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.procesoService.actualizarProceso(this.proceso)
         .subscribe( procesoUpdate => {
          this.objProceso[ pos ] = <ProcesoModel>procesoUpdate ;
           this.alerts.push({
            type: 'success',
            msg: 'La actualización se ha realizado con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualización no pudo completarse',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
