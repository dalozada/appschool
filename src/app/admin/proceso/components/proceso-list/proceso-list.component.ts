import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { ProcesoModel } from '../../models/proceso.model';
import { ProcesoService } from './../../services/proceso.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-proceso-list',
  templateUrl: './proceso-list.component.html',
  styleUrls: ['./proceso-list.component.css']
})
export class ProcesoListComponent implements OnInit {
@Output() openModalNew = new EventEmitter();
@Output() openModalUpdate = new EventEmitter<ProcesoModel>();
@Input() proceso: ProcesoModel[] = [];
procesoIdDelete: number;
pageActual: number = 1;
numReg: number;
alerts: any[] = [];
dismissible = true;
@ViewChild('dangerModal') public dangerModal: ModalDirective;
constructor(
  private procesoService: ProcesoService,
) {
  this.enviarNumeroRegistros(10);
}

ngOnInit(): void {

}
openModalDelete(procesoId: number) {
  this.dangerModal.show();
  this.procesoIdDelete = procesoId;
}
openModalCrear() {
  this.openModalNew.emit();
}
openModalActualizar(proceso: ProcesoModel) {
  this.openModalUpdate.emit(proceso);
}

onDeleteProceso() {

   const pos = this.proceso.indexOf(this.proceso.find( proceso => proceso.id === this.procesoIdDelete));
   this.procesoService.eliminarProceso(this.procesoIdDelete)
   .subscribe(rta => {
    this.alerts.push({
      type: 'success',
      msg: 'El registro fue eliminado con éxito',
      timeout: 2000,
      msgStr: 'Proceso Exitoso!'
    });
     this.proceso.splice(pos, 1);
     this.dangerModal.hide();
    },
     error => {
      this.alerts.push({
        type: 'danger',
        msg: 'El registro no puede ser eliminado',
        timeout: 3000,
        msgStr: 'Ups!'
      });
     });
    }

enviarNumeroRegistros(num: number) {
  this.numReg = num;
}
onClosed(dismissedAlert: AlertComponent): void {
  this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
}

}
