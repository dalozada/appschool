import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProcesoModel } from '../models/proceso.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProcesoService {
  proceso: ProcesoModel;
  url = 'http://localhost:8080';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerProcesos() {
     return this.httpClient.get<ProcesoModel[]>(this.url + '/procesos', this.httpOptions)
     .pipe(map(data => data));
  }

  public slider() {
    return this.httpClient.get(this.url + '/slider', this.httpOptions)
    .pipe(map(data => data));
  }

  public procesosByRolUsuario(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traerprocesossusuario', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public procesosByRol(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traerprocesos', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public info(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/permisos', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  crearProceso(proceso: ProcesoModel) {

    proceso.nombreProceso = proceso.nombreProceso.toUpperCase();
    proceso.descripcion = proceso.descripcion.toUpperCase();
    return this.httpClient.post<ProcesoModel>(this.url + '/proceso', proceso);
  }


  eliminarProceso(procesoId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.post(this.url + '/proceso' , procesoId, this.httpOptions);
  }
  actualizarProceso(proceso: ProcesoModel) {
    proceso.nombreProceso = proceso.nombreProceso.toUpperCase();
    proceso.descripcion = proceso.descripcion.toUpperCase();
    return this.httpClient.put(this.url + '/proceso/' + proceso.id, proceso, this.httpOptions);
  }
}
