export interface ProgramaModel{

  id:number,
  nombrePrograma:string,
  idtipoPrograma:number,
  descripcionPrograma:string,
  codigoPrograma:string,
  estado:number

}
