export interface AcudienteModel {
  id: number,
  nombres: string,
  apellidos: string,
  identificacion: string,
  telmovil: string,
  telfijo: string,
  email: string,
  idparentesco: number,
}
