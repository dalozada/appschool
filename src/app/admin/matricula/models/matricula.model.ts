export interface MatriculaModel{

  id: number,
  fecha: string,
  idacudiente: number,
  idpersona: number,
  idprestacionsocia:number,
  idprograma:number,

}
