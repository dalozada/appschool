import { Injectable } from '@angular/core';
import { ProgramaModel } from '../../models/programa.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProgramaService {
  programa: ProgramaModel;
  url = 'http://localhost:8080/colegio';
  urls = 'http://localhost:8080/periodos';
  urlR = 'http://localhost:8080';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }


  public traerPrograma() {
    // alert('---');
     return this.httpClient.get<ProgramaModel[]>(this.url + '/programas_activos', this.httpOptions)
     .pipe(map(data => data));
  }


  public periodosByProg(idprograma:number) {
    // alert();
    let idProgrma = idprograma;

    return this.httpClient.get(this.urlR + '/ultimo_periodo/'+idProgrma, this.httpOptions)
    .pipe(map(data => data));

  }
  public traerPeriodosByPrograma() {
    // alert('---');
     return this.httpClient.get<ProgramaModel[]>(this.urls + '/programas_activos', this.httpOptions)
     .pipe(map(data => data));
  }
}
