import { Injectable } from '@angular/core';
import { DocumentoMatriculaModel } from '../models/documentomatricula.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TipoDocumentoModel } from '../models/tipodocumento.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DocumentoMatriculaService {

  documento: DocumentoMatriculaModel;
  url = 'http://localhost:8080/colegio';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }


  public traerTiposDocumento() {
    // alert('---');
     return this.httpClient.get<TipoDocumentoModel[]>('http://localhost:8080/tipo_documentos', this.httpOptions)
     .pipe(map(data => data));
  }


  crearDocumento(documento: DocumentoMatriculaModel ) {

    return this.httpClient.post<DocumentoMatriculaModel>(this.url + '/guardardocumentomatricula', documento);
  }


  // eliminarPersona(personaId: number) {
  //   // const url = `${this.path}/${todoId}`;
  //   return this.httpClient.post(this.url + '/persona', personaId, this.httpOptions);
  // }

  // public personaByIdentificacion(identificacion: number) {

  //   return this.httpClient.get(this.url + '/personas/identificacion/' + identificacion, this.httpOptions)
  //   .pipe(map(data => data));

  // }

  // actualizarAcudiente(documento: DocumentoMatriculaModel) {


  //   return this.httpClient.put(this.url + '/acudiente/' + acudiente.id, acudiente, this.httpOptions);
  // }
}
