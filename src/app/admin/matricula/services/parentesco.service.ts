import { Injectable } from '@angular/core';
import { ParentescoModel } from '../models/parentesco.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ParentescoService {
  parentesco:ParentescoModel;
  url = 'http://localhost:8080';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }


  public traerParentesco() {
    // alert('---');
     return this.httpClient.get<ParentescoModel[]>(this.url + '/parentescos', this.httpOptions)
     .pipe(map(data => data));
  }
}
