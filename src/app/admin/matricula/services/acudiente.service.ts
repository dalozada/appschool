import { Injectable } from '@angular/core';
import { AcudienteModel } from '../models/acudiente.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AcudienteService {
  acudiente: AcudienteModel;
  url = 'http://localhost:8080';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }


  // public traerTiposId() {
  //   // alert('---');
  //    return this.httpClient.get<TipoIdentificacionModel[]>(this.url + '/virtualt/tiposidentificacion', this.httpOptions)
  //    .pipe(map(data => data));
  // }


  crearAcudiente(acudiente: AcudienteModel ) {

    return this.httpClient.post<AcudienteModel>(this.url + '/acudiente', acudiente);
  }


  // eliminarPersona(personaId: number) {
  //   // const url = `${this.path}/${todoId}`;
  //   return this.httpClient.post(this.url + '/persona', personaId, this.httpOptions);
  // }

  // public personaByIdentificacion(identificacion: number) {

  //   return this.httpClient.get(this.url + '/personas/identificacion/' + identificacion, this.httpOptions)
  //   .pipe(map(data => data));

  // }

  actualizarAcudiente(acudiente: AcudienteModel) {


    return this.httpClient.put(this.url + '/acudiente/' + acudiente.id, acudiente, this.httpOptions);
  }
}
