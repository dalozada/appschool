import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PersonaModel } from '../../../contratacion/models/persona.model';
import { MatriculaModel } from '../../models/matricula.model';
import { CiudadModel } from '../../../contratacion/models/ciudad.model';
import { DepartamentoModel } from '../../../contratacion/models/departamento.model';
import { RolModel } from '../../../roles/components/models/rol.model';
import { TipoIdentificacionModel } from '../../../contratacion/models/tipoIdentificacion.model';
import { IMyDpOptions } from 'mydatepicker';
import { NgWizardConfig, THEME, NgWizardService, StepChangedArgs } from 'ng-wizard';
import { PersonaService } from '../../../contratacion/service/persona.service';
import { CiudadService } from '../../../contratacion/service/ciudad.service';
import { MatriculaService } from './../../../services/matricula.service';
import { RolesService } from '../../../roles/services/roles.service';
import { DepartamentoService } from '../../../contratacion/service/departamento.service';
import { AcudienteModel } from '../../models/acudiente.model';
import { debounceTime } from 'rxjs/operators';
import { ProgramaModel } from '../../../models/programa.model';
import { ParentescoModel } from '../../models/parentesco.model';
import { ParentescoService } from '../../services/parentesco.service';
// import { ProgramaService } from '../../../../coordinacion/config-pensum/services/programa.service';
import { AcudienteService } from '../../services/acudiente.service';
import { DocumentoMatriculaService } from '../../services/documento-matricula.service';
import { TipoDocumentoModel } from '../../models/tipodocumento.model';
import { ProgramaService } from '../../services/programa.service';
import { PeriodoModel } from '../../../config-pensum/models/periodo.model';
import { DocumentoMatriculaModel } from '../../models/documentomatricula.model';
import { AsignacionRolModel } from '../../../contratacion/models/asignacionrol.model';
import { AsignacionrolService } from '../../../contratacion/service/asignacionrol.service';

@Component({
  selector: 'app-matricula',
  templateUrl: './matricula.component.html',
  styleUrls: ['./matricula.component.css']
})
export class MatriculaComponent implements OnInit {
  formPersona: FormGroup;
  formAcudiente: FormGroup;
  formMatricula: FormGroup;
  formContrato: FormGroup;
  formUbicacion: FormGroup;
  formDocumento: FormGroup;
  fechaPrevia: any;
  persona: PersonaModel;
  asignacion: AsignacionRolModel;
  documento: DocumentoMatriculaModel;
  departamento: DepartamentoModel;
  departamentoU: DepartamentoModel;
  tipoIdentificacionR: TipoIdentificacionModel;
  ciudad: CiudadModel;
  ciudadU: CiudadModel;
  matricula: MatriculaModel;
  idMatricula: number;
  acudiente: AcudienteModel;
  idPersona: number;
  idAcudiente: number;
  objCiudad: CiudadModel[] = [];
  objMatricula: MatriculaModel[] = [];
  objCiudadDep: CiudadModel[] = [];
  objPersonaId: PersonaModel[] = [];
  objCiudadDepU: CiudadModel[] = [];
  objDep: DepartamentoModel[] = [];
  objPersona: PersonaModel[] = [];
  objRol: RolModel;
  objPeriodo: PeriodoModel;
  objTipoDoc: TipoDocumentoModel[] = [];
  objPar: ParentescoModel[] = [];
  objProg: ProgramaModel[] = [];
  objTipoId: TipoIdentificacionModel[] = [];
  per:any;
  // objTipoContrato: TipoContratoModel[] = [];
  index: number;

  public myDatePickerOptionsFin: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    openSelectorOnInputClick: true,
    showInputField: true,
    inline: false,
    editableDateField: false,

    disableSince: {

      year: new Date().getFullYear(),
      month: new Date().getUTCMonth() + 1,
      day: new Date().getDate() + 1

    }

  }

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    openSelectorOnInputClick: true,
    showInputField: true,
    inline: false,
    editableDateField: false


  }


  alerts: any[] = [];
  perIdUpdate: number;

  config: NgWizardConfig = {
    selected: 0,
    theme: THEME.arrows,
    toolbarSettings: {
      // showNextButton:true,
      // showExtraButtons:false,

      toolbarExtraButtons: [
        {

          text: 'Finish', class: 'btn btn-info', event: () => {
            if(this.formDocumento.valid){

              this.guardarPersona(this.persona);
            }else{
              this.alerts.push({
                type: 'danger',
                msg: 'Debes completar todos los pasos',
                timeout: 5000,
                msgStr: 'Ups!'
              });
            }


          }


        },
        // {
        //   text: 'Reset',
        //   class: 'btn btn-danger',
        //   event: () => {
        //     this.resetWizard();
        //   }
        // }


      ]
    }
  };

  constructor(
    private personaService: PersonaService,
    private ciudadService: CiudadService,
    private acudienteService: AcudienteService,
    // private tipoContratoService: TipoContratoService,
    private matriculaService: MatriculaService,
    private asignacionService: AsignacionrolService,
    private documentoMatriculaService: DocumentoMatriculaService,
    private rolService: RolesService,
    private departamentoService: DepartamentoService,
    private parentescoService: ParentescoService,
    private programaService: ProgramaService,
    private formBuilder: FormBuilder,
    private ngWizardService: NgWizardService
  ) {

    this.idPersona = null;
    this.persona = {
      id: null,
      nombre1: '',
      apellido1: '',
      nombre2: '',
      apellido2: '',
      fechaNac: null,
      ciudadNac: { id: null, departamento: null, codigo: '', descripcion: '' },
      ciudad: { id: null, departamento: null, codigo: '', descripcion: '' },
      celular: '',
      direccion: '',
      email: '',
      tipoIdentificacion: { id: null, codigo: '', detalle: '' },
      ciudadUbicacion: { id: null, departamento: null, codigo: '', descripcion: '' },
      identificacion: '',
      rutaFoto: '',
      telefonoFijo: '',

    };



    this.asignacion = {

      fechaInicio: null,
      fechaFin: null,
      idestado: null,
      idrol: null,
      idpersona: null


    }


    this.matricula = {

      id: null,
      fecha: null,
      idacudiente: null,
      idpersona: null,
      idprestacionsocia: null,
      idprograma: null,


    }
    this.acudiente = {

      id: null,
      nombres: '',
      apellidos: '',
      identificacion: '',
      telmovil: '',
      telfijo: '',
      email: '',
      idparentesco: null,


    }
    this.documento = {

      id: null,
      idmatricula: null,
      idTipoDocumento: null,
      file: null,

    }

    this.departamento = {
      id: null,
      descripcion: '',

    };
    this.departamentoU = {
      id: null,
      descripcion: '',
    };

    this.tipoIdentificacionR = {
      id: null,
      codigo: '',
      detalle: ''
    };
    this.ciudad = null;
    this.ciudadU = { id: null, departamento: null, codigo: '', descripcion: '' };


    this.buildForm();




  }

  ngOnInit(): void {
    this.traerDepartamentos();
    this.traerCiudadesByDep();
    this.traerTipoId();
    // this.traerTipoContrato();
    this.traerPersonasById();
    this.traerRol();
    this.traerParentesco();
    this.traerPrograma();
    this.traerTiposD();
    this.traerPeriodosByProg();
    this.index = 0;

  }

  validarSteps() {
    if (this.index == 0) {


      return this.formPersona.valid

    } else if (this.index == 1) {
      return this.formAcudiente.valid

    } else if (this.index == 2) {
      return this.formMatricula.valid



    }


    return false;

  }

  guardarPersona(personaModel: PersonaModel) {
    // alert(personaModel);

    const pos = this.objPersona.indexOf(this.objPersona.find(persona => persona.id === this.perIdUpdate));
    if (this.formPersona.valid) {
      if (this.persona.id === null) {

        console.log(personaModel);
        this.persona.ciudadNac = this.ciudad;
        this.persona.ciudadUbicacion = this.ciudadU;
        this.persona.tipoIdentificacion = this.tipoIdentificacionR;
        this.personaService.crearPersona(personaModel)
          .subscribe(newPer => {
            this.per = newPer;
            this.idPersona = this.per.idpersona;
            console.log(this.idPersona, 'persona creada id ');
            this.asignacion.idpersona = this.idPersona;
            this.asignacion.idrol = this.objRol.id;
            this.guardarAcudiente(this.acudiente);
            this.guardarAsignacion(this.asignacion);
            // this.alerts.push({
            //   type: 'success',
            //   msg: '¡Proceso exitoso! El registro fue creado con éxito.',
            //   timeout: 2000,
            //   msgStr: 'Proceso Exitoso!'
            // });

            this.formPersona.reset();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser creado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });

      } else {
        this.persona.ciudadNac = this.ciudad;
        this.persona.ciudadUbicacion = this.ciudadU;
        this.persona.tipoIdentificacion = this.tipoIdentificacionR;
        this.personaService.actualizarPersona(this.persona)
          .subscribe(funcUpdate => {
            //  this.traerFuncionalidad();
            // this.objFunc[ pos ] = <FuncionalidadModel>funcUpdate ;
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
              timeout: 1000,
              msgStr: 'Proceso Exitoso!'
            });
            // this.closeModal();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser actualizado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });
      }
    }

  }

  guardarAsignacion(asignacion: AsignacionRolModel) {

    this.asignacion.idpersona = this.idPersona;
    console.log(this.objRol.id, 'idviene');
    this.asignacion.idrol = this.objRol.id;
    this.asignacionService.crearAsignacion(asignacion)
      .subscribe(newAsig => {
        // this.alerts.push({
        //   type: 'success',
        //   msg: '¡Proceso exitoso! El registro fue creado con éxito.',
        //   timeout: 2000,
        //   msgStr: 'Proceso Exitoso!'
        // });
        // this.formPersona.reset();
        // this.traerFuncionalidad();
        // this.objFunc.push(newFunc);
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: '¡Ups! El registro no puede ser creado.',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });

  }


  guardarAcudiente(acudiente: AcudienteModel) {
    // alert(personaModel);

    // const pos = this.objAcudiente.indexOf(this.objAcudiente.find(acudiente => acudiente.id === this.perIdUpdate));
    if (this.formAcudiente.valid) {
      if (this.acudiente.id === null) {

        // console.log(personaModel);
        this.acudienteService.crearAcudiente(acudiente)
          .subscribe(newAcu => {

            this.idAcudiente = newAcu.id;
            // console.log(this.idPersona, 'persona creada id ');
            // this.alerts.push({
            //   type: 'success',
            //   msg: '¡Proceso exitoso! El registro fue creado con éxito.',
            //   timeout: 2000,
            //   msgStr: 'Proceso Exitoso!'
            // });
            this.matricula.idpersona = this.idPersona;
            this.matricula.idacudiente = this.idAcudiente;

            this.guardarMatricula(this.matricula);


            this.formAcudiente.reset();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser creado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });

      } else {
        this.acudienteService.actualizarAcudiente(this.acudiente)
          .subscribe(funcUpdate => {
            //  this.traerFuncionalidad();
            // this.objFunc[ pos ] = <FuncionalidadModel>funcUpdate ;
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
              timeout: 1000,
              msgStr: 'Proceso Exitoso!'
            });
            // this.closeModal();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser actualizado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });
      }
    }

  }


  guardarMatricula(matricula: MatriculaModel) {
    // alert(personaModel);

    // console.log(this.idPersona, 'personaContrato');
    const pos = this.objMatricula.indexOf(this.objMatricula.find(func => func.id === this.perIdUpdate));
    if (this.formMatricula.valid) {
      if (this.matricula.id === null) {

        // this.contrato.idpersona = this.persona.id;
        // console.log(this.idPersona,'idpersonaencontrato')
        this.matriculaService.crearMatricula(matricula)
          .subscribe(newMat => {
            this.idMatricula = newMat.id;
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! El registro fue creado con éxito.',
              timeout: 2000,
              msgStr: 'Proceso Exitoso!'
            });
            this.documento.idmatricula = this.idMatricula;
            this.guardarDocumento(this.documento);
            this.formMatricula.reset();
            this.resetWizard();
            // this.traerFuncionalidad();
            // this.objFunc.push(newFunc);
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser creado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });

      } else {
        this.matriculaService.actualizarMatricula(matricula)
          .subscribe(funcUpdate => {
            //  this.traerFuncionalidad();
            // this.objFunc[ pos ] = <FuncionalidadModel>funcUpdate ;
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
              timeout: 1000,
              msgStr: 'Proceso Exitoso!'
            });
            // this.closeModal();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser actualizado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });
      }
    }

  }
  guardarDocumento(documento: DocumentoMatriculaModel) {
    // alert(personaModel);

    // console.log(this.idPersona, 'personaContrato');
    // const pos = this.objDocumento.indexOf(this.objMatricula.find(func => func.id === this.perIdUpdate));
    if (this.formDocumento.valid) {
      if (this.documento.id === null) {

        // this.contrato.idpersona = this.persona.id;
        // console.log(this.idPersona,'idpersonaencontrato')
        this.documentoMatriculaService.crearDocumento(documento)
          .subscribe(newMat => {
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! El registro fue creado con éxito.',
              timeout: 2000,
              msgStr: 'Proceso Exitoso!'
            });
            this.formDocumento.reset();
            // this.traerFuncionalidad();
            // this.objFunc.push(newFunc);
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser creado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });

      } else {
        this.matriculaService.actualizarMatricula(this.matricula)
          .subscribe(funcUpdate => {
            //  this.traerFuncionalidad();
            // this.objFunc[ pos ] = <FuncionalidadModel>funcUpdate ;
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
              timeout: 1000,
              msgStr: 'Proceso Exitoso!'
            });
            // this.closeModal();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser actualizado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });
      }
    }

  }


  compare(a:any,b:any){

    return a&&b&&a.id===b.id
  }


  //Datos de la persona
  get idtipoIdentificacionField() {
    return this.formPersona.get('tipoIdentificacion');
  }
  get identificacionField() {
    return this.formPersona.get('identificacion');
  }
  get nombre1Field() {
    return this.formPersona.get('nombre1');
  }

  get apellido1Field() {
    return this.formPersona.get('apellido1');
  }

  get idciudadNacField() {
    return this.formPersona.get('ciudadNac');
  }
  get departamentoField() {
    return this.formPersona.get('departamento');
  }
  get fechaNacField() {
    return this.formPersona.get('fechaNac');
  }

  // datos ubicacion
  get ciudadField() {
    return this.formUbicacion.get('ciudad');
  }
  get departamentoUField() {
    return this.formUbicacion.get('departamentoU');
  }
  get direccionField() {
    return this.formUbicacion.get('direccion');
  }
  get emailField() {
    return this.formUbicacion.get('email');
  }
  get celularField() {
    return this.formUbicacion.get('celular');
  }
  get telefonoFijoField() {
    return this.formUbicacion.get('telefonoFijo');

  }
  get idciudadUbicacionField() {
    return this.formUbicacion.get('ciudadUbicacion');
  }

  // datos acudiente
  get nombresField() {
    return this.formAcudiente.get('nombres');
  }
  get apellidosField() {
    return this.formAcudiente.get('apellidos');
  }
  get identificacionaField() {
    return this.formAcudiente.get('identificacion');
  }
  get emailaField() {
    return this.formAcudiente.get('email');
  }
  get telMovilField() {
    return this.formAcudiente.get('telMovil');
  }
  get telFijoField() {
    return this.formAcudiente.get('telFijo');

  }
  get idparentescoField() {
    return this.formAcudiente.get('idparentesco');

  }


  //datos matricua
  get fechaField() {
    return this.formMatricula.get('fecha');
  }
  get idprestacionsociaField() {
    return this.formMatricula.get('idprestacionsocia');
  }
  get idprogramaField() {
    return this.formMatricula.get('idprograma');
  }
  get idtipoDocumentoField() {
    return this.formDocumento.get('idtipoDocumento');
  }

  mostrar(){
    if(this.formPersona.valid){
      let el = document.querySelector(".disable");
         el.classList.remove("disable");
      }
  }

  // datos del contrato


  traerCiudades() {
    this.ciudadService.traerCiudades()
      .subscribe(ciudad => {
        this.objCiudadDep = ciudad;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }

  traerTipoId() {
    this.personaService.traerTiposId()
      .subscribe(tipoId => {
        this.objTipoId = tipoId;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }
  traerTiposD() {
    this.documentoMatriculaService.traerTiposDocumento()
      .subscribe(tipoDoc => {
        this.objTipoDoc = tipoDoc;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }
  traerPrograma() {
    this.programaService.traerPrograma()
      .subscribe(prog => {
        this.objProg = prog;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }
  traerParentesco() {
    this.parentescoService.traerParentesco()
      .subscribe(par => {
        this.objPar = par;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }




  traerPeriodosByProg() {
    console.log(document.getElementById("idprograma")['value']);

    this.programaService.periodosByProg(document.getElementById("idprograma")['value'])
      .subscribe((periodo: PeriodoModel) => {
        this.objPeriodo = periodo;
        console.log(this.objPeriodo,'ultimo periodo');
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });

  }
  traerAcudiente(idPersona:number) {

    this.personaService.getAcudiente(idPersona)
      .subscribe((acudiente: AcudienteModel) => {
        // this.objPeriodo = periodo;

        this.acudiente={
          id: acudiente[0].id,
          nombres:acudiente[0].nombres,
          apellidos:acudiente[0].apellidos,
          email:acudiente[0].email,
          identificacion:acudiente[0].identificacion,
          idparentesco:acudiente[0].idparentesco,
          telfijo:acudiente[0].telfijo,
          telmovil:acudiente[0].telmovil

        }
        console.log(acudiente,'acudiente');
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });

  }

  setDate(dateS: Date): void {
    console.log(dateS);
    // Set today date using the patchValue function
    let date = new Date(dateS);
    this.formPersona.patchValue({
      fechaNac: {
        date: {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate() + 1
        }
      }
    });
  }

  traerPersonasById() {
    console.log(document.getElementById("identificacion")['value'], '----***');
    this.personaService.personaByIdentificacion(document.getElementById("identificacion")['value'])
      .subscribe((persona: PersonaModel[]) => {
        // this.objPersonaId = persona;
        // this.persona=persona;
        this.setDate(persona[0].fechaNac);
        this.persona = {

          id: persona[0].id,
          nombre1: persona[0].nombre1,
          apellido1: persona[0].apellido1,
          nombre2: persona[0].nombre2,
          apellido2: persona[0].apellido2,
          fechaNac: persona[0].fechaNac,
          ciudadNac: persona[0].ciudadNac,
          idciudad: 1,
          celular: persona[0].celular,
          direccion: persona[0].direccion,
          email: persona[0].email,
          tipoIdentificacion: persona[0].tipoIdentificacion,
          ciudadUbicacion: persona[0].ciudadUbicacion,
          identificacion: persona[0].identificacion,
          rutaFoto: persona[0].rutaFoto,
          telefonoFijo: persona[0].telefonoFijo,


        };

        // this.formPersona.controls['departamento'].setValue(null);
        this.departamento = persona[0].ciudadNac.departamento;
        this.ciudad = persona[0].ciudadNac;
        this.departamentoU = persona[0].ciudadUbicacion.departamento;
        this.ciudadU = persona[0].ciudadUbicacion;
        this.tipoIdentificacionR = persona[0].tipoIdentificacion;
        this.fechaPrevia = persona[0].fechaNac;
        this.traerCiudadesByDep();
        this.traerCiudadesByDepU();

        this.traerAcudiente(this.persona.id);
        // this.formPersona.controls['departamento'].setValue(persona[0].ciudadNac.departamento);
        // this.setDatosPersona(persona);
        console.log(persona);
        console.log(this.persona, 'llego persona---++++');
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
    // this.traerCiudadesByDepU();
  }





  traerCiudadesByDep() {
    console.log(document.getElementById("departamento")['value']);
    if (document.getElementById("departamento")['value']) {
      this.ciudadService.ciudadesByDep(this.departamento.id)
        .subscribe((ciudad: CiudadModel[]) => {
          this.objCiudadDep = ciudad;
          console.log(this.objCiudadDep);
        },
          error => {
            this.alerts.push({
              type: 'danger',
              msg: 'Error de conexion',
              timeout: 5000,
              msgStr: 'Ups!'
            });
          });

    } else {
      this.traerCiudades();
    }
  }

  traerCiudadesByDepU() {
    // console.log(document.getElementById("departamentoU")['value'],'id dep');
    // this.ciudadService.ciudadesByDep(document.getElementById("departamentoU")['value'])
    this.ciudadService.ciudadesByDep(this.departamentoU.id)
      .subscribe((ciudad: CiudadModel[]) => {
        this.objCiudadDepU = ciudad;
        console.log(this.objCiudadDepU);
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }



  traerDepartamentos() {
    this.departamentoService.traerDepartamentos()
      .subscribe(dep => {
        this.objDep = dep;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }

  traerRol() {
    this.rolService.rolBynombre('ESTUDIANTE')
      .subscribe((rol: RolModel) => {
        this.objRol = rol;
        console.log(rol.id,'rol estudisnte ');
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }



  private buildForm() {



    this.formPersona = this.formBuilder.group({

      id: [0],
      tipoIdentificacion: [null, [Validators.required]],
      identificacion: ['', [Validators.required]],
      nombre1: ['', [Validators.required]],
      nombre2: ['', []],
      apellido1: ['', [Validators.required]],
      apellido2: ['', []],
      departamento: [null, [Validators.required]],
      ciudadNac: [null, [Validators.required]],
      fechaNac: ['', [Validators.required]],
      rutaFoto: [''],
      // idpersona: ['', [Validators.required]],

    });


    this.formUbicacion = this.formBuilder.group({

      idciudad: ['',],
      celular: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      email: ['', [Validators.required]],
      telefonoFijo: ['', [Validators.required]],
      ciudadUbicacion: [null, [Validators.required]],
      departamentoU: [null, [Validators.required]],




    });

    this.formAcudiente = this.formBuilder.group({

      nombres: ['', [Validators.required]],
      apellidos: ['', [Validators.required]],
      identificacion: ['', [Validators.required]],
      email: ['', [Validators.required]],
      telFijo: ['', [Validators.required]],
      telMovil: ['', [Validators.required]],
      idparentesco: ['', [Validators.required]],
      // departamentoU: ['', [Validators.required]],




    });

    this.formMatricula = this.formBuilder.group({


      //contrato
      fecha: ['', [Validators.required]],
      // idacudiente: ['', [Validators.required]],
      // idprestacionsocia: ['', [Validators.required]],
      idprograma: ['', [Validators.required]],

    });

    this.formDocumento = this.formBuilder.group({


      //contrato
      // fecha: ['', [Validators.required]],
      // idacudiente: ['', [Validators.required]],
      // idprestacionsocia: ['', [Validators.required]],
      idtipoDocumento: ['', [Validators.required]],

    });




    console.log(this.formPersona.valid, '--validper')

    this.formPersona.valueChanges
      .pipe(
        debounceTime(350),
      )
      .subscribe(data => {

        console.log(data, 'kjkjkjk');
      });
  }



  onFileChange(files: FileList) {
    console.log(files);
  }

  showPreviousStep(event?: Event) {
    this.ngWizardService.previous();
  }

  showNextStep(event?: Event) {
    alert(event);
    this.ngWizardService.next();

  }

  resetWizard(event?: Event) {

    this.ngWizardService.reset();
  }

  setTheme(theme: THEME) {
    this.ngWizardService.theme(theme);
  }

  stepChanged(args: StepChangedArgs) {


    // console.log(args.step.index)
    this.index = args.step.index;
    // this.buildForm();
  }

}
