import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultLayoutComponent } from '../containers';
import {MenuComponent} from './menu/components/menu/menu.component';
import {InfraestructuraComponent} from './infraestructura/components/infraestructura/infraestructura.component';
// import { PermisosComponent } from "./permisos/components/PermisosComponent";
import { RolesComponent } from './roles/components/roles/roles.component';
import { PermisosComponent } from './permisos/components/permisos.component';
import { TipoContratoComponent } from './tipoContrato/components/tipo-contrato/tipo-contrato.component';
import { TipoActividadComponent } from './tipoActividad/components/tipo-actividad/tipo-actividad.component';
import {MedioPagoComponent} from './medio-pago/components/medio-pago/medio-pago.component';
import {TipoPagoComponent} from './tipo-pago/components/tipo-pago/tipo-pago.component';

import { ModuloComponent } from './modulo/components/modulo/modulo.component';
import { ProcesoComponent } from './proceso/components/proceso/proceso.component';
import { TipoRolComponent } from './tipo_rol/components/tipo-rol/tipo-rol.component';
import { TipoTransaccionComponent } from './tipo_transaccion/components/tipo-transaccion/tipo-transaccion.component';
import { ClaseProductoComponent } from './claseProducto/components/clase-producto/clase-producto.component';
import { TipoProductoComponent } from './tipoProducto/components/tipo-producto/tipo-producto.component';
import { TipoRespuestaComponent } from './tipoRespuesta/components/tipo-respuesta/tipo-respuesta.component';
import { PeriodoMatriculaComponent } from './periodoMatricula/components/periodo-matricula/periodo-matricula.component';
import { DocumentoMatriculaComponent } from './documento_matricula/components/documento-matricula/documento-matricula.component';
import { ClasificacionListComponent } from './clasificacion/components/clasificacion-list/clasificacion-list.component';
// import { MateriaComponent } from './materia/components/materia/materia.component';
import { DocumentoActividadComponent } from './documento_actividad/components/documento-actividad/documento-actividad.component';
import { TipoProgramaListComponent } from './tipo_programa/components/tipo-programa-list/tipo-programa-list.component';
import { FuncionalidadComponent } from './funcionalidad/components/funcionalidad/funcionalidad.component';
import { ConfigEmpresaComponent } from './config-empresa/componentes/config-empresa/config-empresa.component';
import { ContratacionComponent } from './contratacion/components/contratacion/contratacion.component';
import { ConfigPensumComponent } from './config-pensum/components/config-pensum/config-pensum.component';
import { MatriculaComponent } from './matricula/components/matricula/matricula.component';
import { MatriculaEstudiantilComponent } from './matricula-estudiantil/components/matricula-estudiantil/matricula-estudiantil.component';
import { TrayectoriaEstudianteComponent } from './trayectoria-estudiante/components/trayectoria-estudiante/trayectoria-estudiante.component';

import { ListTipoContratoComponent } from './tipoContrato/components/list-tipo-contrato/list-tipo-contrato.component';
import { RolesListComponent } from './roles/components/roles-list/roles-list.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'plantilla/add-menu',
    pathMatch: 'full',
  },
{
  path: 'plantilla',
  component: DefaultLayoutComponent,

  data: {
    title: 'Home'
  },
  children: [
    {
      path: 'add-menu',
      component: MenuComponent
    },
    {
      path: 'gestion-tipopago',
      component: TipoPagoComponent
    },
    {
      path: 'gestion-mediopago',
      component: MedioPagoComponent
    },
    {
      path: 'gestion-infraestructura',
      component: InfraestructuraComponent
    },
    {
      path: 'gestion-clasificacion',
      component: ClasificacionListComponent
    },
    // {
    //   path: 'gestion-materia',
    //   component: MateriaComponent
    // },
    {
      path: 'permisos',
      component: PermisosComponent
    },
    {
      path: 'tipo-contrato',
      component: ListTipoContratoComponent
    },
    {
      path: 'tipo-actividad',
      component: TipoActividadComponent
    },
    {
      path: 'clase-producto',
      component: ClaseProductoComponent
    },
    {
      path: 'tipo-producto',
      component: TipoProductoComponent
    },
    {
      path: 'periodo-matricula',
      component: PeriodoMatriculaComponent
    },
    {
      path: 'contratacion',
      component: ContratacionComponent
    },
    {
      path: 'matricula',
      component: MatriculaComponent
    },
    {
      path: 'tipo-respuesta',
      component: TipoRespuestaComponent
    },
    {
      path: 'funcionalidad',
      component: FuncionalidadComponent
    },
    {
      path: 'add-roles',
      component: RolesListComponent
    },
    {
      path: 'add-modulo',
      component: ModuloComponent
    },
    {
      path: 'add-proceso',
      component: ProcesoComponent
    },
    {
      path: 'add-tipo_rol',
      component: TipoRolComponent
    },
    {
      path: 'add-tipo_transaccion',
      component: TipoTransaccionComponent,
    },
    {
      path: 'add-documento_matricula',
      component: DocumentoMatriculaComponent,
    },
    {
      path: 'add-documento_actividad',
      component: DocumentoActividadComponent,
    },
    {
      path: 'add-tipo_programa',
      component: TipoProgramaListComponent,
    },
    {
      path: 'add-gestion-empresa',
      component: ConfigEmpresaComponent,
    },
    {
      path: 'gestion-pensum',
      component: ConfigPensumComponent
    },
    {
      path: 'gestion-matricula-estudiante',
      component: MatriculaEstudiantilComponent
    },
    {
      path: 'gestion-trayectoria-estudiante',
      component: TrayectoriaEstudianteComponent
    },
   ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
