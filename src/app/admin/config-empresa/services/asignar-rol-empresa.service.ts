import { Injectable } from '@angular/core';
import { AsignarRolEmpresaModel } from '../models/asignar-rol-empresa.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AsignarRolEmpresaService {
  rolEmpresa: AsignarRolEmpresaModel;
  url = 'http://localhost:8080/inventario';
  httOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  crearRolEmpresa(rolEmpresa: AsignarRolEmpresaModel) {
    return this.httpClient.post<AsignarRolEmpresaModel>(this.url + '/rolempresa', rolEmpresa, this.httOptions);
  }

  eliminarEmpresa(empresaId: number, rolId: number) {
    return this.httpClient.delete(this.url + '/eliminarrolempresa', this.httOptions);
  }

  actualizarEmpresa(empresaId: number, rolId: number, rolEmpresa: AsignarRolEmpresaModel) {
    return this.httpClient.put<AsignarRolEmpresaModel>(this.url + '/actualizarrolempresas', this.httOptions);
  }
}
