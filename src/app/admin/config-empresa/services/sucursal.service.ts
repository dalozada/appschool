import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { SucursalModel } from './../models/sucursal.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SucursalService {
  sucursal: SucursalModel;
  url = 'http://localhost:8080/inventario';
  permisos: number;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };

  constructor(
    private httpClient: HttpClient
  ) { }

  public traerSucursal() {
    return this.httpClient.get<SucursalModel[]>(this.url + '/sucursales', this.httpOptions)
    .pipe(map(data => data));
  }
  crearSucursal(sucursal: SucursalModel) {
    return this.httpClient.post<SucursalModel>(this.url + '/guardarsucursal', sucursal, this.httpOptions);
  }

  eliminarSucursal(SucursalId: number) {
    return this.httpClient.post(this.url + '/eliminarsucursal', SucursalId, this.httpOptions);
  }
  actualizarSucursal(sucursal: SucursalModel) {
    sucursal.nombreSucursal = sucursal.nombreSucursal.toUpperCase();
    sucursal.direccion = sucursal.direccion.toUpperCase();
    sucursal.telefono = sucursal.telefono;
    sucursal.celular = sucursal.celular;
    sucursal.ciudad.descripcion = sucursal.ciudad.descripcion;
    sucursal.ciudad.departamento.id = sucursal.ciudad.departamento.id;
    return this.httpClient.put<SucursalModel>(this.url + '/actuarlizarsucursales/' + sucursal.id, sucursal, this.httpOptions);
  }
}

