import { TestBed } from '@angular/core/testing';

import { ConfigEmpresaService } from './config-empresa.service';

describe('ConfigEmpresaService', () => {
  let service: ConfigEmpresaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfigEmpresaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
