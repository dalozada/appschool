import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CiudadModel } from '../models/ciudad.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CiudadService {
  ciudad: CiudadModel;
  url = 'http://localhost:8080/virtualt/';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerCiudades() {
     return this.httpClient.get<CiudadModel[]>(this.url + '/ciudades', this.httpOptions)
     .pipe(map(data => data));
  }

  public ciudadesByDep(iddepartamento: number) {
    console.log('id depto', iddepartamento);
    const url = `${this.url}ciudades/departamento/${iddepartamento}`;
    return this.httpClient.get<CiudadModel[]>(url, this.httpOptions)
    .pipe(map(data => data));

  }
}
