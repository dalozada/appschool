import {CiudadModel} from './ciudad.model';
import { EmpresaModel } from './empresa.model';
export interface SucursalModel {
  id: number;
  nombreSucursal: string;
  direccion: string;
  telefono: string;
  celular: string;
  principal: string;
  idciudad ?: number;
  idempresa ?: number;

  ciudad ?: CiudadModel;
  empresa ?: EmpresaModel;
}
