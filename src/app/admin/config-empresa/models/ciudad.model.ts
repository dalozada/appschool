import { DepartamentoModel } from '../../contratacion/models/departamento.model';

export interface CiudadModel {
  id: number;
  codigo: string;
  descripcion: string;
  iddepartamento ?: number;

  departamento ?: DepartamentoModel;
}
