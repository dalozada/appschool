import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { SucursalModel } from '../../../models/sucursal.model';
import { SucursalService } from '../../../services/sucursal.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-sucursal-list',
  templateUrl: './sucursal-list.component.html',
  styleUrls: ['./sucursal-list.component.css']
})
export class SucursalListComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<SucursalModel>();
  @Input() sucursales: SucursalModel[] = [];
  sucursalIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private sucursalService: SucursalService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {
 
  }
  openModalDelete(sucursalId: number) {
    this.dangerModal.show();
    this.sucursalIdDelete = sucursalId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(sucursal: SucursalModel) {
    this.openModalUpdate.emit(sucursal);
  }

  onDeleteSucursal() {

     const pos = this.sucursales.indexOf(this.sucursales.find( sucursal => sucursal.id === this.sucursalIdDelete));
     this.sucursalService.eliminarSucursal(this.sucursalIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El registro fue eliminado con éxito',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.sucursales.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El registro no puede ser eliminado',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
