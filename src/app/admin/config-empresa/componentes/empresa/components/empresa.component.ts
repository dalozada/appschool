import { Component, OnInit, Input } from '@angular/core';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { EmpresaModel } from '../../../models/empresa.model';
import { EmpresaService } from './../../../../services/empresa.service';
import { AsignarRolEmpresaModel } from '../../../models/asignar-rol-empresa.model';
@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {
  @Input() empresa: EmpresaModel;
  formEmpresa: FormGroup;
  rolEmpresa: AsignarRolEmpresaModel;
  objEmpresas: EmpresaModel[] = [];
  filesRuta: FileList;
  alerts: any[] = [];
  empresaIdUpdate: number;
  constructor(
    private empresaService: EmpresaService,
    private formBuilder: FormBuilder,
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
  }

    get nameEmpresaField() {
      return this.formEmpresa.get('razonSocial');
    }
    get nitEmpresaField() {
      return this.formEmpresa.get('nit');
    }
    get repLegField() {
      return this.formEmpresa.get('representanteLegal');
    }
    onFileChange(files: FileList) {
      this.filesRuta = files;

  }
  private buildForm() {
    this.formEmpresa = this.formBuilder.group({
      id: [null],
      razonSocial: ['', [Validators.required]],
      nit: ['', [Validators.required]],
      digitoVerificacion: [''],
      representanteLegal: ['', [Validators.required]],
      rutaLogo: [''],

    });

    this.formEmpresa.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarEmpresa() {
    const data = new FormData();
    data.append('razonSocial', this.formEmpresa.value.razonSocial.toUpperCase());
    data.append('nit', this.formEmpresa.value.nit);
    data.append('digitoVerificacion', this.formEmpresa.value.digitoVerificacion);
    data.append('representanteLegal', this.formEmpresa.value.representanteLegal.toUpperCase());
    data.append('rutaLogoFile', this.filesRuta[0]);
    const pos = this.objEmpresas.indexOf(this.objEmpresas.find( men => men.id === this.empresaIdUpdate));
    if (this.formEmpresa.valid) {
      if (this.empresa.id === null) {
        this.empresaService.crearEmpresa(data)
        .subscribe( newEmpresa => {
          this.empresa = newEmpresa;
          this.alerts.push({
            type: 'success',
            msg: 'El registro fue creado con éxito',
            timeout: 2000,
            msgStr: 'Procesos Exitoso'
          });

          this.rolEmpresa = {
            id: null,
            idrol: 5,
            fechaRegistro: '',
            idestado: 1,
            idempresa: this.empresa.id
          };
          this.empresaService.crearRolEmpresa(this.rolEmpresa)
          .subscribe(newRol => {
            console.log('rol guardado', newRol);
          });
          this.formEmpresa.disable();
          this.objEmpresas.push(newEmpresa);
        });
      } else {
        this.empresaService.actualizarEmpresa(this.empresa)
        .subscribe( empresaIdUpdate => {
          this.objEmpresas [ pos ] = <EmpresaModel>empresaIdUpdate;
          this.alerts.push({
            type: 'success',
            msg: 'La actualización se ha realizado con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso'
          });
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualización no pudo completarse',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}


