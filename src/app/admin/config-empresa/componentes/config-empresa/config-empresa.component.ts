import { Component, OnInit } from '@angular/core';
import { EmpresaModel } from '../../models/empresa.model';
import { EmpresaService } from './../../../services/empresa.service';
@Component({
  selector: 'app-config-empresa',
  templateUrl: './config-empresa.component.html',
  styleUrls: ['./config-empresa.component.css']
})
export class ConfigEmpresaComponent implements OnInit {
  empresa: EmpresaModel;
  constructor(
    private empresaService: EmpresaService
  ) {
    this.empresa = {
      id: null,
      razonSocial: '',
      nit: '',
      representanteLegal: '',
      digitoVerificacion: null,
      rutaLogo: '',
    };
  }

  ngOnInit(): void {
    this.empresaService.traerEmpresaPrincipal()
    .subscribe(empresa => {
      if (empresa !== null) {
        this.empresa = empresa;
      }
    });
  }

}
