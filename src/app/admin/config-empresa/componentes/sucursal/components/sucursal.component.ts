import { Component, OnInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { SucursalModel } from '../../../models/sucursal.model';
import { SucursalService } from '../../../services/sucursal.service';
import { CiudadModel } from './../../../models/ciudad.model';
import { CiudadService } from './../../../services/ciudad.service';
import { DepartamentoModel } from './../../../models/departamento.model';
import { DepartamentoService } from './../../../services/departamento.service';
import { AlertComponent } from 'ngx-bootstrap/alert';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';
import { EmpresaModel } from '../../../models/empresa.model';

@Component({
  selector: 'app-sucursal',
  templateUrl: './sucursal.component.html',
  styleUrls: ['./sucursal.component.css']
})
export class SucursalComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  @Input() empresa: EmpresaModel;
  formSucursal: FormGroup;
  modalRef: BsModalRef;
  sucursal: SucursalModel;
  objSucursales: SucursalModel[] = [];
  departamento: DepartamentoModel;
  ciudad: CiudadModel;
  objCiudad: CiudadModel[] = [];
  objCiudadDep: CiudadModel[] = [];
  objDep: DepartamentoModel[] = [];
  alerts: any[] = [];
  sucursalIdUpdate: number;
  constructor(
    private sucursalService: SucursalService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private ciudadService: CiudadService,
    private departamentoService: DepartamentoService,
    ) {

      this.sucursal = {
        id: null,
        nombreSucursal: '',
        direccion: '',
        telefono: '',
        celular: '',
        ciudad: {
          id: null,
          descripcion: '',
          codigo: '',
          departamento: {
            id: null,
            descripcion: '',
          }
        },
        empresa: this.empresa,
        principal: ''
              };
      this.buildForm();
    }

  ngOnInit(): void {
    this.traerDepartamentos();
    this.sucursalService.traerSucursal()
     .subscribe(sucursal => {
       this.objSucursales = sucursal;
     },
     error => {
       this.alerts.push({
         type: 'danger',
         msg: 'Error de conexion',
         timeout: 5000,
         msgStr: 'Ups!'
     });
   });

  }
  get nameSucursalField() {
    return this.formSucursal.get('nombreSucursal');
  }
  get direccionSucursalField() {
    return this.formSucursal.get('direccion');
  }
  get telefonoSucursalField() {
    return this.formSucursal.get('telefono');
  }
  get celularSucursalField() {
    return this.formSucursal.get('celular');
  }
  get departamentoSucursalField() {
    return this.formSucursal.get('departamento');
  }
  get ciudadSucursalField() {
    return this.formSucursal.get('ciudad');
  }
  get principalSucursalField() {
    return this.formSucursal.get('principal');
  }

  traerCiudadesByDep(depto: DepartamentoModel) {
    console.log(this.departamento.id);
    this.ciudadService.ciudadesByDep(this.departamento.id)
    .subscribe(ciudades => {
      this.objCiudadDep = ciudades;
    });
  }
  traerDepartamentos() {
    this.departamentoService.traerDepartamentos()
    .subscribe(dep => {
      this.objDep = dep;
    });
  }
  compareDpto(a,b) {
    return a && b && a.id === b.id;
  }

  openModalSucursal() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.modalRef.hide();

}
  openModalSucursalUpdate(sucursalObj: SucursalModel) {
    this.sucursal = {
    id: sucursalObj.id,
    nombreSucursal: sucursalObj.nombreSucursal.toUpperCase(),
    direccion: sucursalObj.direccion.toUpperCase(),
    telefono: sucursalObj.telefono,
    celular: sucursalObj.celular,
    idciudad: sucursalObj.ciudad.id,
    principal: sucursalObj.principal,
    idempresa: sucursalObj.empresa.id,
    ciudad: sucursalObj.ciudad,
    empresa: sucursalObj.empresa
  };
  this.departamento = sucursalObj.ciudad.departamento;
  this. traerCiudadesByDep(this.departamento);
  this.ciudad = sucursalObj.ciudad;
  console.log('objeto actualizar', this.sucursal.ciudad.departamento);
     this.sucursalIdUpdate = sucursalObj.id;
     this.modalRef = this.modalService.show(this.templatePr);
     this.ciudadService.ciudadesByDep(this.sucursal.ciudad.departamento.id)
     .subscribe(ciudades => {
       const i = 0;
       for (const ciudad of ciudades) {
            ciudad.departamento = this.sucursal.ciudad.departamento;
            ciudades [ i ] = ciudad;
       }
       console.log( 'array', this.sucursal.ciudad.departamento);
     });

  }
  private buildForm() {

    this.formSucursal = this.formBuilder.group({
      id: [0],
      nombreSucursal: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      telefono: [''],
      celular: [''],
      departamento: [null, [Validators.required]],
      ciudad: [null, [Validators.required]],
      principal: ['N', [Validators.required]],
      empresa: [null],
    });

    this.formSucursal.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarSucursal() {
    this.sucursal = {
          id: this.formSucursal.value.id,
          nombreSucursal: this.formSucursal.value.nombreSucursal.toUpperCase(),
          direccion: this.formSucursal.value.direccion.toUpperCase(),
          telefono: this.formSucursal.value.telefono,
          celular: this.formSucursal.value.celular,
          idciudad: this.formSucursal.value.ciudad.id,
          principal: this.formSucursal.value.principal,
          idempresa: this.empresa.id,
        };
     this.departamento = this.objDep.find(dep => dep.id === parseInt ( this.formSucursal.value.departamento , 10));
     this.ciudad = this.objCiudad.find(ciu => ciu.id === parseInt ( this.formSucursal.value.idciudad , 10));
    const pos = this.objSucursales.indexOf(this.objSucursales.find( sucursales => sucursales.id === this.sucursalIdUpdate));
    if (this.formSucursal.valid) {
      if (this.sucursal.id === null ) {
        this.sucursalService.crearSucursal(this.sucursal)
       .subscribe( newSucursal => {
        this.alerts.push({
          type: 'success',
          msg: 'El registro fue creado con éxito',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.objSucursales.push(
          Object.assign({}, {'ciudad': newSucursal.ciudad, 'empresa': this.empresa}, newSucursal));
          console.log(newSucursal.ciudad, 'sucursales');
          console.log(this.formSucursal.value.ciudad);
          this.formSucursal.reset();
          this.sucursal = {
            id: null,
            nombreSucursal: '',
            direccion: '',
            telefono: '',
            celular: '',
            ciudad: {
              id: null,
              descripcion: '',
              codigo: '',
              departamento: {
                id: null,
                descripcion: '',
              }
            },
            empresa: this.empresa,
            principal: ''
                  };
      },

      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.sucursalService.actualizarSucursal(this.formSucursal.value)
         .subscribe( sucursalUpdate => {
          this.objSucursales[ pos ] = Object.assign({}, {'ciudad': this.formSucursal.value.ciudad, 'empresa': this.empresa}, sucursalUpdate);
           this.alerts.push({
            type: 'success',
            msg: 'La actualizacion se ha realizado con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualizacion no pudo completarse',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
