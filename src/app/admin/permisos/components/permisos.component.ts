import { Component, OnInit } from '@angular/core';
// import { MenuService } from '../services/menu.service';
import { FuncionalidadService } from '../../funcionalidad/services/funcionalidad.service';
import { FuncionalidadModel } from '../../funcionalidad/models/funcionalidad.model';
// import { MenuModel } from '../models/menu.model';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { PermisosService } from '../services/permisos.service';
import { MenuService } from '../../menu/services/menu.service';
import { MenuModel } from '../../menu/models/menu.model';
//import { RolesService } from '../../services/roles.service';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';
import { RolesService } from '../../roles/services/roles.service';
import { RolModel } from '../../roles/components/models/rol.model';


@Component({
  selector: 'app-permisos',
  templateUrl: './permisos.component.html',
  styleUrls: ['./permisos.component.css']
})
export class PermisosComponent implements OnInit {
  pageActual: number = 1;
  objRol: RolModel[] = [];

  constructor(
    private rolService:RolesService,
    private funcionalidadService: FuncionalidadService,
    private menuService: MenuService,
    private formBuilder: FormBuilder,
    private permisosService: PermisosService
  ) {
    this.fun= new Array();
    this.enviarNumeroRegistros(10);

  }

  ngOnInit(): void {
    this.traerfunc();
    this.menusByrol();
    this.rolService.traerRol()
    .subscribe(rol => {
      // alert(this.objRol);
      this.objRol = rol;
      console.log(rol,'--roles',this.objRol);
    });
  }

  public menus: MenuModel[];
  public func: FuncionalidadModel[];
  public fun: number[];
  public roles: number[];
  formMenu: FormGroup;
  numReg: number;
  alerts: any[] = [];



  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }


  traerfunc() {
    this.funcionalidadService.traerFuncionalidad().subscribe((data: any) => {
      console.log(data, '-------');
      this.func = data;
    },
      error => {
        console.log('There was an error while retrieving data !!!', error);
      });
  }

//     traerRoles() {
//     this.rolService.traerRol().subscribe((data: any) => {
//       console.log(data,'roles');
//       this.roles = data;
//     },
//     error => {
//       console.log('There was an error while retrieving data !!!', error);
//     });
// }
  /* permiso=false; */

  menusByrol() {
    /* alert(document.getElementById("rol")['value']); */
    this.menuService.menusByRol(document.getElementById("rol")['value']).subscribe((data: any) => {
    // this.menuService.menusByRol(1).subscribe((data: any) => {
      console.log(data, 'data2');

      this.menus = data;
      /*  if(data.permiso){
         this.permiso=true;
       } */

    },
      error => {
        console.log('There was an error while retrieving data !!!', error);
      });
  }

  form: FormGroup = new FormGroup({

    rol: new FormControl('', Validators.required)
  });

  toogleInput(idfuncionalidad: number) {
    console.log(idfuncionalidad,'--idfuncionalidad--');

    const index = this.fun.indexOf(idfuncionalidad);

    if (index === -1) {
      this.fun.push(idfuncionalidad);
      console.log(this.fun,'---')
    } else{
      this.fun.splice(index, 1);
    }


    return this.fun

  }


  guardarPermiso() {
    console.log(this.fun,'-----funcionalidades****')
    if(this.fun.length !== 0){
      this.menus.forEach((value)=> {

        console.log(value);
        value['funcionalidades'].forEach((idFun)=> {
          if(idFun.permiso==true){

            this.fun.push(idFun.funcionalidad.id);
          }
        });
      });

      // this.fun=Object.assign([],this.menus,this.fun);
    if (this.form.valid) {
      let obj: any = new Object();
      obj.idRol = this.form.value.rol;
      // obj.idRol = 1;
      obj.funciones = this.fun;
      console.log(obj,'-----');
      this.permisosService.guardar(obj).subscribe((data: any) => {
        console.log(data);
        // alert('muy bien');
        this.alerts.push({
          type: 'success',
          msg: 'Se guardo la configuración exitosamente ',
          timeout: 3000,
          msgStr: 'Proceso Exitoso!'
        });
        this.form.reset();
        this.fun=[];
        // window.location.reload();
      },
        error => {
          console.log('There was an error while retrieving data !!!', error);
        });
    } else {
      this.alerts.push({
        type: 'danger',
        msg: 'Debe seleccionar un rol',
        timeout: 3000,
        msgStr: 'Error!'
      });
      // alert('Por favor revise los campos');
    }
  }else{
    this.alerts.push({
      type: 'danger',
      msg: 'No hay Cambios',
      timeout: 3000,
      msgStr: 'Error!'
    });
  }


  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

  get rol() { return this.form.get('rol') }



}

// public roles:any[];
//   public func:any[];
//   public menus:any[];
//   public fun:any[];



//   ngOnInit() {
//     this.traerRoles();
//     this.traerfunc();
//     this.menusByrol();
//     this.initializeFormGroup();

//   }

//   traerRoles(){
//     this.rolService.traerRoles().subscribe((data: any) => {
//       console.log(data);
//       this.roles = data;
//     },
//     error => {
//       console.log('There was an error while retrieving data !!!', error);
//     });
// }

// traerfunc(){
//   this.funcionalidadService.traerFuncionalidad().subscribe((data: any) => {
//     console.log(data);
//     this.func = data;
//   },
//   error => {
//     console.log('There was an error while retrieving data !!!', error);
//   });
// }

// /* permiso=false; */

// menusByrol() {
//   /* alert(document.getElementById("rol")['value']); */
//   this.menuService.menusByRol(document.getElementById("rol")['value']).subscribe((data: any) => {
//     console.log(data);

//     this.menus = data;
//    /*  if(data.permiso){
//       this.permiso=true;
//     } */

//   },
//     error => {
//       console.log('There was an error while retrieving data !!!', error);
//     });
// }

// form: FormGroup = new FormGroup({

//   rol: new FormControl('',Validators.required)
// });

// initializeFormGroup(){
//   this.form.setValue({
//     rol: null,
//   });
// }

// toogleInput(idfuncionalidad:number){


//    const index = this.fun.indexOf(idfuncionalidad);
//    if(index === -1){
//      this.fun.push(idfuncionalidad);
//    }else{
//     this.fun.splice(index,1);
//    }
//    return this.fun

// }

// guardarPermiso(){

//     if(this.form.valid){
//       let obj:any = new Object();
//       obj.idRol=this.form.value.rol;
//       obj.funciones=this.fun;
//       console.log(obj);
//       this.permisosService.guardar(obj).subscribe((data: any) => {
//         console.log(data);
//         alert('muy bien');
//         this.form.reset();
//         window.location.reload();
//       },
//       error => {
//         console.log('There was an error while retrieving data !!!', error);
//       });
//     }else{
//       alert('Por favor revise los campos');
//     }


// }
// get rol() { return this.form.get('rol') }

