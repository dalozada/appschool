import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PermisosService {

  url = 'http://localhost:8080/virtualt';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };

  constructor(
    private httpClient: HttpClient

  ) { }


  guardar(data:any): Observable<Object[]> {
        /* this.data=data;
        let obj:any = new Object();
        obj.id = data.id;
        obj.rol = data.rol;
        obj.fun = data.fun; */
           /*  console.log(JSON.stringify(data)+'*****');   */
        return this.httpClient
          .post(this.url + '/asignarfuncionalidades', JSON.stringify(data), this.httpOptions)
          .pipe(map(response => response as Object[]));
      }

}

// const url = 'http://localhost:8080/';
// const httpOptions = {
//   headers: new HttpHeaders({
//     'Content-Type': 'application/json',

//   })
// };

// @Injectable({
//   providedIn: 'root'
// })
// export class PermisosService {

//   constructor(private http: HttpClient) { }

//   public data={'rol':'', 'fun':[], 'id':''};



//   guardar(data:any): Observable<Object[]> {
//     /* this.data=data;
//     let obj:any = new Object();
//     obj.id = data.id;
//     obj.rol = data.rol;
//     obj.fun = data.fun; */
//        /*  console.log(JSON.stringify(data)+'*****');   */
//     return this.http
//       .post(url + 'virtualt/asignarfuncionalidades', JSON.stringify(data), httpOptions)
//       .pipe(map(response => response as Object[]));
//   }
