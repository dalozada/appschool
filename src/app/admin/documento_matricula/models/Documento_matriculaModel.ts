// tslint:disable-next-line: class-name
export interface Documento_matriculaModel {
    id: number;
    tipoDocumento: number;
    contenido: string;
    rutaDoc: string;
  }