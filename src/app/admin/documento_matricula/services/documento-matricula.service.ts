import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Documento_matriculaModel } from '../models/Documento_matriculaModel';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DocumentoMatriculaService {
  documento_matricula: Documento_matriculaModel;
  url = 'http://localhost:8080/colegio';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerDocumento_matricula() {
     return this.httpClient.get<Documento_matriculaModel[]>(this.url + '/documentosmatricula', this.httpOptions)
     .pipe(map(data => data));
  }

  public slider() {
    return this.httpClient.get(this.url + '/slider', this.httpOptions)
    .pipe(map(data => data));
  }

  public documento_matriculaByRolUsuario(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traerdocumento_matriculaaususuario', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public documento_matriculaByRol(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traerdocumento_matricula', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public info(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/permisos', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  crearDocumento_matricula(documento_matricula: Documento_matriculaModel) {

    documento_matricula.tipoDocumento = documento_matricula.tipoDocumento;
    documento_matricula.contenido = documento_matricula.contenido;
    return this.httpClient.post<Documento_matriculaModel>(this.url + '/guardardocumentomatricula', documento_matricula);
  }

  eliminarDocumento_matricula(documento_matriculaId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.post(this.url + '/eliminardocumentomatricula', documento_matriculaId, this.httpOptions);
  }
  actualizarDocumento_matricula(documento_matricula: Documento_matriculaModel) {
    documento_matricula.tipoDocumento = documento_matricula.tipoDocumento;
    documento_matricula.contenido = documento_matricula.contenido;

    return this.httpClient.post(this.url + '/actualizardocumentomatricula', documento_matricula, this.httpOptions);
  }
}
