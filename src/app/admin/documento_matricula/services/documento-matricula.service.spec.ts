import { TestBed } from '@angular/core/testing';

import { DocumentoMatriculaService } from './documento-matricula.service';

describe('DocumentoMatriculaService', () => {
  let service: DocumentoMatriculaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocumentoMatriculaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
