import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Documento_matriculaModel } from './../../models/Documento_matriculaModel';
import { DocumentoMatriculaService } from './../../services/documento-matricula.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-documento-matricula',
  templateUrl: './documento-matricula.component.html',
  styleUrls: ['./documento-matricula.component.css']
})
export class DocumentoMatriculaComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formDocumento_matricula: FormGroup;
  modalRef: BsModalRef;
  documento_matricula: Documento_matriculaModel;
  objDocumento_matricula: Documento_matriculaModel[] = [];
  alerts: any[] = [];
  documento_matriculaIdUpdate: number;
  constructor(
    private documento_matriculaService: DocumentoMatriculaService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.documento_matricula = {
        id: null,
        tipoDocumento: null,
        contenido: '',
        rutaDoc: '',
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.documento_matriculaService.traerDocumento_matricula()
    .subscribe(documento_matricula => {
      this.objDocumento_matricula = documento_matricula;
    },
    error => {
      this.alerts.push({
        type: 'danger',
        msg: 'Error de conexion',
        timeout: 5000,
        msgStr: 'Ups!'
      });
    });
  }
  get nameDocumento_matriculaField() {
    return this.formDocumento_matricula.get('idmatricula');
  }
  get tipoDocumentoDocumento_matricula() {
    return this.formDocumento_matricula.get('tipoDocumento');
  }
  get contenidoDocumento_matricula() {
    return this.formDocumento_matricula.get('contenido');
  }

  openModalDocumento_matricula() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.formDocumento_matricula.reset();
  this.modalRef.hide();

}
onFileChange(files: FileList) {
  console.log(files);
}
  openModalDocumento_matriculaUpdate(documento_matriculaObj: Documento_matriculaModel) {
     this.documento_matricula = {
      id: documento_matriculaObj.id,
      tipoDocumento: documento_matriculaObj.tipoDocumento,
      contenido: documento_matriculaObj.contenido.toUpperCase(),
      rutaDoc: '',
    };
    this.documento_matriculaIdUpdate = documento_matriculaObj.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formDocumento_matricula = this.formBuilder.group({
      id: [null],
      tipoDocumento: ['', [Validators.required]],
      contenido: ['', [Validators.required]],
      rutaDoc: ['', [Validators.required]],
    });

  this.formDocumento_matricula.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarDocumento_matricula() {
    this.formDocumento_matricula.value.contenido = this.formDocumento_matricula.value.contenido.toUpperCase();
    // tslint:disable-next-line: max-line-length
    const pos = this.objDocumento_matricula.indexOf(this.objDocumento_matricula.find( documento => documento.id === this.documento_matriculaIdUpdate));
    if (this.formDocumento_matricula.valid) {
      if (this.documento_matricula.id === null ) {

        this.documento_matriculaService.crearDocumento_matricula(this.formDocumento_matricula.value)
       .subscribe( newDocumento_matricula => {
        this.alerts.push({
          type: 'success',
          msg: 'El regitro fue creado con éxito',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formDocumento_matricula.reset();
        this.objDocumento_matricula.push(newDocumento_matricula);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.documento_matriculaService.actualizarDocumento_matricula(this.documento_matricula)
         .subscribe( documento_matriculaUpdate => {
          this.objDocumento_matricula[ pos ] = <Documento_matriculaModel>documento_matriculaUpdate ;
           this.alerts.push({
            type: 'success',
            msg: 'La actualización se ha realizado con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualización no pudo completarse',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
