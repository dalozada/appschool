import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentoMatriculaComponent } from './documento-matricula.component';

describe('DocumentoMatriculaComponent', () => {
  let component: DocumentoMatriculaComponent;
  let fixture: ComponentFixture<DocumentoMatriculaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentoMatriculaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentoMatriculaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
