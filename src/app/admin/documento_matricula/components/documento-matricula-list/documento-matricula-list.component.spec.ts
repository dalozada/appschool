import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentoMatriculaListComponent } from './documento-matricula-list.component';

describe('DocumentoMatriculaListComponent', () => {
  let component: DocumentoMatriculaListComponent;
  let fixture: ComponentFixture<DocumentoMatriculaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentoMatriculaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentoMatriculaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
