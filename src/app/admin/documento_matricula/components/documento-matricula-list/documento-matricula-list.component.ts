import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { Documento_matriculaModel } from '../../models/Documento_matriculaModel';
import { DocumentoMatriculaService } from './../../services/documento-matricula.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';


@Component({
  selector: 'app-documento-matricula-list',
  templateUrl: './documento-matricula-list.component.html',
  styleUrls: ['./documento-matricula-list.component.css']
})
export class DocumentoMatriculaListComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<Documento_matriculaModel>();
  @Input() documento_matricula: Documento_matriculaModel[] = [];
  documento_matriculaIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private documento_matriculaService: DocumentoMatriculaService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(documento_matriculaId: number) {
    this.dangerModal.show();
    this.documento_matriculaIdDelete = documento_matriculaId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(documento_matricula: Documento_matriculaModel) {
    this.openModalUpdate.emit(documento_matricula);
  }

  onDeleteDocumento_matricula() {

     // tslint:disable-next-line: max-line-length
     const pos = this.documento_matricula.indexOf(this.documento_matricula.find( documento_matricula => documento_matricula.id === this.documento_matriculaIdDelete));
     this.documento_matriculaService.eliminarDocumento_matricula(this.documento_matriculaIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El registro fue eliminado con éxito',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.documento_matricula.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El registro no puede ser eliminado',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
