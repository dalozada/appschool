import { TestBed } from '@angular/core/testing';

import { PeriodoMatriculaService } from './periodo-matricula.service';

describe('PeriodoMatriculaService', () => {
  let service: PeriodoMatriculaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PeriodoMatriculaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
