import { Injectable } from '@angular/core';
import { PeriodoMatriculaModel } from '../models/periodoMatricula.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PeriodoMatriculaService {

 periodoMatricula: PeriodoMatriculaModel;
  url = 'http://localhost:8080';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient

  ) { }

  public traerPeriodoMatricula() {
    // alert('---');
     return this.httpClient.get<PeriodoMatriculaModel[]>(this.url + '/periodos', this.httpOptions)
     .pipe(map(data => data));
  }




  crearPeriodoMatricula(periodoMatricula: PeriodoMatriculaModel ) {

    periodoMatricula.periodo = periodoMatricula.periodo;
    periodoMatricula.descripcion = periodoMatricula.descripcion.toUpperCase();
    periodoMatricula.calendario = periodoMatricula.calendario.toUpperCase();
    periodoMatricula.fechaInicial = periodoMatricula.fechaInicial["formatted"];
    periodoMatricula.fechaFinal = periodoMatricula.fechaFinal["formatted"];
    return this.httpClient.post<PeriodoMatriculaModel>(this.url + '/periodo',periodoMatricula);
  }


  eliminarPeriodoMatricula(periodoMatriculaId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.delete(this.url + '/periodo/'+periodoMatriculaId, this.httpOptions);
  }

  actualizarPeriodoMatricula(periodoMatricula: PeriodoMatriculaModel) {
    let fechaIn:any;
    let fechaFn:any;
    if(typeof periodoMatricula.fechaInicial==='string'){

      fechaIn=periodoMatricula.fechaInicial;
    }else{
       fechaIn=periodoMatricula.fechaInicial["formatted"];
    }
    if(typeof periodoMatricula.fechaFinal==='string'){

       fechaFn=periodoMatricula.fechaFinal;
    }else{
       fechaFn=periodoMatricula.fechaFinal["formatted"];
    }
    console.log('peridodo--',periodoMatricula);
    periodoMatricula.periodo = periodoMatricula.periodo;
    periodoMatricula.descripcion = periodoMatricula.descripcion.toUpperCase();
    periodoMatricula.calendario = periodoMatricula.calendario.toUpperCase();
    periodoMatricula.fechaInicial = fechaIn;
    periodoMatricula.fechaFinal = fechaFn;
    return this.httpClient.put(this.url + '/periodo/'+periodoMatricula.id,periodoMatricula, this.httpOptions);
  }



}

