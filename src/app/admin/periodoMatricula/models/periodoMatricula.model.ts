export interface PeriodoMatriculaModel{

  id:number,
  periodo:string,
  descripcion:string,
  calendario:string,
  fechaInicial:Date,
  fechaFinal:Date

}
