import { Component, OnInit, Output, Input,EventEmitter, ViewChild } from '@angular/core';
import { PeriodoMatriculaModel } from '../../models/periodoMatricula.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { PeriodoMatriculaService } from '../../services/periodo-matricula.service';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-list-periodo-matricula',
  templateUrl: './list-periodo-matricula.component.html',
  styleUrls: ['./list-periodo-matricula.component.css']
})
export class ListPeriodoMatriculaComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<PeriodoMatriculaModel>();
  @Input() periodo: PeriodoMatriculaModel[] = [];
  periodoDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private periodoService: PeriodoMatriculaService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(periodoId: number) {
    this.dangerModal.show();
    this.periodoDelete = periodoId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(periodo: PeriodoMatriculaModel) {
    this.openModalUpdate.emit(periodo);
  }

  onDeletePeriodo() {

     const pos = this.periodo.indexOf(this.periodo.find( tp => tp.id === this.periodoDelete));
     this.periodoService.eliminarPeriodoMatricula(this.periodoDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: ' ¡Proceso exitoso! El registro fue eliminado con éxito.',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.periodo.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: '¡Ups! El registro no puede ser eliminado.',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
