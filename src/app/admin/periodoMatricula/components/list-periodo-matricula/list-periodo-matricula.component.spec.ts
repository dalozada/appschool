import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListPeriodoMatriculaComponent } from './list-periodo-matricula.component';

describe('ListPeriodoMatriculaComponent', () => {
  let component: ListPeriodoMatriculaComponent;
  let fixture: ComponentFixture<ListPeriodoMatriculaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPeriodoMatriculaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListPeriodoMatriculaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
