import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { PeriodoMatriculaModel } from '../../models/periodoMatricula.model';
import { PeriodoMatriculaService } from '../../services/periodo-matricula.service';
import { debounceTime } from 'rxjs/operators';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';
import { IMyDpOptions, IMyDateModel} from 'mydatepicker';


@Component({
  selector: 'app-periodo-matricula',
  templateUrl: './periodo-matricula.component.html',
  styleUrls: ['./periodo-matricula.component.css']
})
export class PeriodoMatriculaComponent implements OnInit {

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    openSelectorOnInputClick:true,
    showInputField:true,
    inline:false,
    editableDateField:false


  }
  public myDatePickerOptionsFin: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    openSelectorOnInputClick:true,
    showInputField:true,
    inline:false,
    editableDateField:false


  }
  public onDateChanged(event: IMyDateModel) {

    let copy: IMyDpOptions = this.getCopyOfEndDateOptions();
    copy.disableUntil = event.date;
    this.myDatePickerOptionsFin = copy;
    // this.myDatePickerOptionsFin.disableUntil=event.date;


}

getCopyOfEndDateOptions(): IMyDpOptions {
  return JSON.parse(JSON.stringify(this.myDatePickerOptionsFin));
}


  @ViewChild('template') private templatePr: TemplateRef<any>;
  formPeriodo: FormGroup;

  modalRef: BsModalRef;
  periodo: PeriodoMatriculaModel;

  objPeriodo: PeriodoMatriculaModel[] = [];

  alerts: any[] = [];
  periodoIdUpdate: number;
  constructor(
    private peridoMatriculaService: PeriodoMatriculaService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.periodo = {
        id: null,
        periodo: '',
        descripcion: '',
        calendario: '',
        fechaInicial:new Date(),
        fechaFinal:new Date(),
      };
      this.buildForm();
    }



  ngOnInit(): void {
    this.peridoMatriculaService.traerPeriodoMatricula()
    .subscribe(periodo => {
      this.objPeriodo = periodo;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexion',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });



  }


  get periodoField() {
    return this.formPeriodo.get('periodo');
  }
  get descripcionField() {
    return this.formPeriodo.get('descripcion');
  }
  get calendarioField() {
    return this.formPeriodo.get('calendario');
  }
  get fechaInicialField() {
    return this.formPeriodo.get('fechaInicial');
  }
  get fechaFinalField() {
    return this.formPeriodo.get('fechaFinal');
  }
  openModalPeriodo() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.formPeriodo.reset();
  this.modalRef.hide();

}
  openModalPeriodoUpdate(periodo: PeriodoMatriculaModel) {
    this.setDate(periodo.fechaInicial);
    this.setDateFin(periodo.fechaFinal);


    this.periodo = {
      id: periodo.id,
      periodo: periodo.periodo,
      descripcion: periodo.descripcion,
      calendario: periodo.calendario,
      fechaInicial: periodo.fechaInicial,
      fechaFinal: periodo.fechaFinal,
    };
    this.periodoIdUpdate = periodo.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formPeriodo = this.formBuilder.group({
      id: [0],
      periodo: ['', [Validators.required]],
      descripcion: ['', [Validators.required,]],
      calendario: ['', [Validators.required,Validators.maxLength(1),]],
      fechaInicial: ['', [Validators.required]],
      fechaFinal: ['', [Validators.required]],
    });
    this.formPeriodo.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }
  setDate(dateS:Date): void {
    console.log(dateS);
    // Set today date using the patchValue function
    let date = new Date(dateS);
    this.formPeriodo.patchValue({fechaInicial: {
      date: {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate()+1}
    }});
}
  setDateFin(dateS:Date): void {
    console.log(dateS);
    // Set today date using the patchValue function
    let date = new Date(dateS);
    this.formPeriodo.patchValue({fechaFinal: {
      date: {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate()+1}
    }});
}



  guardarPeriodo() {

    const pos = this.objPeriodo.indexOf(this.objPeriodo.find( men => men.id === this.periodoIdUpdate));
    if (this.formPeriodo.valid) {
      if (this.periodo.id === null ) {

        this.peridoMatriculaService.crearPeriodoMatricula(this.formPeriodo.value)
       .subscribe( newTpago => {
        this.alerts.push({
          type: 'success',
          msg: '¡Proceso exitoso! El registro fue creado con éxito.',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formPeriodo.reset();
        this.objPeriodo.push(newTpago);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: '¡Ups! El registro no puede ser creado.',
          timeout: 2000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.peridoMatriculaService.actualizarPeriodoMatricula(this.periodo)
         .subscribe( periodoUpdate => {
          this.objPeriodo[ pos ] = <PeriodoMatriculaModel>periodoUpdate ;
           this.alerts.push({
            type: 'success',
            msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: '¡Ups! El registro no puede ser actualizado.',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
