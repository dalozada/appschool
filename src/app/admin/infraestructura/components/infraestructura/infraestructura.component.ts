import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {InfraestructuraModel} from './../../model/infraestructura.model';
import {InfraestructuraService} from './../../services/infraestructura.service';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-infraestructura',
  templateUrl: './infraestructura.component.html',
  styleUrls: ['./infraestructura.component.css']
})
export class InfraestructuraComponent implements OnInit {

  @ViewChild('template') private templatePr: TemplateRef<any>;
  formInfraestructura: FormGroup;
  modalRef: BsModalRef;
  infraestructura: InfraestructuraModel;
  objInfraestructuras: InfraestructuraModel [] = [];
  infraIdUpdate: number;
  alerts: any[] = [];
  constructor(
    private infraestructuraService: InfraestructuraService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.infraestructura = {
        id: null,
        nombreSalon: '',
        capacidadEstudiantes: 0,
        descripcion: '',
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.infraestructuraService.traerInfraestructuras()
    .subscribe(infraestructuras => {
      this.objInfraestructuras = infraestructuras;
    },
    error => {
      this.alerts.push({
        type: 'danger',
        msg: 'Error de conexión',
        timeout: 5000,
        msgStr: 'Ups!'
      });
  });

  }
  get nameSalonField() {
    return this.formInfraestructura.get('nombreSalon');
  }
  get numberStudentField() {
    return this.formInfraestructura.get('capacidadEstudiantes');
  }
  get descripcionField() {
    return this.formInfraestructura.get('descripcion');
  }

  openModalInfraestructura() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
  closeModal() {
    this.formInfraestructura.reset();
    this.modalRef.hide();

  }
  openModalInfraestructuraUpdate(infraestructuraObj: InfraestructuraModel) {
    this.infraestructura = {
      id: infraestructuraObj.id,
      nombreSalon: infraestructuraObj.nombreSalon.toUpperCase(),
      capacidadEstudiantes: infraestructuraObj.capacidadEstudiantes,
      descripcion: infraestructuraObj.descripcion.toUpperCase()
    };
    this.infraIdUpdate = infraestructuraObj.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formInfraestructura = this.formBuilder.group({
      id: [null],
      nombreSalon: ['', [Validators.required]],
      capacidadEstudiantes: [null, [Validators.required]],
      descripcion: ['', [Validators.required]]
    });
    this.formInfraestructura.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarInfraestructura(infraestructura: InfraestructuraModel) {
    this.formInfraestructura.value.nombreSalon = this.formInfraestructura.value.nombreSalon.toUpperCase();
    this.formInfraestructura.value.descripcion = this.formInfraestructura.value.descripcion.toUpperCase();
    const pos = this.objInfraestructuras.indexOf(this.objInfraestructuras.find( men => men.id === this.infraIdUpdate));
    if (this.formInfraestructura.valid) {
      if (this.infraestructura.id === null) {
        this.infraestructuraService.crearInfraestructura(this.formInfraestructura.value)
       .subscribe( newInfraestructura => {
        this.alerts.push({
          type: 'success',
          msg: 'La infraestructura fué creado exitosamente',
          timeout: 5000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formInfraestructura.reset();
        this.objInfraestructuras.push(newInfraestructura);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 5000,
          msgStr: 'Ups!'
        });
      });
      } else {
         this.infraestructuraService.actualizarInfraestructura(this.formInfraestructura.value)
         .subscribe( infraestructuraUpdate => {
          this.alerts.push({
            type: 'success',
            msg: 'La actualización se realizó con éxito',
            timeout: 2000,
            msgStr: 'Proceso Exitoso!'
          });
          this.objInfraestructuras[ pos ] = <InfraestructuraModel>infraestructuraUpdate;
          this.modalRef.hide();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: error,
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
