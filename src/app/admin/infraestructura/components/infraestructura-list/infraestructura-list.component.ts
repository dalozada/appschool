import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { InfraestructuraModel } from './../../model/infraestructura.model';
import { InfraestructuraService } from './../../services/infraestructura.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-infraestructura-list',
  templateUrl: './infraestructura-list.component.html',
  styleUrls: ['./infraestructura-list.component.css']
})
export class InfraestructuraListComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<InfraestructuraModel>();
  @Input() infraestructuras: InfraestructuraModel[] = [];
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  infraIdDelete: number;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private infraestructuraService: InfraestructuraService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(infraId: number) {
    this.dangerModal.show();
    this.infraIdDelete = infraId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(Infraestructura: InfraestructuraModel) {
    this.openModalUpdate.emit(Infraestructura);
  }

  onDeleteInfraestructura() {
    const pos = this.infraestructuras.indexOf(this.infraestructuras.find( men => men.id === this.infraIdDelete));
     this.infraestructuraService.eliminarInfraestructura(this.infraIdDelete)
     .subscribe(rta => {
       this.alerts.push({
        type: 'success',
        msg: 'La Infraestructura fué eliminada exitosamente',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.infraestructuras.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'La infraestructura no puede ser eliminada',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
    }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);

  }

}
