export interface InfraestructuraModel {
  id: number;
  nombreSalon: string;
  capacidadEstudiantes: number;
  descripcion: string;
}
