import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { InfraestructuraModel } from '../model/infraestructura.model';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class InfraestructuraService {
  infraestructura: InfraestructuraModel;
  url = 'http://localhost:8080/colegio';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json;',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerInfraestructuras() {
     return this.httpClient.get<InfraestructuraModel[]>(this.url + '/infraestructura', this.httpOptions)
     .pipe(map(data => data));
  }

  crearInfraestructura(infraestructura: InfraestructuraModel ) {
    return this.httpClient.post<InfraestructuraModel>(this.url + '/guardarinfraestructura', infraestructura);
  }


  eliminarInfraestructura(infraestructuraId: number) {
    // const url = `${this.path}/${todoId}`;
    console.log('esta es el id', infraestructuraId);
    return this.httpClient.post(this.url + '/eliminarinfraestructura', infraestructuraId, this.httpOptions);
  }
  actualizarInfraestructura(infraestructura: InfraestructuraModel) {
    return this.httpClient.post(this.url + '/actualizarinfraestructura', infraestructura, this.httpOptions);
  }
}
