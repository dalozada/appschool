import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { RolModel } from '../../roles/components/models/rol.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RolesService {
  rol: RolModel;
  url = 'http://localhost:8080/virtualt';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerRol() {
     return this.httpClient.get<RolModel[]>(this.url + '/roles', this.httpOptions)
     .pipe(map(data => data));
  }

  public slider() {
    return this.httpClient.get(this.url + '/slider', this.httpOptions)
    .pipe(map(data => data));
  }

  public rolByRolUsuario(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traerrolususuario', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public rolBynombre(nombre:string){
    let name=nombre.toUpperCase();

    return this.httpClient.get(this.url + '/roles/nombre/'+name,this.httpOptions)
    .pipe(map(data => data));

  }

  public rolByEstado(estado:string,tipoRol:string){
    let state=estado.toUpperCase();
    let tipo=tipoRol.toUpperCase();
    return this.httpClient.get(this.url + '/roles/'+state+'/'+tipo,this.httpOptions)
    .pipe(map(data => data));

  }

  public rolByRol(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traerrol', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public info(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/permisos', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  crearRol(rol: RolModel ) {

    rol.nombreRol = rol.nombreRol.toUpperCase();
    return this.httpClient.post<RolModel>(this.url + '/guardarrol', rol);
  }


  eliminarRol(rolId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.post(this.url + '/eliminarrol', rolId, this.httpOptions);
  }
  actualizarRol(rol: RolModel) {
    rol.nombreRol = rol.nombreRol.toUpperCase();
    return this.httpClient.post(this.url + '/actualizarrol', rol, this.httpOptions);
  }
}
