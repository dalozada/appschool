import { EstadoModel } from './estado.models';
import { Tipo_rolModel } from './tipo_rol.model';

export interface RolModel {
  id: number;
  nombreRol: string;
  descripcion: string;
  idestado ?: number;
  idtipoRol ?: number;

  estado ?: EstadoModel;
  tipoRol ?: Tipo_rolModel;

}
