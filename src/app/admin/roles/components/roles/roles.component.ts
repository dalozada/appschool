import { Component, OnInit, TemplateRef, ViewChild, Input } from '@angular/core';
import { RolModel } from './../models/rol.model';
import { RolesService } from './../services/roles.service';
import { AlertComponent } from 'ngx-bootstrap/alert';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';
import { TipoRolService } from './../../../tipo_rol/services/tipo-rol.service';
import { Tipo_rolModel } from './../../../tipo_rol/models/tipo_rol.model';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit {
  @ViewChild('formtemplate') public templateTp: ModalDirective;
  @Input() roles: RolModel[] = [];

  formRol: FormGroup;
  modalRef: BsModalRef;
  rol: RolModel;
  objRol: RolModel[] = [];
  objtipoRol: Tipo_rolModel[] = [];
  alerts: any[] = [];
  rolIdUpdate: number;
  constructor(
    private rolService: RolesService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private tipoRolService: TipoRolService,
    ) {
      this.rol = {
        id: null,
        nombreRol: '',
        descripcion: '',
        idestado: null,
        tipoRol: null,

      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.rolService.traerRol()
    .subscribe(rol => {
      const roles = [];
      rol.forEach(function (value) {if (value.estado.id !== 3) {
      roles.push(value);
      } });
      this.objRol = roles;
      //console.log(this.objRol);
    },
    error => {
      this.alerts.push({
        type: 'danger',
        msg: 'Error de conexion',
        timeout: 5000,
        msgStr: 'Ups!'
    });
  });
  this.traertipoRol();

  }
  traertipoRol() {
    this.tipoRolService.traerTipo_rol()
    .subscribe(tipoRol => {
      this.objtipoRol = tipoRol;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexion',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });
  }
  get nombreRolField() {
    return this.formRol.get('nombreRol');
  }
  get descripcionRol() {
    return this.formRol.get('descripcion');
  }

  get idtipo_rolRol() {
    return this.formRol.get('tipoRol');
  }
  openModalRol() {
    this.modalRef = this.modalService.show(this.templateTp);
  }
closeModal() {
  this.formRol.reset();
  this.templateTp.hide();

}
  openModalRolUpdate(rolObj: RolModel) {
     this.rol = {
      id: rolObj.id,
      nombreRol: rolObj.nombreRol.toUpperCase(),
      descripcion: rolObj.descripcion.toUpperCase(),
      idestado: rolObj.idestado,
      tipoRol: rolObj.tipoRol,
    };
    this.rolIdUpdate = rolObj.id;
    this.templateTp.show();

    // this.modalRef = this.modalService.show(this.templateTp);
  }
  private buildForm() {

    this.formRol = this.formBuilder.group({
      id: [0],
      nombreRol: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      idestado: [''],
      tipoRol: ['', [Validators.required]],
    });

    this.formRol.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarRol(rol: RolModel) {
    console.log(this.roles,'rolesG');
    const tipoRol=this.formRol.value.tipoRol;
    const pos = this.roles.indexOf(this.roles.find( roles => roles.id === this.rolIdUpdate));
    if (this.formRol.valid) {
      if (this.rol.id === null ) {

        // console.log(rol,'---***');
        this.rol.idestado = 1;
        this.rolService.crearRol(rol)
       .subscribe( newRol => {
        //  console.log(newRol,'---');
        this.alerts.push({
          type: 'success',
          msg: 'El registro fue creado con éxito',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.roles.push(
          Object.assign({}, {'estado': {id: 1, estado: 'ACTIVO'}, 'tipoRol':tipoRol}, newRol));
          this.formRol.reset();
        },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
      });

      } else {
        this.rol.idestado = 1;
         this.rolService.actualizarRol(this.rol)
         .subscribe( rolUpdate => {
           console.log(rolUpdate,'***--***');
          this.objRol[ pos ] = <RolModel>rolUpdate ;
           this.alerts.push({
            type: 'success',
            msg: 'La actualizacion se ha realizado con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualizacion no pudo completarse',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
