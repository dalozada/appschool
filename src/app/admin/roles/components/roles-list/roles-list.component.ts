import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { RolModel } from '../../../roles/components/models/rol.model';
import { RolesService } from './../services/roles.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';
import { RolesComponent } from '../roles/roles.component';

@Component({
  selector: 'app-roles-list',
  templateUrl: './roles-list.component.html',
  styleUrls: ['./roles-list.component.css']
})
export class RolesListComponent implements OnInit {
  @ViewChild(RolesComponent) formTp: RolesComponent;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  // @Output() openModalNew = new EventEmitter();
  // @Output() openModalUpdate = new EventEmitter<RolModel>();
    rol: RolModel[] = [];
  rolIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  constructor(
    private rolService: RolesService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {
    this.rolService.traerRol()
    .subscribe(rol => {
      this.rol = rol;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexion',
          timeout: 5000,
          msgStr: 'Ups!'
        });
      });
  }
  openModalDelete(rolId: number) {
    this.dangerModal.show();
    this.rolIdDelete = rolId;
  }
  openModalCrear() {
    this.formTp.templateTp.show();
   }
  openModalActualizar(rol: RolModel) {
    console.log(rol,'***')
    this.formTp.openModalRolUpdate(rol);

    // this.openModalUpdate.emit(rol);
  }

  closeModal() {
    this.formTp.closeModal();
  }

  onDeleteRol() {

     const pos = this.rol.indexOf(this.rol.find( rol => rol.id === this.rolIdDelete));
     this.rolService.eliminarRol(this.rolIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El registro fue eliminado con éxito',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.rol.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El registro no puede ser eliminado',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
