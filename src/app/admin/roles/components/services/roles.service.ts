import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { RolModel } from './../models/rol.model';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})

export class RolesService {
  rol: RolModel;
  url = 'http://localhost:8080/virtualt';
  permisos: number;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };

  constructor(
    private httpClient: HttpClient
  ) { }

  public traerRol() {
    return this.httpClient.get<RolModel[]>(this.url + '/roles', this.httpOptions)
    .pipe(map(data => data));
  }
  public slider() {
    return this.httpClient.get(this.url + '/slider', this.httpOptions)
    .pipe(map(data => data));
  }

  public RolesByRolUsuario(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traerrolsusuario', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public rolesByRol(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traeroles', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public info(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/permisos', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  crearRol(rol: RolModel ) {
    console.log(rol, 'verificacion');
     rol.nombreRol = rol.nombreRol.toUpperCase();
     rol.descripcion = rol.descripcion.toUpperCase();
     rol.idestado = rol.idestado;
     rol.idtipoRol = rol.tipoRol.id;
    return this.httpClient.post<RolModel>(this.url + '/guardarrol', rol);
  }

  eliminarRol(RolId: number) {
    return this.httpClient.post(this.url + '/eliminarrol', RolId, this.httpOptions);
  }
  actualizarRol(rol: RolModel) {
    rol.nombreRol = rol.nombreRol.toUpperCase();
     rol.descripcion = rol.descripcion.toUpperCase();
    //  rol.idestado = rol.idestado;
     rol.idtipoRol = rol.tipoRol.id;
    return this.httpClient.post(this.url + '/actualizarrol', rol, this.httpOptions);
  }
}

