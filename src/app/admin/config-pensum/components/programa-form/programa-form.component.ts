import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ProgramaModel } from '../../../models/programa.model';
import { TipoProgramaModel } from '../../../models/tipo-programa.model';
import { ProgramaService } from '../../../services/programa.service';

import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';
import { TipoProgramaService } from '../../../services/tipo-programa.service';
import { PeriodoModel } from '../../models/periodo.model';
import { PeriodoService } from '../../services/periodo.service';
import { TipoProgramaComponent } from '../../../tipo_programa/components/tipo-programa/tipo-programa.component';

@Component({
  selector: 'app-programa-form',
  templateUrl: './programa-form.component.html',
  styleUrls: ['./programa-form.component.css']
})
export class ProgramaFormComponent implements OnInit {
  @ViewChild('formtemplate') public templateFrm: ModalDirective;
  @ViewChild(TipoProgramaComponent) formTipoPrograma: TipoProgramaComponent;
  @Input() programas: ProgramaModel[] = [];
  @Output() enviarPeriodoInicial = new EventEmitter<PeriodoModel>();
  periodo: PeriodoModel;
  programa: ProgramaModel;
  formPrograma: FormGroup;
  tiposProgramas: TipoProgramaModel[] = [];
  alerts: any[] = [];
  programaIdUpdate: number;
  constructor(
    private programaService: ProgramaService,
    private tipoProgramaService: TipoProgramaService,
    private formBuilder: FormBuilder,
    private periodoService: PeriodoService
    ) {

      this.programa = {
        id: null,
        nombrePrograma: '',
        tipoPrograma: {
                        id: null,
                        nombreTipoPrograma: '',
                        descripcion: ''
                      },
        descripcionPrograma: '',
        codigoPrograma: '',
        estado: {
                  id: null,
                  estado: '',
                  descripcion: ''
                }
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.tipoProgramaService.traerTipoProgramas()
    .subscribe(tiposProgramas => {
      this.tiposProgramas = tiposProgramas;
    });

  }
  get nombreProgramaField() {
    return this.formPrograma.get('nombrePrograma');
  }
  get codigoProgramaField() {
    return this.formPrograma.get('codigoPrograma');
  }
  get tipoProgramaField() {
    return this.formPrograma.get('tipoPrograma');
  }
  get descripcionProgramaField() {
    return this.formPrograma.get('descripcionPrograma');
  }
  cargarObjPrograma(programaObj: ProgramaModel) {
    this.programa = {
      id: programaObj.id,
      nombrePrograma: programaObj.nombrePrograma,
      tipoPrograma: {
                      id: programaObj.tipoPrograma.id,
                      nombreTipoPrograma: programaObj.tipoPrograma.nombreTipoPrograma,
                      descripcion: programaObj.tipoPrograma.descripcion
                    },
      descripcionPrograma: programaObj.descripcionPrograma,
      codigoPrograma: programaObj.codigoPrograma,
      estado: {
                id: programaObj.estado.id,
                estado: programaObj.estado.estado,
                descripcion: programaObj.estado.descripcion
              }
    };
    this.templateFrm.show();
  }
  openModalCrearTipoPrograma() {
    this.formTipoPrograma.templateTp.show();
  }
closeModal() {

  this.templateFrm.hide();

}
  private buildForm() {

    this.formPrograma = this.formBuilder.group({
      id: [0],
      nombrePrograma: ['', [Validators.required]],
      tipoPrograma: [this.programa.tipoPrograma, [Validators.required]],
      descripcionPrograma: ['', [Validators.required]],
      codigoPrograma: ['', [Validators.required]],
    });
    this.formPrograma.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarPrograma() {
    this.programa = {
      id: null,
      nombrePrograma: this.formPrograma.value.nombrePrograma.toUpperCase(),
      descripcionPrograma: this.formPrograma.value.descripcionPrograma.toUpperCase(),
      codigoPrograma: this.formPrograma.value.codigoPrograma.toUpperCase(),
      idestado: 2,
      idtipoPrograma: this.formPrograma.value.tipoPrograma.id,
    };
    const pos = this.programas.indexOf(this.programas.find( prog => prog.id === this.programaIdUpdate));
    if (this.formPrograma.valid) {
      if (this.programa.id === null ) {
        this.programaService.crearPrograma(this.programa)
       .subscribe( newPrograma => {
        this.periodo = {
          fechaFinal: '1600-01-01',
          fechaInicial: '1600-01-01',
          idprograma: newPrograma.id,
          id: null,
          periodo: '000000'
        };
         this.periodoService.crearPeriodo(this.periodo)
         .subscribe(periodoIni => {
           this.enviarPeriodoInicial.emit(this.periodo);
        this.programas.push(
          Object.assign({}, {'tipoPrograma': this.formPrograma.value.tipoPrograma, 'estado': {id: 2, estado: 'INACTIVO'}}, newPrograma));
        this.alerts.push({
          type: 'success',
          msg: 'El registro fué creado exitosamente',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
      });
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });

      } else {
         this.programaService.actualizarPrograma(this.programa)
         .subscribe( programaUpdate => {
          this.programas[ pos ] = <ProgramaModel>programaUpdate ;
           this.alerts.push({
            type: 'success',
            msg: 'La actualización se realizó con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualización no pudo completarce',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
