import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodoProgramaTabComponent } from './periodo-programa-tab.component';

describe('PeriodoProgramaTabComponent', () => {
  let component: PeriodoProgramaTabComponent;
  let fixture: ComponentFixture<PeriodoProgramaTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodoProgramaTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodoProgramaTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
