import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { ProgramaModel } from '../../../models/programa.model';
import { ProgramaService } from '../../../services/programa.service';
import { PeriodoService } from './../../services/periodo.service';
import { PeriodoModel } from './../../models/periodo.model';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective, BsModalRef} from 'ngx-bootstrap/modal';
import { PeriodoProgramaFormComponent } from './../periodo-programa-form/periodo-programa-form.component';

@Component({
  selector: 'app-periodo-programa-tab',
  templateUrl: './periodo-programa-tab.component.html',
  styleUrls: ['./periodo-programa-tab.component.css']
})
export class PeriodoProgramaTabComponent implements OnInit {

  @Input() programa: ProgramaModel;
  @Output() periodoApertura = new EventEmitter<PeriodoModel>();
  periodos: PeriodoModel[] = [];
  modalRef: BsModalRef;
  periodoId: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  @ViewChild('tabModal') public templateTab: ModalDirective;
  @ViewChild(PeriodoProgramaFormComponent) proPerFrm: PeriodoProgramaFormComponent;
  constructor(
    private periodoService: PeriodoService,
    private programaService: ProgramaService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {
    this.traerPeriodosPrograma();
    if (this.programa.estado.estado === 'ACTIVO') {
      this.getPeriodoApertura();
    }
  }
  openModalDelete(periodoId: number) {
    this.dangerModal.show();
    this.periodoId = periodoId;
  }
  openModalActualizar(periodoObj: PeriodoModel) {
    this.proPerFrm.cargarFormObj(periodoObj, this.programa.id);
  this.periodoId = periodoObj.id;
  this.proPerFrm.templateFrm.show();
  }
  traerPeriodosPrograma() {
    this.periodoService.traerPeriodosPrograma(this.programa.id)
    .subscribe(periodos => {
      this.periodos = periodos;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexión',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });
  }
  onDeletePrograma() {

     const pos = this.periodos.indexOf(this.periodos.find( per => per.id === this.periodoId));
     this.periodoService.eliminarPeriodo(this.periodoId)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El registro fue eliminado con éxito',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
      this.programaService.cambiarEstadoPrograma(this.programa.id, 2)
      .subscribe();
       this.programa.estado.id = 2;
       this.programa.estado.estado = 'INACTIVO';
       this.periodos.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El registro no puede ser eliminado',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
closeModal() {
  this.templateTab.hide();
}
openFormularioActivarProgramas() {
    this.proPerFrm.templateFrm.show();
}
getPeriodoAperturaNew(newPeriodo: PeriodoModel) {
  this.periodoApertura.emit(newPeriodo) ;
}
getPeriodoApertura() {
  this.periodoService.traerPeriodoApertura(this.programa.id)
  .subscribe(newPeriodo => {
    newPeriodo.idprograma = this.programa.id;
    this.periodoApertura.emit(newPeriodo) ;

  });
}
}
