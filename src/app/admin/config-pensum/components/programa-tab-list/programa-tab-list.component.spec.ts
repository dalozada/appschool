import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramaTabListComponent } from './programa-tab-list.component';

describe('ProgramaTabListComponent', () => {
  let component: ProgramaTabListComponent;
  let fixture: ComponentFixture<ProgramaTabListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramaTabListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramaTabListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
