import { Component, OnInit, Output, EventEmitter, Input, ViewChild, TemplateRef } from '@angular/core';
import { ProgramaModel } from '../../../models/programa.model';
import { ProgramaService} from '../../../services/programa.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective, BsModalRef} from 'ngx-bootstrap/modal';
import { ProgramaFormComponent } from './../programa-form/programa-form.component';
import { PeriodoModel } from '../../models/periodo.model';

@Component({
  selector: 'app-programa-tab-list',
  templateUrl: './programa-tab-list.component.html',
  styleUrls: ['./programa-tab-list.component.css']
})
export class ProgramaTabListComponent implements OnInit {
  @Input() programas: ProgramaModel[] = [];
  @Output() enviarPeriodoProgramas = new EventEmitter<PeriodoModel>();
  programa: ProgramaModel;
  modalRef: BsModalRef;
  programaIdMod: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  @ViewChild('tabModal') public templateTab: ModalDirective;
  @ViewChild(ProgramaFormComponent) proFrmTab: ProgramaFormComponent;
  constructor(
    private programaService: ProgramaService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {
  }

  openModalDelete(programaId: number) {
    this.dangerModal.show();
    this.programaIdMod = programaId;
  }
  openModalActualizar(programaObj: ProgramaModel) {
    this.proFrmTab.cargarObjPrograma(programaObj);
}
  cargarPeriodoInicial(peridoInicial: PeriodoModel) {
    this.enviarPeriodoProgramas.emit(peridoInicial);
  }
  onDeletePrograma() {

     const pos = this.programas.indexOf(this.programas.find( menu => menu.id === this.programaIdMod));
     this.programaService.eliminarPrograma(this.programaIdMod)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El registro fue eliminado con éxito',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.programas.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El registro no puede ser eliminado',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
closeModal() {
  this.templateTab.hide();
}
openFormularioGestionProgramas() {
  this.proFrmTab.templateFrm.show();
}
cambiarEstado(idPrograma: number , idEstado: number) {
  const pos = this.programas.indexOf(this.programas.find( menu => menu.id === this.programaIdMod));
  this.programaService.cambiarEstadoPrograma(idPrograma, idEstado)
  .subscribe( progUpdate => {
    this.programas[ pos ] = <ProgramaModel>progUpdate ;
     this.alerts.push({
      type: 'success',
      msg: 'La actualización se realizó con éxito',
      timeout: 1000,
      msgStr: 'Proceso Exitoso!'
    });
    this.closeModal();
  },
  error => {
    this.alerts.push({
      type: 'danger',
      msg: 'La actualización no pudo completarce',
      timeout: 2000,
      msgStr: 'Ups!'
    });
  });
}
}
