import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodoProgramaFormComponent } from './periodo-programa-form.component';

describe('PeriodoProgramaFormComponent', () => {
  let component: PeriodoProgramaFormComponent;
  let fixture: ComponentFixture<PeriodoProgramaFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodoProgramaFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodoProgramaFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
