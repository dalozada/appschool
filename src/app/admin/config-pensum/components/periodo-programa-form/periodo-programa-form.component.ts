import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { PeriodoModel } from './../../models/periodo.model';
import { PeriodoService } from './../../services/periodo.service';
import { ProgramaService } from '../../../services/programa.service';
import { ProgramaModel } from '../../../models/programa.model';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { debounceTime, filter, subscribeOn } from 'rxjs/operators';
import { DatePipe } from '@angular/common';

import * as moment from 'moment';
import { from } from 'rxjs';
import { MateriaService } from '../../services/materia.service';
import { AsignarMateriaProgramaModel } from '../../models/asignar-materia-programa.model';

@Component({
  selector: 'app-periodo-programa-form',
  templateUrl: './periodo-programa-form.component.html',
  styleUrls: ['./periodo-programa-form.component.css']
})
export class PeriodoProgramaFormComponent implements OnInit {
  pipe = new DatePipe('en-US');
  now = Date.now();
  @ViewChild('formPeriodoTemplate') public templateFrm: ModalDirective;
  @Input() programa: ProgramaModel;
  @Input() objPeriodos: PeriodoModel[] = [];
  @Output() periodoAperturaFrm = new EventEmitter<PeriodoModel>();
  // @Input() Periodo: PeriodoModel;
  formProgPeriodo: FormGroup;
  materiaPrograma: AsignarMateriaProgramaModel;
  periodo: PeriodoModel;
  alerts: any[] = [];
  periodoIdUpdate: number;
  constructor(
    private periodoService: PeriodoService,
    private programaService: ProgramaService,
    private formBuilder: FormBuilder,
    private materiaService: MateriaService
    ) {
      this.periodo = {
        id: null,
        periodo: '',
        fechaFinal: null,
        fechaInicial: null,
        idprograma: null
      };
      this.buildForm();
    }

  ngOnInit(): void {
  }
  get periodoField() {
    return this.formProgPeriodo.get('periodo');
  }
  get fechaInicialField() {
    return this.formProgPeriodo.get('fechaInicial');
  }
  get fechaFinalField() {
    return this.formProgPeriodo.get('fechaFinal');
  }
closeModal() {
  this.formProgPeriodo.reset();
  this.templateFrm.hide();

}
  private buildForm() {

    this.formProgPeriodo = this.formBuilder.group({
      id: [0],
      periodo: ['', [Validators.required]],
      fechaInicial: ['', [Validators.required]],
      fechaFinal: ['', [Validators.required]],
      idprograma: [0],
    });
    this.formProgPeriodo.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }
  cargarFormObj(periodoObj: PeriodoModel, idprog: number) {
    periodoObj.fechaFinal = moment(periodoObj.fechaFinal).format('YYYY-MM-DD');
    periodoObj.fechaInicial = moment(periodoObj.fechaInicial).format('YYYY-MM-DD');
    this.periodo = {
      id: periodoObj.id,
      periodo: periodoObj.periodo,
      fechaFinal: periodoObj.fechaFinal,
      fechaInicial: periodoObj.fechaInicial,
      idprograma: this.programa.id
  };
  }
  guardarPrograma() {
    this.formProgPeriodo.value.idprograma = this.programa.id;
    const pos = this.objPeriodos.indexOf(this.objPeriodos.find( prog => prog.id === this.periodoIdUpdate));
    if (this.formProgPeriodo.valid) {
      if (this.periodo.id === null ) {
        this.periodoService.crearPeriodo(this.formProgPeriodo.value)
       .subscribe( newPeriodo => {
        this.alerts.push({
          type: 'success',
          msg: 'El registro fué creado y el programa se activo con éxito',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.periodoAperturaFrm.emit(newPeriodo);
        this.programaService.cambiarEstadoPrograma(this.programa.id, 1)
        .subscribe(rta => {});
        this.programa.estado.id = 1;
        this.programa.estado.estado = 'ACTIVO';
        this.formProgPeriodo.reset();
        this.objPeriodos.push(newPeriodo);
        this.materiaService.traerAsignacionMateriasPensum(this.programa.id, newPeriodo.id)
        .subscribe(asignacionMates => {
          for ( const materiaR of asignacionMates) {
            let idcontrato: number = null;
            if (materiaR.contrato !== null) {
              idcontrato = materiaR.contrato.id;
            }
            this.materiaPrograma = {
              id: null,
              idcontrato: idcontrato,
              idclasificacion: materiaR.clasificacion.id,
              idmateria: materiaR.materia.id,
              idperiodo: newPeriodo.id,
              idprograma: this.programa.id,
              idestado: 1
            };

            this.materiaService.CrearMateriaPrograma(this.materiaPrograma)
            .subscribe();
          }
        });
        this.closeModal();
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });

      } else {
         this.periodoService.actualizarPeriodo(this.periodo)
         .subscribe( programaUpdate => {
          this.objPeriodos[ pos ] = <PeriodoModel>programaUpdate ;
           this.alerts.push({
            type: 'success',
            msg: 'La actualización se realizó con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualización no pudo completarce',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
