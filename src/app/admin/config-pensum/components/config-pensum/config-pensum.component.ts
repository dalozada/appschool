import { Component, OnInit, Output, EventEmitter, ViewChild, AfterViewInit } from '@angular/core';
import { ProgramaTabListComponent } from './../programa-tab-list/programa-tab-list.component';
import { AsignarMateriaProgramaModel } from '../../models/asignar-materia-programa.model';
import { MateriaFormComponent } from './../materia-form/materia-form.component';
import { ProgramaModel } from '../../../models/programa.model';
import { ProgramaService } from '../../../services/programa.service';
import { MateriaService } from './../../services/materia.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { EmpresaService } from './../../../services/empresa.service';
import { EmpresaModel } from './../../../models/empresa.model';
import { PeriodoService } from '../../services/periodo.service';


@Component({
  selector: 'app-config-pensum',
  templateUrl: './config-pensum.component.html',
  styleUrls: ['./config-pensum.component.css']
})
export class ConfigPensumComponent implements OnInit {
  @ViewChild(ProgramaTabListComponent) proTab: ProgramaTabListComponent;
  @ViewChild(MateriaFormComponent) formMat: MateriaFormComponent;
  modalRef: BsModalRef;
  alerts: any[] = [];
  objsProgramas: ProgramaModel[] = [];
  objsAsignacionMaterias: AsignarMateriaProgramaModel[] = [];
  materia: AsignarMateriaProgramaModel;
  programa: ProgramaModel;
  empresaPr: EmpresaModel;
  verificacion: number = 0;
  rutaLogo: string;
  constructor(
    private programaService: ProgramaService,
    private materiaService: MateriaService,
    private empresaService: EmpresaService,
    private periodoService: PeriodoService
    ) {
      this.empresaPr = {
        id: null,
        digitoVerificacion: null,
        nit: null,
        razonSocial: '',
        representanteLegal: '',
        rutaLogo: ''
      };
      this.programa = {
        id: null,
        nombrePrograma: '',
        codigoPrograma: '',
        tipoPrograma: {
          id: null,
          nombreTipoPrograma: '',
          descripcion: '',
        },
        estado: {
          id: null,
          estado: '',
          descripcion: ''
        },
        descripcionPrograma: ''
      };
  }

  ngOnInit(): void {
    this.empresaService.traerEmpresaPrincipal()
    .subscribe(empr => {
      if (empr !== null) {
        this.verificacion = 1;
        this.empresaPr = empr;
        this.rutaLogo = `http://localhost:8080/${empr.rutaLogo}`;
      }
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexión',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });
    this.programaService.traerProgramas()
    .subscribe(progs => {
      this.objsProgramas = progs;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexión',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });
  }
  getPensumPrograma(prog: ProgramaModel) {
    this.programa = {
      id: prog.id,
      nombrePrograma: prog.nombrePrograma,
      codigoPrograma: prog.codigoPrograma,
      tipoPrograma: prog.tipoPrograma,
      estado: {
        id: prog.estado.id,
        estado: prog.estado.estado,
        descripcion: prog.estado.descripcion
      },
      descripcionPrograma: prog.descripcionPrograma
    };
    this.periodoService.traerPeriodoApertura(this.programa.id)
    .subscribe(periodoUlt => {
      this.materiaService.traerAsignacionMateriasPensum(this.programa.id, periodoUlt.id)
    .subscribe(asignacionMates => {
      for ( const materiaR of asignacionMates) {
        if (materiaR.contrato === null) {
          materiaR.contrato = {
            id: 0,
            persona: {
              apellido1: '',
              apellido2: '',
              nombre2: '',
              nombre1: '',
              id: null,
              identificacion: '',
              rutaFoto: ''
            }
           };
       }
      }
      console.log('materias asignadas', asignacionMates);
      this.objsAsignacionMaterias = asignacionMates;
    });

    });
  }
  openTablaGestionProgramas() {
    this.proTab.templateTab.show();
  }
  openActivarPeriodoPrograma() {
    // this.activarPeriodo.templatePeriodo.show();
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
  openFrmMateria() {
    this.formMat.templateFrm.show();
  }
  cargarObjetoUpdate(asigMateria: AsignarMateriaProgramaModel) {
    this.formMat.cargarObjMaterias(asigMateria);
    this.formMat.templateFrm.show();
  }
}
