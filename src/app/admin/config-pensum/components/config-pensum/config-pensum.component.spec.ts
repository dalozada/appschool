import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigPensumComponent } from './config-pensum.component';

describe('ConfigPensumComponent', () => {
  let component: ConfigPensumComponent;
  let fixture: ComponentFixture<ConfigPensumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigPensumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigPensumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
