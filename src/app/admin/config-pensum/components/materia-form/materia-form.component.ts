import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MateriaModel } from './../../models/materia.model';
import { ProgramaModel } from '../../../models/programa.model';
import { MateriaService } from './../../services/materia.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';
import { ClasificacionModel } from './../../../clasificacion/models/clasificacion.model';
import { ClasificacionService } from './../../../clasificacion/sevices/clasificacion.service';
import { RolPersonasService } from './../../../services/rol-personas.service';
import { RolPersonaModel } from '../../models/rol-persona.model';
import { PeriodoService } from './../../services/periodo.service';
import { AsignarMateriaProgramaModel } from './../../models/asignar-materia-programa.model';
import { ContratoModel } from '../../models/contrato.model';
import { ClasificacionComponent } from '../../../clasificacion/components/clasificacion/clasificacion.component';
import { PeriodoModel } from '../../models/periodo.model';

@Component({
  selector: 'app-materia-form',
  templateUrl: './materia-form.component.html',
  styleUrls: ['./materia-form.component.css']
})
export class MateriaFormComponent implements OnInit {
  @Input() programa: ProgramaModel;
  @Input() materias: AsignarMateriaProgramaModel[] = [];
  @Input() materia: AsignarMateriaProgramaModel;
  @ViewChild('materiaForm') public templateFrm: ModalDirective;
  @ViewChild(ClasificacionComponent) formClas: ClasificacionComponent;
  ObjsClasificaciones: ClasificacionModel[] = [];
  objsContratos: RolPersonaModel[] = [];
  materiaUp: MateriaModel;
  formMateria: FormGroup;
  contrato: ContratoModel;
  periodo: PeriodoModel;
  frm: ClasificacionComponent;
  val: number = 0;
  idPeriodo: number = null;
  idContrato: number = null;
  materiaPrograma: AsignarMateriaProgramaModel;
  materiaProgramaUpdt: AsignarMateriaProgramaModel;
  alerts: any[] = [];
  materiaInput: MateriaModel[] = [];
  materiaIdUpdate: number;
  constructor(
    private materiaService: MateriaService,
    private formBuilder: FormBuilder,
    private clasificacionService: ClasificacionService,
    private rolPersonasService: RolPersonasService,
    private periodoService: PeriodoService,
    ) {
      this.contrato = {
        id: 0,
        idpersona: null,
        persona: {
          apellido1: '',
          apellido2: '',
          nombre2: '',
          nombre1: '',
          id: null,
          identificacion: '',
          rutaFoto: ''
        }
      };
      this.materia = {
        id: null,
        idcontrato: null,
        idclasificacion: null,
        idmateria: null,
        idperiodo: null,
        idprograma: null,
        clasificacion: {
          id: null,
          detalleClasificacion: ''
        },
        contrato: {
          id: null,
          idpersona: null,
          persona: {
            apellido1: '',
            apellido2: '',
            nombre2: '',
            nombre1: '',
            id: null,
            identificacion: '',
            rutaFoto: ''
          }
        },
        materia: {
          id: null,
          contenido: '',
          descripcion: '',
          rutaDoc: '',
          tituloMateria: ''
        },
        periodo: {
          fechaFinal: '',
          fechaInicial: '',
          id: null,
          idprograma: null,
          periodo: '',
        }
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.clasificacionService.traerClasificacion()
     .subscribe(clasifs => {
       this.ObjsClasificaciones = clasifs;
     },
     error => {
       this.alerts.push({
         type: 'danger',
         msg: 'Error de conexion',
         timeout: 5000,
         msgStr: 'Ups!'
     });
   });
   this.rolPersonasService.traerDocentes()
     .subscribe(docentes => {
       if (docentes !== null) {
        this.val = 1;
        this.objsContratos = docentes;
       }
     },
     error => {
       this.alerts.push({
         type: 'danger',
         msg: 'Error de conexion',
         timeout: 5000,
         msgStr: 'Ups!'
     });
   });
  }
  get tituloMateriaField() {
    return this.formMateria.get('tituloMateria');
  }
  get descripcionField() {
    return this.formMateria.get('descripcion');
  }
  get contenidoField() {
    return this.formMateria.get('contenido');
  }
  get rutaDocField() {
    return this.formMateria.get('rutaDoc');
  }
  onFileChange(files: FileList) {
    console.log(files);
    // this.labelImport.nativeElement.innerText = Array.from(files)
    //   .map(f => f.name)
    //   .join(', ');
    // this.fileToUpload = files.item(0);
  }
  onChange(materiaAntes: MateriaModel, isChecked: boolean, pos: number) {
    if (isChecked) {
      this.materiaInput.push(materiaAntes);
    } else {
      this.materiaInput.splice(pos, 1);
    }
  }
  openModalCrearClasificacion() {
    this.formClas.templatePr.show();
  }
closeModal() {
  // this.formMateria.reset();
  this.materia = {
    id: null,
    idcontrato: null,
    idclasificacion: null,
    idmateria: null,
    idperiodo: null,
    idprograma: null,
    clasificacion: {
      id: null,
      detalleClasificacion: ''
    },
    contrato: {
      id: null,
      idpersona: null,
      persona: {
        apellido1: '',
        apellido2: '',
        nombre2: '',
        nombre1: '',
        id: null,
        identificacion: '',
        rutaFoto: ''
      }
    },
    materia: {
      id: null,
      contenido: '',
      descripcion: '',
      rutaDoc: '',
      tituloMateria: ''
    },
    periodo: {
      fechaFinal: '',
      fechaInicial: '',
      id: null,
      idprograma: null,
      periodo: '',
    }
  };
  this.templateFrm.hide();

}
  private buildForm() {

    this.formMateria = this.formBuilder.group({
      id: [0],
      tituloMateria: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      contenido: ['', [Validators.required]],
      rutaDoc: ['', [Validators.required]],
      programa: [null],
      clasificacion: [null, [Validators.required]],
      contrato : [null]
    });
    this.formMateria.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }
  cargarObjMaterias(materiaObj: AsignarMateriaProgramaModel) {
    this.materia = materiaObj;
    this.materiaIdUpdate = this.materia.id;
  }
  openFormularioNiveles() {
   this.frm.openModalClasificacion();
  }
  guardarMateria() {
    this.formMateria.value.tituloMateria = this.formMateria.value.tituloMateria.toUpperCase();
    if (this.objsContratos.length > 0) {
      this.val = 1;
      this.idContrato = this.formMateria.value.contrato.id;
      this.contrato = this.formMateria.value.contrato;
    }

    this.formMateria.value.idprograma = this.programa.id;
    const pos = this.materias.indexOf(this.materias.find( mat => mat.id === this.materiaIdUpdate));
      if (this.formMateria.value.id === null ) {
        this.materiaUp = {
          id: null,
          tituloMateria: this.formMateria.value.tituloMateria,
          contenido: this.formMateria.value.contenido,
          descripcion: this.formMateria.value.descripcion,
          rutaDoc: this.formMateria.value.rutaDoc
        };
        this.materiaService.crearMateria(this.materiaUp)
       .subscribe( newMateria => {
        this.alerts.push({
          type: 'success',
          msg: 'El registro fué creado con éxito',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.periodoService.traerPeriodoApertura(this.programa.id)
        .subscribe(ultperiodo => {
          if (ultperiodo !== null) {
            this.idPeriodo = ultperiodo.id;
            this.periodo = ultperiodo;
          }
          this.materia = {
            id: null,
            idcontrato: this.idContrato,
            idclasificacion: this.formMateria.value.clasificacion.id,
            idmateria: newMateria.id,
            idperiodo: this.idPeriodo,
            idprograma: this.programa.id,
            idestado: this.programa.estado.id
          };
          this.materiaService.CrearMateriaPrograma(this.materia)
          .subscribe( newAsignacionMateria => {
            this.materias.push(
              Object.assign({}, {'materia': newMateria, 'programa': this.programa, 'clasificacion': this.formMateria.value.clasificacion, 'periodo': ultperiodo, 'contrato': this.contrato, 'estado': this.programa.estado.id}, newAsignacionMateria));
          this.formMateria.reset();
          this.closeModal();
          });
        });
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });

      } else {

        if (this.materia.periodo !== null) {
          this.idPeriodo = this.materia.periodo.id;
          this.periodo = this.materia.periodo;
        }
        this.materiaUp = {
          id: this.materia.materia.id,
          tituloMateria: this.formMateria.value.tituloMateria,
          contenido: this.formMateria.value.contenido,
          descripcion: this.formMateria.value.descripcion,
          rutaDoc: this.formMateria.value.rutaDoc
        };
        if (this.formMateria.value.contrato !== null) {
            this.contrato = this.formMateria.value.contrato;
        }
         this.materiaService.actualizarMateria(this.materiaUp)
         .subscribe( materiaUpdate => {
          this.materiaProgramaUpdt = {
            id: this.materia.id,
            idcontrato: this.idContrato,
            idclasificacion: this.formMateria.value.clasificacion.id,
            idmateria: materiaUpdate.id,
            idperiodo: this.idPeriodo,
            idprograma: this.programa.id,
            idestado: this.materia.estado.id
          };
          this.materiaService.actualizarAsignacionMateriasPensum(this.materiaProgramaUpdt)
          .subscribe( asigMateriaUpdate => {
            this.materia = {
              id: asigMateriaUpdate.id,
              clasificacion: this.formMateria.value.clasificacion,
              contrato: this.contrato,
              materia: materiaUpdate,
              periodo: this.periodo,
              programa: this.programa
            };
              this.materias[ pos ] = this.materia;
                this.alerts.push({
                  type: 'success',
                  msg: 'La actualización se realizó con éxito',
                  timeout: 1000,
                  msgStr: 'Proceso Exitoso!'
                });

                this.closeModal();

          });
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualización no pudo completarce',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }


  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
