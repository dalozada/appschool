import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { ProgramaModel } from '../../../models/programa.model';
import { AsignarMateriaProgramaModel } from '../../models/asignar-materia-programa.model';
import { MateriaFormComponent } from '../materia-form/materia-form.component';
@Component({
  selector: 'app-materia',
  templateUrl: './materia.component.html',
  styleUrls: ['./materia.component.css']
})
export class MateriaComponent implements OnInit {
  @Input() programa: ProgramaModel;
  @Input() materia: AsignarMateriaProgramaModel;
  @Input() frm: MateriaFormComponent;
  @Output() updateAsignacionMateria = new EventEmitter<AsignarMateriaProgramaModel>();
  // @ViewChild(MateriaFormComponent) formMat2: MateriaFormComponent;
  constructor(
    ) {
      this.materia = {
        id: null,
        idcontrato: null,
        idclasificacion: null,
        idmateria: null,
        idperiodo: null,
        idprograma: null,
        clasificacion: {
          id: null,
          detalleClasificacion: ''
        },
        contrato: {
          id: null,
          idpersona: null,
          persona: {
            apellido1: '',
            apellido2: '',
            nombre2: '',
            nombre1: '',
            id: null,
            identificacion: '',
            rutaFoto: ''
          }
        },
        materia: {
          id: null,
          contenido: '',
          descripcion: '',
          rutaDoc: '',
          tituloMateria: ''
        },
        periodo: {
          fechaFinal: '',
          fechaInicial: '',
          id: null,
          idprograma: null,
          periodo: '',
        }
      };
    }

  ngOnInit(): void {
  }
  cargarMateria(materiaObj: AsignarMateriaProgramaModel) {
    this.materia = materiaObj;
    console.log(this.materia);
  }
  openUpdateAsignacion(materiaObj: AsignarMateriaProgramaModel) {
    this.updateAsignacionMateria.emit(materiaObj);
    }
}

