import { Component, OnInit, Input } from '@angular/core';
import { DiaModel } from './../../../models/dia.model';
import { JornadaModel } from './../../../models/jornada.model';

@Component({
  selector: 'app-dia-list',
  templateUrl: './dia-list.component.html',
  styleUrls: ['./dia-list.component.css']
})
export class DiaListComponent implements OnInit {
@Input() jornada: JornadaModel;
@Input() dia: DiaModel;
  constructor() { }

  ngOnInit(): void {
  }

}
