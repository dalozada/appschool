import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { ProgramaModel } from '../../../models/programa.model';
import { PeriodoProgramaTabComponent} from './../periodo-programa-tab/periodo-programa-tab.component';
import { JornadaTabComponent } from './../jornada-tab/jornada-tab.component';
import { PeriodoModel } from '../../models/periodo.model';
import * as moment from 'moment';


@Component({
  selector: 'app-programa',
  templateUrl: './programa.component.html',
  styleUrls: ['./programa.component.css']
})
export class ProgramaComponent implements OnInit {
  @Input() programa: ProgramaModel;
  @Output() outputPrograma = new EventEmitter<ProgramaModel>();
  @ViewChild(PeriodoProgramaTabComponent) perTab: PeriodoProgramaTabComponent;
  @ViewChild(JornadaTabComponent) jorTab: JornadaTabComponent;
  periodo: PeriodoModel;
  fechaActual = new Date();
  today: string;
  constructor(
    ) {
      this.periodo = {
        id: null,
        periodo: null,
        fechaInicial: '',
        fechaFinal: '',
        idprograma: null
      };
    }

  ngOnInit(): void {
    this.today = moment(this.fechaActual.getDate()).format('YYYY-MM-DD');
  }
  pensumPrograma(prog: ProgramaModel) {
    this.outputPrograma.emit(prog);
  }
  openTablaPeriodosPrograma() {
    this.perTab.templateTab.show();
  }
  openTablaJornadasPrograma() {
    this.jorTab.templateTab.show();
  }

  cargarPeriodoIncial(periodoInicial: PeriodoModel) {
    this.periodo = {
      id: periodoInicial.id,
      fechaFinal: periodoInicial.fechaFinal,
      fechaInicial: periodoInicial.fechaInicial,
      idprograma: periodoInicial.idprograma,
      periodo: periodoInicial.periodo
    };
  }
  getPeriodoApertura(per: PeriodoModel) {
    this.periodo = {
      id: per.id,
      periodo: per.periodo,
      fechaInicial: per.fechaInicial,
      fechaFinal: per.fechaFinal,
      idprograma: per.idprograma,
    };
  }
}
