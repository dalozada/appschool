import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { JornadaModel } from './../../../models/jornada.model';
import { JornadaService } from './../../../services/jornada.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {FormGroup, FormBuilder, Validators, FormArray, FormControl} from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { debounceTime, filter, subscribeOn } from 'rxjs/operators';
import { ProgramaModel } from './../../../models/programa.model';
import { DiaModel } from './../../../models/dia.model';
import { DiaService } from './../../../services/dia.service';


@Component({
  selector: 'app-jornada-form',
  templateUrl: './jornada-form.component.html',
  styleUrls: ['./jornada-form.component.css']
})
export class JornadaFormComponent implements OnInit {

  @ViewChild('formJornadaTemplate') public templateFrm: ModalDirective;
  @Input() jorandas: JornadaModel[] = [];
  @Input() programa: ProgramaModel;
  @Input() objsDias: DiaModel[] = [];
  @Output() enviarDiasJornada = new EventEmitter<DiaModel[]>();
  diasString: string = '';
  diasInput: DiaModel[] = [];
  formJornada: FormGroup;
  jornada: JornadaModel;
  alerts: any[] = [];
  jornadaIdUpdate: number;
  constructor(
    private jornadaService: JornadaService,
    private diaService: DiaService,
    private formBuilder: FormBuilder,
    ) {
      this.jornada = {
        id: null,
        horaFinal: '',
        horaInicial: null,
        descripcion: null,
        nombreJornada: '',
        numeroHoras: null
      };

      this.buildForm();
    }

  ngOnInit(): void {

  }
  get nombreJornadaField() {
    return this.formJornada.get('nombreJornada');
  }
  get horaInicialField() {
    return this.formJornada.get('horaInicial');
  }
  get horaFinalField() {
    return this.formJornada.get('horaFinal');
  }
  get numHorasField() {
    return this.formJornada.get('numeroHoras');
  }
  get descripcionField() {
    return this.formJornada.get('descripcion');
  }
closeModal() {
  this.formJornada.reset();
  this.templateFrm.hide();

}
  private buildForm() {

    this.formJornada = this.formBuilder.group({
      id: [0],
      nombreJornada: ['', [Validators.required]],
      horaInicial: ['', [Validators.required]],
      horaFinal: ['', [Validators.required]],
      numeroHoras: ['', [Validators.required]],
      dataDia: this.formBuilder.array([])
    });
    this.formJornada.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }
  onChange(dia: DiaModel, isChecked: boolean, pos: number) {
    if (isChecked) {
      this.diasInput.push(dia);
    } else {
      this.diasInput.splice(pos, 1);
    }
  }
  cargarObjJornada(jornadaObj: JornadaModel) {
    this.jornada = {
      id: jornadaObj.id,
      descripcion: jornadaObj.descripcion,
      horaFinal: jornadaObj.horaFinal,
      horaInicial: jornadaObj.horaInicial,
      nombreJornada: jornadaObj.nombreJornada,
      numeroHoras: jornadaObj.numeroHoras
    };
    this.templateFrm.show();
    this.jornadaIdUpdate = jornadaObj.id;
  }
  guardarJornada() {
    for (const dia of this.diasInput) {
      this.diasString = this.diasString + ' ' + dia.dia;
      }
    this.formJornada.value.idprograma = this.jornada.id;
    const pos = this.jorandas.indexOf(this.jorandas.find( prog => prog.id === this.jornadaIdUpdate));
    if (this.formJornada.valid) {
      if (this.jornada.id === null ) {
        this.jornada = {
          id: null,
          descripcion: this.diasString,
          horaFinal: this.formJornada.value.horaFinal + ':00',
          horaInicial: this.formJornada.value.horaInicial + ':00',
          nombreJornada: this.formJornada.value.nombreJornada,
          numeroHoras: this.formJornada.value.numeroHoras
        };
        this.jornadaService.crearJornada(this.jornada)
       .subscribe( newJornada => {
         this.jorandas.push(newJornada);
         for (const dia of this.diasInput) {
           const objasigMateriaJornada: object = {
             id: null,
             idjornada: newJornada.id,
             iddia: dia.id
           };
          this.diaService.agregarDiaJornada(objasigMateriaJornada)
          .subscribe(objAsig => {
          });
         }
        this.alerts.push({
          type: 'success',
          msg: 'El registro fué creado y el programa se activo con éxito',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.diasString = '';
        this.closeModal();
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });

      } else {
         this.jornadaService.actualizarJornada(this.jornada)
         .subscribe( jornadaUpdate => {
          this.jorandas[ pos ] = <JornadaModel>jornadaUpdate ;
           this.alerts.push({
            type: 'success',
            msg: 'La actualización se realizó con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualización no pudo completarce',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }
    this.enviarDiasJornada.emit(this.diasInput);
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
