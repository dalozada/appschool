import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JornadaTabComponent } from './jornada-tab.component';

describe('JornadaTabComponent', () => {
  let component: JornadaTabComponent;
  let fixture: ComponentFixture<JornadaTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JornadaTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JornadaTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
