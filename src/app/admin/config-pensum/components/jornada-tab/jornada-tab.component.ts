import { Component, OnInit, Output, EventEmitter, Input, ViewChild, TemplateRef } from '@angular/core';
import { ProgramaModel } from '../../../models/programa.model';
import { JornadaService} from '../../../services/jornada.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective, BsModalRef} from 'ngx-bootstrap/modal';
import { JornadaFormComponent } from './../jornada-form/jornada-form.component';
import { JornadaModel } from '../../../models/jornada.model';
import { DiaService } from './../../../services/dia.service';
import { DiaModel } from './../../../models/dia.model';
import { AsignarJornadaProgramaModel } from './../../../models/asignar-jornada-programa';

@Component({
  selector: 'app-jornada-tab',
  templateUrl: './jornada-tab.component.html',
  styleUrls: ['./jornada-tab.component.css']
})
export class JornadaTabComponent implements OnInit {
  @Input() programa: ProgramaModel;
  jornadas: JornadaModel[] = [];
  jornadasProg: AsignarJornadaProgramaModel[] = [];
  objsDias: DiaModel[] = [];
  objDiasAsignados: DiaModel[] = [];
  jornadasInput: JornadaModel[] = [];
  modalRef: BsModalRef;
  jornadaIdMod: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  @ViewChild('tabModal') public templateTab: ModalDirective;
  @ViewChild(JornadaFormComponent) jorFrmTab: JornadaFormComponent;
  constructor(
    private jornadaService: JornadaService,
    private diaService: DiaService
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {
    this.jornadaService.traerJornadas()
    .subscribe(jorns => {
      // if (jorns !== null) {
        this.jornadas = jorns;
      // }
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexión',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });
    this.jornadaService.traerAsignacionJornadasPrograma(this.programa.id)
    .subscribe(jorns => {
      // if (jorns !== null) {
        this.jornadasProg = jorns;
      // }
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexión',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });
  }

  openModalDelete(jornadaId: number) {
    this.dangerModal.show();
    this.jornadaIdMod = jornadaId;
  }
  onChange(jornada: JornadaModel, isChecked: boolean, pos: number) {
    if (isChecked) {
      this.jornadasInput.push(jornada);
    } else {
      this.jornadasInput.splice(pos, 1);
    }
  }
  cargarObjDiasJornada(diasObj: DiaModel[]) {
    this.objDiasAsignados = diasObj;
  }
  openModalActualizar(jornadaObj: JornadaModel) {
    this.cargarDias();
     this.jorFrmTab.cargarObjJornada(jornadaObj);
}
  onDeletePrograma() {

     const pos = this.jornadas.indexOf(this.jornadas.find( jorn => jorn.id === this.jornadaIdMod));
     this.jornadaService.eliminarJornada(this.jornadaIdMod)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El registro fue eliminado con éxito',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.jornadas.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El registro no puede ser eliminado',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
closeModal() {
  this.templateTab.hide();
}
agregarJornada () {
  for (const jornada of this.jornadasInput) {
    const objAsgJornadas: object = {
      id: null,
      idprograma: this.programa.id,
      idjornada: jornada.id
    };
    this.jornadaService.agregarJornadaPrograma(objAsgJornadas)
    .subscribe();
  }
}
openFormularioGestionJornada() {
  this.cargarDias();
  this.jorFrmTab.templateFrm.show();
}
cargarDias () {
  this.diaService.traerdias()
  .subscribe(dias => {
       this.objsDias = dias;
  },
    error => {
      this.alerts.push({
        type: 'danger',
        msg: 'Error de conexión',
        timeout: 5000,
        msgStr: 'Ups!'
      });
  });
}
}
