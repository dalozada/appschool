import { Injectable } from '@angular/core';
import { PeriodoModel } from '../models/periodo.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PeriodoService {

 periodoMatricula: PeriodoModel;
  url = 'http://localhost:8080';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient

  ) { }

  public traerPeriodo() {
    // alert('---');
     return this.httpClient.get<PeriodoModel[]>(this.url + '/periodos', this.httpOptions)
     .pipe(map(data => data));
  }
  public traerPeriodosPrograma(idPrograma: number) {
    const url = `${this.url}/periodos/${idPrograma}`;
     return this.httpClient.get<PeriodoModel[]>(url, this.httpOptions)
     .pipe(map(data => data));
  }
  public traerPeriodoApertura(idPrograma: number) {
    const url = `${this.url}/ultimo_periodo/${idPrograma}`;
     return this.httpClient.get<PeriodoModel>(url, this.httpOptions)
     .pipe(map(data => data));
  }
  crearPeriodo(periodo: PeriodoModel ) {
    return this.httpClient.post<PeriodoModel>(this.url + '/periodo', periodo);
  }




  eliminarPeriodo(periodoId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.delete(this.url + '/periodo/' + periodoId, this.httpOptions);
  }

  actualizarPeriodo(periodo: PeriodoModel) {
    const url = `${this.url}/periodos/${periodo.id}`;
    return this.httpClient.put(url, this.httpOptions);
  }



}

