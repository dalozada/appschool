import { Injectable, ɵConsole } from '@angular/core';
import { MateriaModel } from '../models/materia.model';
import { AsignarMateriaProgramaModel } from '../models/asignar-materia-programa.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class MateriaService {

  materia: MateriaModel;
  url = 'http://localhost:8080/colegio';
    httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerMaterias() {
     return this.httpClient.get<MateriaModel[]>(this.url + '/materias', this.httpOptions)
     .pipe(map(data => data));
  }
  public traerAsignacionMateriasPensum(idPrograma: number, idPeriodo: number) {
    const url = `${this.url}/asignacion_materia/programa/${idPrograma}/periodo/${idPeriodo}`;
    return this.httpClient.get<AsignarMateriaProgramaModel[]>(url, this.httpOptions)
    .pipe(map(data => data));
 }
 public actualizarAsignacionMateriasPensum(materiaPrograma: AsignarMateriaProgramaModel) {
  const url = `${this.url}/asignacion_materia/${materiaPrograma.id}`;
  console.log('programa asignacion', materiaPrograma);
  return this.httpClient.put<MateriaModel>(url, materiaPrograma);
}
public cambiarEstadoAsignacionMateriasPensum(idmateriaPrograma: number, idestado: number) {
  const url = `${this.url}/asignacion_materia/${idmateriaPrograma}/estado/${idestado}`;
  return this.httpClient.put(url, idmateriaPrograma);
}
  public traerMateriasPensum(idPrograma: number) {
    return this.httpClient.get<MateriaModel[]>(this.url + '/materias', this.httpOptions)
    .pipe(map(data => data));
 }

  crearMateria(materia: MateriaModel ) {
    return this.httpClient.post<MateriaModel>(this.url + '/materias', materia, this.httpOptions);
  }
  CrearMateriaPrograma(materiaPrograma: AsignarMateriaProgramaModel ) {
    return this.httpClient.post<AsignarMateriaProgramaModel>(this.url + '/asignacion_materia', materiaPrograma, this.httpOptions);
  }

  eliminarMateria(materiaId: number) {
    const url = `${this.url}/materias/${materiaId}`;
    return this.httpClient.delete(url);
  }
  actualizarMateria(materia: MateriaModel) {
    const url = `${this.url}/materias/${materia.id}`;
    return this.httpClient.put<MateriaModel>(url, materia);
  }
  cambiarEstadoMateria(idMateria: number, idEstado: number) {
    const url = `${this.url}/materias/${idMateria}/estado/${idEstado}`;
    return this.httpClient.put<MateriaModel>(url, {idestado: idEstado });
  }
}
