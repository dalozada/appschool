import { MateriaModel } from './materia.model';
import { ProgramaModel } from '../../models/programa.model';
import { ClasificacionModel } from './clasificacion.model';
import { PeriodoModel } from './periodo.model';
import { ContratoModel } from './contrato.model';
import { EstadoModel } from './estado.model';
export interface AsignarMateriaProgramaModel {
  id: number;
  idmateria ?: number;
  idprograma ?: number;
  idclasificacion ?: number;
  idcontrato ?: number;
  idperiodo ?: number;
  idestado ?: number;
  estado ?: EstadoModel;
  materia ?: MateriaModel;
  programa ?: ProgramaModel;
  clasificacion ?: ClasificacionModel;
  periodo ?: PeriodoModel;
  contrato ?: ContratoModel;
}
