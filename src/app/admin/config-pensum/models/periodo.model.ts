export interface PeriodoModel {
  id: number;
  periodo: string;
  fechaInicial: string;
  fechaFinal: string;
  idprograma: number;

}
