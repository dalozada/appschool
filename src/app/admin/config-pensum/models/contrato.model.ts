import { Time } from '@angular/common';
import { PersonaModel } from './persona.model';

export interface ContratoModel {

  id: number;
  idpersona ?: number;
  persona ?: PersonaModel;

}
