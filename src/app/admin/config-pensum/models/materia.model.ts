import { ClasificacionModel } from './clasificacion.model';

export interface MateriaModel {
  id: number;
  tituloMateria: string;
  descripcion: string;
  contenido: string;
  rutaDoc: string;
}
