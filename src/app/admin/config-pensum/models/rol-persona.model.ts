import { PersonaModel } from './persona.model';
import { EstadoModel } from './estado.model';
import { RolModel } from './rol.model';

export interface RolPersonaModel {
  id: number;
  fechaInicio: string;
  fechaFin: string;
  idestado ?: number;
  idpersona ?: number;
  idrol ?: number;

  persona ?: PersonaModel;
  estado ?: EstadoModel;
  rol ?: RolModel;
}
