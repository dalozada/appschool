import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Tipo_rolModel } from '../models/tipo_rol.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TipoRolService {
  tipo_rol: Tipo_rolModel;
  url = 'http://localhost:8080/virtualt';
  permisos: number;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerTipo_rol() {
     return this.httpClient.get<Tipo_rolModel[]>(this.url + '/tiporoles', this.httpOptions)
     .pipe(map(data => data));
  }

  public slider() {
    return this.httpClient.get(this.url + '/slider', this.httpOptions)
    .pipe(map(data => data));
  }

  public tipo_rolByRolUsuario(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traerdescripcionususuario', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public tipo_rolByRol(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traerdescripcion', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public info(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/permisos', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  crearTipo_rol(tipo_rol: Tipo_rolModel ) {
    tipo_rol.descripcionRol = tipo_rol.descripcionRol.toUpperCase();
    return this.httpClient.post<Tipo_rolModel>(this.url + '/guardartiporol', tipo_rol);
  }


  eliminarTipo_rol(tipo_rolId: number) {
    return this.httpClient.post(this.url + '/eliminartiporol', tipo_rolId, this.httpOptions);
  }
  actualizarTipo_rol(tipo_rol: Tipo_rolModel) {
    tipo_rol.descripcionRol = tipo_rol.descripcionRol.toUpperCase();
    return this.httpClient.post(this.url + '/actualizartiporol', tipo_rol, this.httpOptions);
  }
}
