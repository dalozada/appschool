import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { Tipo_rolModel } from '../../models/tipo_rol.model';
import { TipoRolService } from './../../services/tipo-rol.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-tipo-rol-list',
  templateUrl: './tipo-rol-list.component.html',
  styleUrls: ['./tipo-rol-list.component.css']
})
export class TipoRolListComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<Tipo_rolModel>();
  @Input() tipo_rol: Tipo_rolModel[] = [];
  tipo_rolIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private tipo_rolService: TipoRolService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(tipo_rolId: number) {
    this.dangerModal.show();
    this.tipo_rolIdDelete = tipo_rolId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(tipo_rol: Tipo_rolModel) {
    this.openModalUpdate.emit(tipo_rol);
  }

  onDeleteTipo_rol() {

     const pos = this.tipo_rol.indexOf(this.tipo_rol.find( tipo_rol => tipo_rol.id === this.tipo_rolIdDelete));
     this.tipo_rolService.eliminarTipo_rol(this.tipo_rolIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'La registro fue eliminado con éxito',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.tipo_rol.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'La registro no puede ser eliminado',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
