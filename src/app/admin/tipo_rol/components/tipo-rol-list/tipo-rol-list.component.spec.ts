import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoRolListComponent } from './tipo-rol-list.component';

describe('TipoRolListComponent', () => {
  let component: TipoRolListComponent;
  let fixture: ComponentFixture<TipoRolListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoRolListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoRolListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
