import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Tipo_rolModel } from './../../models/tipo_rol.model';
import { TipoRolService } from './../../services/tipo-rol.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-tipo-rol',
  templateUrl: './tipo-rol.component.html',
  styleUrls: ['./tipo-rol.component.css']
})
export class TipoRolComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formTipo_rol: FormGroup;
  modalRef: BsModalRef;
  tipo_rol: Tipo_rolModel;
  objTipo_rol: Tipo_rolModel[] = [];
  alerts: any[] = [];
  tipo_rolIdUpdate: number;
  constructor(
    private tipo_rolService: TipoRolService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.tipo_rol = {
        id: null,
        descripcionRol: '',
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.tipo_rolService.traerTipo_rol()
    .subscribe(tipo_rol => {
      this.objTipo_rol = tipo_rol;
    },
    error => {
      this.alerts.push({
        type: 'danger',
        msg: 'Error de conexion',
        timeout: 5000,
        msgStr: 'Ups!'
      });
    });
  }
  get nameTipo_rolField() {
    return this.formTipo_rol.get('descripcionRol');
  }
  openModalTipo_rol() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.formTipo_rol.reset();
  this.modalRef.hide();

}
  openModalTipo_rolUpdate(tipo_rolObj: Tipo_rolModel) {
     this.tipo_rol = {
      id: tipo_rolObj.id,
      descripcionRol: tipo_rolObj.descripcionRol.toUpperCase(),
    };
    this.tipo_rolIdUpdate = tipo_rolObj.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formTipo_rol = this.formBuilder.group({
      id: [0],
      descripcionRol: ['', [Validators.required]],
    });
    this.formTipo_rol.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarTipo_rol(tipo_rol: Tipo_rolModel) {

    // tslint:disable-next-line: no-shadowed-variable
    const pos = this.objTipo_rol.indexOf(this.objTipo_rol.find( tipo_rol => tipo_rol.id === this.tipo_rolIdUpdate));
    if (this.formTipo_rol.valid) {
      if (this.tipo_rol.id === null ) {

        this.tipo_rolService.crearTipo_rol(this.formTipo_rol.value)
       .subscribe( newTipo_rol => {
        this.alerts.push({
          type: 'success',
          msg: 'La regitro fue creado con éxito',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formTipo_rol.reset();
        this.objTipo_rol.push(newTipo_rol);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.tipo_rolService.actualizarTipo_rol(this.tipo_rol)
         .subscribe( tipo_rolUpdate => {
          this.objTipo_rol[ pos ] = <Tipo_rolModel>tipo_rolUpdate ;
           this.alerts.push({
            type: 'success',
            msg: 'La actualización se ha realizado con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualizacion no pudo completarse',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
