import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoRolComponent } from './tipo-rol.component';

describe('TipoRolComponent', () => {
  let component: TipoRolComponent;
  let fixture: ComponentFixture<TipoRolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoRolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoRolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
