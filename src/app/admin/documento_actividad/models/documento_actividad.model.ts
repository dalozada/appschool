// tslint:disable-next-line: class-name
export interface Documento_actividadModel {
  id: number;
  nombre_documento: string;
  observacion_documento: string;
  contenido: string;
  rutaDoc: string;
}
