import { TestBed } from '@angular/core/testing';

import { DocumentoActividadService } from './documento-actividad.service';

describe('DocumentoActividadService', () => {
  let service: DocumentoActividadService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DocumentoActividadService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
