import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Documento_actividadModel } from './../../documento_actividad/models/documento_actividad.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DocumentoActividadService {
  documento: Documento_actividadModel;
  url = 'http://localhost:8080/colegio';
  permisos: number;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };

  constructor(
    private httpClient: HttpClient
  ) { }

  public traerDocumento() {
    return this.httpClient.get<Documento_actividadModel[]>(this.url + '/documentosactividades', this.httpOptions)
    .pipe(map(data => data));
  }
  public slider() {
    return this.httpClient.get(this.url + '/slider', this.httpOptions)
    .pipe(map(data => data));
  }

  public DocumentoByRolUsuario(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traerdocumentousuario', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public documentoByRol(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traerdocumento', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public info(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/permisos', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  crearDocumento(documento: Documento_actividadModel) {
     documento.nombre_documento = documento.nombre_documento.toUpperCase();
     documento.observacion_documento = documento.observacion_documento.toUpperCase();
     documento.contenido = documento.contenido.toUpperCase();
    return this.httpClient.post<Documento_actividadModel>(this.url + '/guardardocumentoactividades', documento);
  }

  eliminarDocumento(DocumentoId: number) {
    return this.httpClient.post(this.url + '/eliminardocumentoactividades', DocumentoId, this.httpOptions);
  }
  actualizarDocumento(documento: Documento_actividadModel) {
    documento.nombre_documento = documento.nombre_documento.toUpperCase();
     documento.observacion_documento = documento.observacion_documento.toUpperCase();
     documento.contenido = documento.contenido.toUpperCase();
    return this.httpClient.post(this.url + '/actualizardocumentoactividades', documento, this.httpOptions);
  }
}

