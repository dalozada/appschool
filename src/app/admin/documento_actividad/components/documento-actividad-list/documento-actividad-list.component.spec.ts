import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentoActividadListComponent } from './documento-actividad-list.component';

describe('DocumentoActividadListComponent', () => {
  let component: DocumentoActividadListComponent;
  let fixture: ComponentFixture<DocumentoActividadListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentoActividadListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentoActividadListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
