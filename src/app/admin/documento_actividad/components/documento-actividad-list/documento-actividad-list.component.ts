import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { Documento_actividadModel } from '../../../documento_actividad/models/documento_actividad.model';
import { DocumentoActividadService } from './../../services/documento-actividad.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-documento-actividad-list',
  templateUrl: './documento-actividad-list.component.html',
  styleUrls: ['./documento-actividad-list.component.css']
})
export class DocumentoActividadListComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<Documento_actividadModel>();
  @Input() documento: Documento_actividadModel[] = [];
  documentoIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private documentoService: DocumentoActividadService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(documentoId: number) {
    this.dangerModal.show();
    this.documentoIdDelete = documentoId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(documento: Documento_actividadModel) {
    this.openModalUpdate.emit(documento);
  }

  onDeleteDocumento() {

     const pos = this.documento.indexOf(this.documento.find( documento => documento.id === this.documentoIdDelete));
     this.documentoService.eliminarDocumento(this.documentoIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El registro fue eliminado con éxito',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.documento.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El registro no puede ser eliminado',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
