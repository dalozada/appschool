import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentoActividadComponent } from './documento-actividad.component';

describe('DocumentoActividadComponent', () => {
  let component: DocumentoActividadComponent;
  let fixture: ComponentFixture<DocumentoActividadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DocumentoActividadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentoActividadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
