import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Documento_actividadModel } from './../../models/documento_actividad.model';
import { DocumentoActividadService } from './../../services/documento-actividad.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-documento-actividad',
  templateUrl: './documento-actividad.component.html',
  styleUrls: ['./documento-actividad.component.css']
})
export class DocumentoActividadComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formDocumento: FormGroup;
  modalRef: BsModalRef;
  documento: Documento_actividadModel;
  objDocumento: Documento_actividadModel[] = [];
  alerts: any[] = [];
  documentoIdUpdate: number;
  constructor(
    private documentoService: DocumentoActividadService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.documento = {
        id: null,
        nombre_documento: '',
        observacion_documento: '',
       contenido: '',
        rutaDoc: '',
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.documentoService.traerDocumento()
    .subscribe(documento => {
      this.objDocumento = documento;
    },
    error => {
      this.alerts.push({
        type: 'danger',
        msg: 'Error de conexion',
        timeout: 5000,
        msgStr: 'Ups!'
      });
    });
  }
  get nameDocumentoField() {
    return this.formDocumento.get('nombre_documento');
  }
  get observacion_documentoDocumento() {
    return this.formDocumento.get('observacion_documento');
  }
  get contenidodocumentoDocumento() {
    return this.formDocumento.get('contenido');
  }
  openModalRol() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.formDocumento.reset();
  this.modalRef.hide();

}
onFileChange(files: FileList) {
  console.log(files);
}
  openModalDocumentoUpdate(documentoObj: Documento_actividadModel) {
     this.documento = {
      id: documentoObj.id,
      nombre_documento: documentoObj.nombre_documento.toUpperCase(),
      observacion_documento: documentoObj.observacion_documento.toUpperCase(),
      contenido: documentoObj.contenido.toUpperCase(),
      rutaDoc: ''
    };
    this.documentoIdUpdate = this.documento.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formDocumento = this.formBuilder.group({
      id: [null],
      nombre_documento: ['', [Validators.required]],
      observacion_documento: ['', [Validators.required]],
      contenido: ['', [Validators.required]],
      rutaDoc: ['', [Validators.required]],
    });

    this.formDocumento.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarDocumento(documento: Documento_actividadModel) {
    this.formDocumento.value.contenido = this.formDocumento.value.contenido.toUpperCase();
    // tslint:disable-next-line: no-shadowed-variable
    const pos = this.objDocumento.indexOf(this.objDocumento.find( documento => documento.id === this.documentoIdUpdate));
    if (this.formDocumento.valid) {
      if (this.documento.id === null ) {

        this.documentoService.crearDocumento(this.formDocumento.value)
       .subscribe( newDocumento => {
        this.alerts.push({
          type: 'success',
          msg: 'El registro fue creado con éxito',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formDocumento.reset();
        this.objDocumento.push(newDocumento);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.documentoService.actualizarDocumento(this.documento)
         .subscribe( documentoUpdate => {
          this.objDocumento[ pos ] = <Documento_actividadModel>documentoUpdate ;
           this.alerts.push({
            type: 'success',
            msg: 'La actualización se ha realizado con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualización no pudo completarse',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
