import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { TipoProductoModel } from '../../models/tipoProducto.model';
import { TipoProductoService } from '../../services/tipo-producto.service';
import { debounceTime } from 'rxjs/operators';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-tipo-producto',
  templateUrl: './tipo-producto.component.html',
  styleUrls: ['./tipo-producto.component.css']
})
export class TipoProductoComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formTipo: FormGroup;
  modalRef: BsModalRef;
  tipoProducto: TipoProductoModel;
  objTipo: TipoProductoModel[] = [];
  alerts: any[] = [];
  tipoIdUpdate: number;

  claseIdUpdate: number;

  constructor(
    private tipoProductoService: TipoProductoService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
  ) {
    this.tipoProducto = {
      id: null,
      detalle: '',
      descripcion: '',
    };
    this.buildForm();
  }

  ngOnInit(): void {
    this.tipoProductoService.traerTipoProducto()
      .subscribe(tipo => {
        this.objTipo = tipo;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });

        });
  }
  get nameTipoField() {
    return this.formTipo.get('detalle');
  }
  get descripcionTipo() {
    return this.formTipo.get('descripcion');
  }
  openModalTipo() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
  closeModal() {
    this.formTipo.reset();
    this.modalRef.hide();

  }
  openModalTipoUpdate(tipo: TipoProductoModel) {
    this.tipoProducto = {
      id: tipo.id,
      detalle: tipo.detalle.toUpperCase(),
      descripcion: tipo.descripcion,
    };
    this.tipoIdUpdate = tipo.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formTipo = this.formBuilder.group({
      id: [0],
      detalle: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
    });
    this.formTipo.valueChanges
      .pipe(
        debounceTime(350),
      )
      .subscribe(data => {
      });
  }

  guardarTipoProducto(tipo: TipoProductoModel) {

    // const pos = this.objTipo.indexOf(tipo);
    const pos = this.objTipo.indexOf(this.objTipo.find( tip =>tip.id === this.tipoIdUpdate));

    if (this.formTipo.valid) {
      if (this.tipoProducto.id === null) {

        this.tipoProductoService.crearTipoProducto(this.formTipo.value)
          .subscribe(newClase => {
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! El registro fue creado con éxito.',
              timeout: 5000,
              msgStr: 'Proceso Exitoso!'
            });
            this.formTipo.reset();
            this.objTipo.push(newClase);
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser creado.',
                timeout: 5000,
                msgStr: 'Ups!'
              });
            });

      } else {
        this.tipoProductoService.actualizarTipoProducto(this.tipoProducto)
          .subscribe(claseUpdate => {
            this.objTipo[pos] = <TipoProductoModel>claseUpdate;
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
              timeout: 2000,
              msgStr: 'Proceso Exitoso!'
            });
           this.closeModal();

          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser actualizado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }


}
