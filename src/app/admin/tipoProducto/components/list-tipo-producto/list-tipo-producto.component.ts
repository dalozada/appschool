import { Component, OnInit, Output, Input,EventEmitter, ViewChild} from '@angular/core';
import { TipoProductoModel } from '../../models/tipoProducto.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TipoProductoService } from '../../services/tipo-producto.service';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-list-tipo-producto',
  templateUrl: './list-tipo-producto.component.html',
  styleUrls: ['./list-tipo-producto.component.css']
})
export class ListTipoProductoComponent implements OnInit {

  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<TipoProductoModel>();
  @Input() tipoProducto: TipoProductoModel[] = [];
  tipoIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private tipoProductoService: TipoProductoService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(tipoId:number) {
    this.dangerModal.show();
    this.tipoIdDelete = tipoId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(tipoProducto:TipoProductoModel ) {
    this.openModalUpdate.emit(tipoProducto);
  }

  onDeleteTipo() {

    //  const pos = this.tipoProducto.indexOf(tipoProducto);
    //  this.tipoProductoService.eliminarTipoProducto(tipoProducto.id)
     const pos = this.tipoProducto.indexOf(this.tipoProducto.find( tipo => tipo.id === this.tipoIdDelete));

     this.tipoProductoService.eliminarTipoProducto(this.tipoIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: '¡Proceso exitoso! El registro fue eliminado con éxito.',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.tipoProducto.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: '¡Ups! El registro no puede ser eliminado.',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
