import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTipoProductoComponent } from './list-tipo-producto.component';

describe('ListTipoProductoComponent', () => {
  let component: ListTipoProductoComponent;
  let fixture: ComponentFixture<ListTipoProductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTipoProductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTipoProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
