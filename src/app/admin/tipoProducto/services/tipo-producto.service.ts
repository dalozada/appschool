import { Injectable } from '@angular/core';
import { TipoProductoModel } from '../models/tipoProducto.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TipoProductoService {
  tipoProducto: TipoProductoModel;
  url = 'http://localhost:8080';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerTipoProducto() {
    // alert('---');
     return this.httpClient.get<TipoProductoModel[]>(this.url + '/tipo_productos', this.httpOptions)
     .pipe(map(data => data));
  }




  crearTipoProducto(tipoProducto: TipoProductoModel ) {

    tipoProducto.detalle = tipoProducto.detalle.toUpperCase();
    tipoProducto.descripcion = tipoProducto.descripcion.toUpperCase();
    return this.httpClient.post<TipoProductoModel>(this.url + '/tipo_producto',tipoProducto);
  }


  eliminarTipoProducto(tipoProductoId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.delete(this.url + '/tipo_producto/'+tipoProductoId,);
  }

  actualizarTipoProducto(tipoProducto: TipoProductoModel) {
    tipoProducto.detalle = tipoProducto.detalle.toUpperCase();
    tipoProducto.descripcion = tipoProducto.descripcion.toUpperCase();
    return this.httpClient.put(this.url + '/tipo_producto/'+tipoProducto.id,tipoProducto, this.httpOptions);
  }
}
