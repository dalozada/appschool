import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BootstrapModule } from './../bootstrap/bootstrap.module';
import { AdminRoutingModule } from './admin-routing.module';
import { DefaultLayoutComponent } from './../containers';
import {ReactiveFormsModule} from '@angular/forms';

const APP_CONTAINERS = [
  DefaultLayoutComponent
 ];
 import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';
// import { PermisosComponent } from './permisos/components/permisos.component';
import { RolesComponent } from './roles/components/roles/roles.component';

import { MenuListComponent } from './menu/components/menu-list/menu-list.component';
import { MenuComponent } from './menu/components/menu/menu.component';
import { MenuService } from './menu/services/menu.service';
import { InfraestructuraComponent } from './infraestructura/components/infraestructura/infraestructura.component';
import { InfraestructuraListComponent } from './infraestructura/components/infraestructura-list/infraestructura-list.component';
import {MedioPagoComponent} from './medio-pago/components/medio-pago/medio-pago.component';
import {MedioPagoListComponent} from './medio-pago/components/medio-pago-list/medio-pago-list.component';
import {TipoPagoComponent} from './tipo-pago/components/tipo-pago/tipo-pago.component';
import {TipoPagoListComponent} from './tipo-pago/components/tipo-pago-list/tipo-pago-list.component';

import { InfraestructuraService} from './infraestructura/services/infraestructura.service';
import {MedioPagoService} from './medio-pago/services/medio-pago.service';
import {TipoPagoService} from './tipo-pago/services/tipo-pago.service';
import { CaracteristicasComponent } from './caracteristias-producto/components/caracteristicas/caracteristicas.component';
import { CaracteristicasListComponent } from './caracteristias-producto/components/caracteristicas-list/caracteristicas-list.component';
import { RolesListComponent } from './roles/components/roles-list/roles-list.component';
import { RolesService } from './roles/services/roles.service';
import { from } from 'rxjs';
import { PermisosComponent } from './permisos/components/permisos.component';
import { TipoContratoComponent } from './tipoContrato/components/tipo-contrato/tipo-contrato.component';
import { ListTipoContratoComponent } from './tipoContrato/components/list-tipo-contrato/list-tipo-contrato.component';
import { ClaseProductoComponent } from './claseProducto/components/clase-producto/clase-producto.component';
import { ListClaseProductoComponent } from './claseProducto/components/list-clase-producto/list-clase-producto.component';
import { TipoActividadComponent } from './tipoActividad/components/tipo-actividad/tipo-actividad.component';
import { ListTipoActividadComponent } from './tipoActividad/components/list-tipo-actividad/list-tipo-actividad.component';
import { ModuloComponent } from './modulo/components/modulo/modulo.component';
import { ModuloListComponent } from './modulo/components/modulo-list/modulo-list.component';
import { ModuloService } from './modulo/services/modulo.service';
import { ProcesoComponent } from './proceso/components/proceso/proceso.component';
import { ProcesoListComponent } from './proceso/components/proceso-list/proceso-list.component';
import { ProcesoService } from './proceso/services/proceso.service';
import { TipoRolComponent } from './tipo_rol/components/tipo-rol/tipo-rol.component';
import { TipoRolListComponent } from './tipo_rol/components/tipo-rol-list/tipo-rol-list.component';
import { TipoRolService } from './tipo_rol/services/tipo-rol.service';
import { TipoTransaccionComponent } from './tipo_transaccion/components/tipo-transaccion/tipo-transaccion.component';
import { TipoTransaccionListComponent } from './tipo_transaccion/components/tipo-transaccion-list/tipo-transaccion-list.component';
import { TipoTransaccionService } from './tipo_transaccion/services/tipo-transaccion.service';
import { DocumentoMatriculaComponent } from './documento_matricula/components/documento-matricula/documento-matricula.component';
import { DocumentoMatriculaListComponent } from './documento_matricula/components/documento-matricula-list/documento-matricula-list.component';
import { DocumentoMatriculaService } from './documento_matricula/services/documento-matricula.service';
import { ListTipoProductoComponent } from './tipoProducto/components/list-tipo-producto/list-tipo-producto.component';
import { TipoProductoComponent } from './tipoProducto/components/tipo-producto/tipo-producto.component';
import { ClasificacionListComponent } from './clasificacion/components/clasificacion-list/clasificacion-list.component';
import { ClasificacionComponent } from './clasificacion/components/clasificacion/clasificacion.component';
import { ClasificacionService } from './clasificacion/sevices/clasificacion.service';
// import { MateriaComponent } from './materia/components/materia/materia.component';
import { MateriaTabListComponent } from './materia/components/materia-tab-list/materia-tab-list.component';
import { MateriaService } from './materia/sevices/materia.service';
import { DocumentoActividadComponent } from './documento_actividad/components/documento-actividad/documento-actividad.component';
import { DocumentoActividadListComponent } from './documento_actividad/components/documento-actividad-list/documento-actividad-list.component';
import { DocumentoActividadService } from './documento_actividad/services/documento-actividad.service';
import { TipoProgramaComponent } from './tipo_programa/components/tipo-programa/tipo-programa.component';
import { TipoProgramaListComponent } from './tipo_programa/components/tipo-programa-list/tipo-programa-list.component';
import { TipoProgramaService } from './services/tipo-programa.service';
import { ListTipoRespuestaComponent } from './tipoRespuesta/components/list-tipo-respuesta/list-tipo-respuesta.component';
import { TipoRespuestaComponent } from './tipoRespuesta/components/tipo-respuesta/tipo-respuesta.component';
import { PeriodoMatriculaComponent } from './periodoMatricula/components/periodo-matricula/periodo-matricula.component';
import { ListPeriodoMatriculaComponent } from './periodoMatricula/components/list-periodo-matricula/list-periodo-matricula.component';
import { ListFuncionalidadComponent } from './funcionalidad/components/list-funcionalidad/list-funcionalidad.component';
import { FuncionalidadComponent } from './funcionalidad/components/funcionalidad/funcionalidad.component';
import { MyDatePickerModule } from 'mydatepicker';
import { ConfigEmpresaComponent } from './config-empresa/componentes/config-empresa/config-empresa.component';
import { ConfigEmpresaService  } from './config-empresa/services/config-empresa.service';
import { EmpresaComponent } from './config-empresa/componentes/empresa/components/empresa.component';
import { EmpresaService } from './services/empresa.service';
import { AsignarRolEmpresaService } from './config-empresa/services/asignar-rol-empresa.service';
import { SucursalComponent } from './config-empresa/componentes/sucursal/components/sucursal.component';
import { SucursalService } from './config-empresa/services/sucursal.service';
import { SucursalListComponent } from './config-empresa/componentes/sucursal-list/components/sucursal-list.component';
import { NgWizardModule, NgWizardConfig, THEME } from 'ng-wizard';
import { ContratacionComponent } from './contratacion/components/contratacion/contratacion.component';
import { ProgramaComponent } from './config-pensum/components/programa/programa.component';
import { ProgramaFormComponent } from './config-pensum/components/programa-form/programa-form.component';
import { MateriaFormComponent } from './config-pensum/components/materia-form/materia-form.component';
import { ProgramaTabListComponent } from './config-pensum/components/programa-tab-list/programa-tab-list.component';
import { PeriodoProgramaFormComponent } from './config-pensum/components/periodo-programa-form/periodo-programa-form.component';
import { PeriodoProgramaTabComponent } from './config-pensum/components/periodo-programa-tab/periodo-programa-tab.component';
import { ConfigPensumComponent } from './config-pensum/components/config-pensum/config-pensum.component';
import { ProgramaService } from './services/programa.service';
import { PeriodoService } from './config-pensum/services/periodo.service';
import { RolPersonasService } from './services/rol-personas.service';
import { MateriaComponent } from './config-pensum/components/materia/materia.component';
import { MatriculaComponent } from './matricula/components/matricula/matricula.component';
import { JornadaTabComponent } from './config-pensum/components/jornada-tab/jornada-tab.component';
import { JornadaFormComponent } from './config-pensum/components/jornada-form/jornada-form.component';
import { MatriculaService } from './services/matricula.service';
import { JornadaService } from './services/jornada.service';
import { DiaService } from './services/dia.service';
import { DiaListComponent } from './config-pensum/components/dia-list/dia-list.component';
import { MatriculaEstudiantilComponent } from './matricula-estudiantil/components/matricula-estudiantil/matricula-estudiantil.component';
import { EstudianteComponent } from './matricula-estudiantil/components/estudiante/estudiante.component';
import { MateriasPendientesComponent } from './matricula-estudiantil/components/materias-pendientes/materias-pendientes.component';
import { MateriasAprobadasComponent } from './matricula-estudiantil/components/materias-aprobadas/materias-aprobadas.component';
import { MateriasMatriculadasComponent } from './matricula-estudiantil/components/materias-matriculadas/materias-matriculadas.component';
import { BuscarEstudianteComponent } from './matricula-estudiantil/components/buscar-estudiante/buscar-estudiante.component';
import { ProgramasMatriculadosComponent } from './matricula-estudiantil/components/programas-matriculados/programas-matriculados.component';
import { TrayectoriaEstudianteComponent } from './trayectoria-estudiante/components/trayectoria-estudiante/trayectoria-estudiante.component';
const ngWizardConfig: NgWizardConfig = {
  theme: THEME.default
};
@NgModule({
  declarations: [
    ...APP_CONTAINERS,
    ConfigPensumComponent,
    MenuComponent,
    PermisosComponent,
    RolesComponent,
    MenuListComponent,
    InfraestructuraComponent,
    InfraestructuraListComponent,
    MedioPagoComponent,
    MedioPagoListComponent,
    TipoPagoListComponent,
    TipoPagoComponent,
    CaracteristicasComponent,
    CaracteristicasListComponent,
    RolesListComponent,
    TipoContratoComponent,
    ListTipoContratoComponent,
    ClaseProductoComponent,
    ListClaseProductoComponent,
    TipoActividadComponent,
    ListTipoActividadComponent,
    ModuloComponent,
    ModuloListComponent,
    ProcesoComponent,
    ProcesoListComponent,
    TipoRolComponent,
    TipoRolListComponent,
    TipoTransaccionComponent,
    TipoTransaccionListComponent,
    DocumentoMatriculaComponent,
    DocumentoMatriculaListComponent,
    ListTipoProductoComponent,
    TipoProductoComponent,
    ClasificacionListComponent,
    ClasificacionComponent,
    MateriaComponent,
    MateriaTabListComponent,
    DocumentoActividadComponent,
    DocumentoActividadListComponent,
    TipoProgramaComponent,
    TipoProgramaListComponent,
    ListTipoRespuestaComponent,
    TipoRespuestaComponent,
    PeriodoMatriculaComponent,
    ListPeriodoMatriculaComponent,
    ListFuncionalidadComponent,
    FuncionalidadComponent,
    ConfigEmpresaComponent,
    EmpresaComponent,
    SucursalComponent,
    SucursalListComponent,
    ContratacionComponent,
    ProgramaComponent,
    ProgramaFormComponent,
    MateriaFormComponent,
    MateriaComponent,
    ProgramaTabListComponent,
    PeriodoProgramaFormComponent,
    PeriodoProgramaTabComponent,
    MatriculaComponent,
    JornadaTabComponent,
    JornadaFormComponent,
    DiaListComponent,
    MatriculaEstudiantilComponent,
    EstudianteComponent,
    MateriasPendientesComponent,
    MateriasAprobadasComponent,
    MateriasMatriculadasComponent,
    BuscarEstudianteComponent,
    ProgramasMatriculadosComponent,
    TrayectoriaEstudianteComponent
  ],
  imports: [
    NgWizardModule.forRoot(ngWizardConfig),
    CommonModule,
    AdminRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    BootstrapModule,
    ReactiveFormsModule,
    MyDatePickerModule,


  ],
  providers: [
    MenuService,
    InfraestructuraService,
    MedioPagoService,
    TipoPagoService,
    RolesService,
    ModuloService,
    ProcesoService,
    TipoRolService,
    TipoTransaccionService,
    DocumentoMatriculaService,
    ClasificacionService,
    MateriaService,
    DocumentoActividadService,
    TipoProgramaService,
    MyDatePickerModule,
    ConfigEmpresaService,
    EmpresaService,
    SucursalService,
    AsignarRolEmpresaService,
    ProgramaService,
    PeriodoService,
    MateriaService,
    RolPersonasService,
    JornadaService,
    DiaService,
    MatriculaService
  ]
})
export class AdminModule { }
