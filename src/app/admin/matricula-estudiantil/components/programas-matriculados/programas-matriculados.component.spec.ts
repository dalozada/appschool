import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProgramasMatriculadosComponent } from './programas-matriculados.component';

describe('ProgramasMatriculadosComponent', () => {
  let component: ProgramasMatriculadosComponent;
  let fixture: ComponentFixture<ProgramasMatriculadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProgramasMatriculadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProgramasMatriculadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
