import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MateriasMatriculadasComponent } from './materias-matriculadas.component';

describe('MateriasMatriculadasComponent', () => {
  let component: MateriasMatriculadasComponent;
  let fixture: ComponentFixture<MateriasMatriculadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MateriasMatriculadasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MateriasMatriculadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
