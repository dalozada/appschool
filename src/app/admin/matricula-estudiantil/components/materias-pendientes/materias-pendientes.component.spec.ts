import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MateriasPendientesComponent } from './materias-pendientes.component';

describe('MateriasPendientesComponent', () => {
  let component: MateriasPendientesComponent;
  let fixture: ComponentFixture<MateriasPendientesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MateriasPendientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MateriasPendientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
