import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatriculaEstudiantilComponent } from './matricula-estudiantil.component';

describe('MatriculaEstudiantilComponent', () => {
  let component: MatriculaEstudiantilComponent;
  let fixture: ComponentFixture<MatriculaEstudiantilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatriculaEstudiantilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatriculaEstudiantilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
