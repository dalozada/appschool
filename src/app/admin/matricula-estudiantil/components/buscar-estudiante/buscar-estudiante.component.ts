import { Component, OnInit } from '@angular/core';
import { MatriculaService } from '../../../services/matricula.service';
import { ProgramaModel } from '../../../models/programa.model';
import { MatriculaModel } from '../../../models/matricula.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-buscar-estudiante',
  templateUrl: './buscar-estudiante.component.html',
  styleUrls: ['./buscar-estudiante.component.css']
})
export class BuscarEstudianteComponent implements OnInit {
  
  programas: ProgramaModel[] = [];
  matricula: MatriculaModel;
  objsMatriculas: MatriculaModel[] = [];
  objsProgramas: ProgramaModel[] = [];
  alerts: any[] = [];
  formSearchEstudiante: FormGroup;
  constructor(
    private matriculaService: MatriculaService,
    private formBuilder: FormBuilder,
  ) {
    this.matricula = {
      id: null,
      fecha: null,
      idacudiente: null,
      idpersona: null,
      idprograma: null,
    };
  }

  ngOnInit(): void {
    this.buildForm();
  }
  private buildForm() {
    this.formSearchEstudiante = this.formBuilder.group({
     identificacion: ['', [Validators.required]]
    });

    this.formSearchEstudiante.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }
  get identificacionEstudiante() {
    return this.formSearchEstudiante.get('identificacion');
  }
  buscarMatriculasEstudiante() {
  this.matriculaService.traerMatriculasEstudiante(this.formSearchEstudiante.value.identificacion)
  .subscribe(matriculas => {
    this.objsMatriculas = matriculas;
  },
    error => {
      this.alerts.push({
        type: 'danger',
        msg: 'Error de conexión',
        timeout: 5000,
        msgStr: 'Ups!'
      });
  });
  }
}
