import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { MateriaModel } from './../models/materia.model';

@Injectable({
  providedIn: 'root'
})
export class MateriaService {

  materiaModel: MateriaModel;
  url = 'http://localhost:8080';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerMaterias() {
     return this.httpClient.get<MateriaModel[]>(this.url + '/materia', this.httpOptions)
     .pipe(map(data => data));
  }

  crearMateria(materia: MateriaModel ) {
    return this.httpClient.post<MateriaModel>(this.url + '/materia', materia, this.httpOptions);
  }


  eliminarMateria(materiaId: number) {
    const url = `${this.url}/materia/${materiaId}`;
    console.log(url);
    return this.httpClient.delete(url);
  }
  actualizarMateria(materia: MateriaModel) {
    const url = `${this.url}/materia/${materia.id}`;
    return this.httpClient.put<MateriaModel>(url, materia);
  }
}
