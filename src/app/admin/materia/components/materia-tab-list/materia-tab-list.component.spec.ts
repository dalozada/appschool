import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MateriaTabListComponent } from './materia-tab-list.component';

describe('MateriaTabListComponent', () => {
  let component: MateriaTabListComponent;
  let fixture: ComponentFixture<MateriaTabListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MateriaTabListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MateriaTabListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
