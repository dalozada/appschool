import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { MateriaModel } from './../../models/materia.model';
import { MateriaService } from './../../sevices/materia.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-materia-tab-list',
  templateUrl: './materia-tab-list.component.html',
  styleUrls: ['./materia-tab-list.component.css']
})
export class MateriaTabListComponent implements OnInit {

  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<MateriaModel>();
  @Input() materias: MateriaModel[] = [];
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  materiaIdDelete: number;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private materiaService: MateriaService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(infraId: number) {
    this.dangerModal.show();
    this.materiaIdDelete = infraId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(materia: MateriaModel) {
    this.openModalUpdate.emit(materia);
  }

  onDeleteInfraestructura() {
    const pos = this.materias.indexOf(this.materias.find( mat => mat.id === this.materiaIdDelete));
     this.materiaService.eliminarMateria(this.materiaIdDelete)
     .subscribe(rta => {
       this.alerts.push({
        type: 'success',
        msg: 'La asignatura fué eliminada exitosamente',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.materias.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'La asignatura no puede ser eliminada',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
    }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
