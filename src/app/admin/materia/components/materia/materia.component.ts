import { Component, OnInit, ViewChild, TemplateRef, ElementRef } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import { MateriaModel } from './../../models/materia.model';
import { MateriaService } from './../../sevices/materia.service';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-materia',
  templateUrl: './materia.component.html',
  styleUrls: ['./materia.component.css']
})
export class MateriaComponent implements OnInit {
  @ViewChild('labelImport') labelImport: ElementRef;
  fileToUpload: File = null;
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formMateria: FormGroup;
  modalRef: BsModalRef;
  materia: MateriaModel;
  objMaterias: MateriaModel [] = [];
  materiaIdUpdate: number;
  alerts: any[] = [];
  constructor(
    private materiaService: MateriaService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.materia = {
        id: null,
        tituloMateria: '',
        descripcion: '',
        contenido: '',
        rutaDoc: ''
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.materiaService.traerMaterias()
    .subscribe(materias => {
      this.objMaterias = materias;
    },
    error => {
      this.alerts.push({
        type: 'danger',
        msg: 'Error de conexion',
        timeout: 5000,
        msgStr: 'Ups!'
      });
  });

  }
  get tituloMateriaField() {
    return this.formMateria.get('tituloMateria');
  }
  get descripcionMateriaField() {
    return this.formMateria.get('descripcion');
  }
  get contenidoMateriaField() {
    return this.formMateria.get('contenido');
  }
  // get documentoMateriaField() {
  //   return this.formMateria.get('rutaDoc');
  // }
  openModalMateria() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
  closeModal() {
    this.formMateria.reset();
    this.modalRef.hide();

  }
  onFileChange(files: FileList) {
    console.log(files);
    // this.labelImport.nativeElement.innerText = Array.from(files)
    //   .map(f => f.name)
    //   .join(', ');
    // this.fileToUpload = files.item(0);
  }
  openModalMateriaUpdate(materiaObj: MateriaModel) {
    this.materia = {
      id: materiaObj.id,
      tituloMateria: materiaObj.tituloMateria.toUpperCase(),
        descripcion: materiaObj.descripcion.toUpperCase(),
        contenido: materiaObj.contenido.toUpperCase(),
        rutaDoc: ''
    };
    this.materiaIdUpdate = materiaObj.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formMateria = this.formBuilder.group({
      id: [null],
      tituloMateria: ['', [Validators.required]],
      descripcion: [null, [Validators.required]],
      contenido: ['', [Validators.required]],
      rutaDoc: ['', Validators.required]
    });
    this.formMateria.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarMateria() {
    this.formMateria.value.descripcion = this.formMateria.value.descripcion.toUpperCase();
    this.formMateria.value.contenido = this.formMateria.value.contenido.toUpperCase();
    const pos = this.objMaterias.indexOf(this.objMaterias.find( men => men.id === this.materiaIdUpdate));
    if (this.formMateria.valid) {
      if (this.materia.id === null) {
        this.materiaService.crearMateria(this.formMateria.value)
       .subscribe( newMateria => {
        this.alerts.push({
          type: 'success',
          msg: 'La materia fue creado exitosamente',
          timeout: 5000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formMateria.reset();
        this.objMaterias.push(newMateria);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 5000,
          msgStr: 'Ups!'
        });
      });
      } else {
         this.materiaService.actualizarMateria(this.formMateria.value)
         .subscribe( materiaUpdate => {
          this.alerts.push({
            type: 'success',
            msg: 'La actualizacion se realizao con exito',
            timeout: 2000,
            msgStr: 'Proceso Exitoso!'
          });
          this.objMaterias[ pos ] = <MateriaModel>materiaUpdate;
          this.modalRef.hide();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: error,
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
