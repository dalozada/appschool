import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgWizardConfig, THEME, StepChangedArgs, NgWizardService } from 'ng-wizard';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PersonaModel } from '../../models/persona.model';
import { CiudadModel } from '../../models/ciudad.model';
import { DepartamentoModel } from '../../models/departamento.model';
import { TipoIdentificacionModel } from '../../models/tipoIdentificacion.model';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { PersonaService } from '../../service/persona.service';
import { CiudadService } from '../../service/ciudad.service';
import { DepartamentoService } from '../../service/departamento.service';
import { debounceTime } from 'rxjs/operators';
import { TipoContratoService } from '../../../tipoContrato/services/tipo-contrato.service';
import { TipoContratoModel } from '../../../tipoContrato/models/tipoContrato.model';
import { ContratoModel } from '../../models/contrato.model';
import { ContratacionService } from '../../service/contratacion.service';
import { RolModel } from '../../../roles/components/models/rol.model';
import { RolesService } from '../../../roles/services/roles.service';
import { AsignacionRolModel } from '../../models/asignacionrol.model';
import { ThrowStmt } from '@angular/compiler';
import { AsignacionrolService } from '../../service/asignacionrol.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TipoContratoComponent } from '../../../tipoContrato/components/tipo-contrato/tipo-contrato.component';
import { RolesComponent } from '../../../roles/components/roles/roles.component';



@Component({
  selector: 'app-contratacion',
  templateUrl: './contratacion.component.html',
  styleUrls: ['./contratacion.component.css']
})
export class ContratacionComponent implements OnInit {

  // @Output() openModalNew = new EventEmitter();
  @ViewChild(TipoContratoComponent) formTp: TipoContratoComponent;
  @ViewChild(RolesComponent) formTpr: RolesComponent;

  formPersona: FormGroup;
  formUbicacion: FormGroup;
  formContacto: FormGroup;
  formContrato: FormGroup;
  persona: PersonaModel;
  contrato: ContratoModel;
  fechaPrevia: any;
  asignacion: AsignacionRolModel;
  rol: RolModel;
  departamento: DepartamentoModel;
  departamentoU: DepartamentoModel;
  tipoIdentificacionR: TipoIdentificacionModel;
  ciudadN: CiudadModel;
  ciudadU: CiudadModel;
  idPersona: number;
  objCiudad: CiudadModel[] = [];
  objContrato: ContratoModel[] = [];
  objCiudadDep: CiudadModel[] = [];
  objPersonaId: PersonaModel[] = [];
  objCiudadDepU: CiudadModel[] = [];
  objDep: DepartamentoModel[] = [];
  objPersona: PersonaModel[] = [];
  objRol: RolModel[] = [];
  objTipoId: TipoIdentificacionModel[] = [];
  objTipoContrato: TipoContratoModel[] = [];
  index: number;
  per: any;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;


  public myDatePickerOptionsFin: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    openSelectorOnInputClick: true,
    showInputField: true,
    inline: false,
    editableDateField: false,
    disableSince: {

      year: new Date().getFullYear(),
      month: new Date().getUTCMonth() + 1,
      day: new Date().getDate() + 1

    }

  }

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    openSelectorOnInputClick: true,
    showInputField: true,
    inline: false,
    editableDateField: false


  }


  public myDatePickerOptionsFinC: IMyDpOptions = {
    // other options...
    dateFormat: 'yyyy-mm-dd',
    openSelectorOnInputClick: true,
    showInputField: true,
    inline: false,
    editableDateField: false


  }
  openModalCrear() {
    this.formTp.templatePr.show();
  }
  openModalCrearRol() {
    this.formTpr.templateTp.show();
  }
  public onDateChanged(event: IMyDateModel) {

    const copy: IMyDpOptions = this.getCopyOfEndDateOptions();
    copy.disableUntil = event.date;
    this.myDatePickerOptionsFinC = copy;
    // this.myDatePickerOptionsFin.disableUntil=event.date;


  }

  getCopyOfEndDateOptions(): IMyDpOptions {
    return JSON.parse(JSON.stringify(this.myDatePickerOptionsFinC));
  }

  setDate(dateS: Date): void {
    console.log(dateS);
    // Set today date using the patchValue function
    const date = new Date(dateS);
    this.formPersona.patchValue({
      fechaNac: {
        date: {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate() + 1
        }
      }
    });
  }

  alerts: any[] = [];
  perIdUpdate: number;

  config: NgWizardConfig = {
    selected: 0,
    theme: THEME.arrows,
    toolbarSettings: {
      // showNextButton:true,
      toolbarExtraButtons: [
        {

          text: 'Finish', class: 'btn btn-info btnFin', event: () => {

            if (this.formContacto.valid) {
              this.guardarPersona(this.persona);
            } else {
              this.alerts.push({
                type: 'danger',
                msg: 'Debes completar todos los pasos',
                timeout: 5000,
                msgStr: 'Ups!'
              });
            }


          }


        }
      ]
    }
  };

  constructor(
    private personaService: PersonaService,
    private ciudadService: CiudadService,
    private tipoContratoService: TipoContratoService,
    private contratoService: ContratacionService,
    private rolService: RolesService,
    private departamentoService: DepartamentoService,
    private asignacionService: AsignacionrolService,
    private formBuilder: FormBuilder,
    private ngWizardService: NgWizardService
  ) {

    this.idPersona = null;
    this.persona = {
      id: null,
      nombre1: '',
      apellido1: '',
      nombre2: '',
      apellido2: '',
      fechaNac: null,
      ciudadNac: { id: null, departamento: null, codigo: '', descripcion: '' },
      ciudad: { id: null, departamento: null, codigo: '', descripcion: '' },
      celular: '',
      direccion: '',
      email: '',
      tipoIdentificacion: { id: null, codigo: '', detalle: '' },
      ciudadUbicacion: { id: null, departamento: null, codigo: '', descripcion: '' },
      identificacion: '',
      rutaFoto: '',
      telefonoFijo: '',

    };

    this.asignacion = {

      fechaInicio: null,
      fechaFin: null,
      idestado: null,
      idrol: null,
      idpersona: null


    }
    this.rol = {
      id: null,
      descripcion: null,
      idestado: null,
      idtipoRol: null,
      nombreRol: '',

    }

    this.fechaPrevia = null;

    this.contrato = {

      id: null,
      fechaContratacion: null,
      fechaFinalContrato: null,
      horaSistema: "00:00:00",
      idpersona: null,
      perfilProfesional: '',
      otrosi: 'N',
      idtipoContrato: null,
      numeroContrato: '',
      observacion: '',

    }
    this.departamento = { id: null, descripcion: '' };
    this.departamentoU = {
      id: null,
      descripcion: ''
    };

    this.tipoIdentificacionR = {
      id: null,
      codigo: '',
      detalle: ''
    };
    this.ciudadN = { id: null, departamento: { id: null, descripcion: '' }, codigo: '', descripcion: '' };
    this.ciudadU = { id: null, departamento: { id: null, descripcion: '' }, codigo: '', descripcion: '' };


    this.buildForm();




  }

  ngOnInit(): void {
    this.traerDepartamentos();
    this.traerCiudadesByDep();
    this.traerTipoId();
    this.traerTipoContrato();
    this.traerPersonasById();
    this.traerRol();
    this.index = 0;

  }

  validarSteps() {
    if (this.index == 0) {
      return this.formPersona.valid
    } else if (this.index == 1) {
      return this.formUbicacion.valid

    } else if (this.index == 2) {
      return this.formContacto.valid
    }
    return false;

  }



  guardarAsignacion(asignacion: AsignacionRolModel) {

    this.asignacion.idpersona = this.idPersona;
    this.asignacion.idrol = this.formContacto.value.rol.id;
    this.asignacionService.crearAsignacion(asignacion)
      .subscribe(newAsig => {
        console.log('guardando asignacion', newAsig);
        this.alerts.push({
          type: 'success',
          msg: '¡Proceso exitoso! El registro fue creado con éxito.',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        // this.formPersona.reset();
        // this.traerFuncionalidad();
        // this.objFunc.push(newFunc);
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: '¡Ups! El registro no puede ser creado.',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });

  }
  guardarPersona(personaModel: PersonaModel) {
    // alert(personaModel);

    const pos = this.objPersona.indexOf(this.objPersona.find(persona => persona.id === this.perIdUpdate));
    if (this.formPersona.valid) {
      if (this.persona.id === null) {

        console.log(personaModel);
        this.persona.ciudadNac = this.ciudadN;
        this.persona.ciudadUbicacion = this.ciudadU;
        this.persona.tipoIdentificacion = this.tipoIdentificacionR;

        this.personaService.crearPersona(personaModel)
          .subscribe(newPer => {
            this.per = newPer;
            this.idPersona = this.per.idpersona;
            console.log(this.per.idpersona, 'persona creada id ');
            // this.alerts.push({
            //   type: 'success',
            //   msg: '¡Proceso exitoso! El registro fue creado con éxito.',
            //   timeout: 2000,
            //   msgStr: 'Proceso Exitoso!'
            // });
            this.contrato.idpersona = this.idPersona;
            this.asignacion.idpersona = this.idPersona;
            this.asignacion.idrol = this.rol.id;
            this.guardarContrato(this.contrato);
            this.guardarAsignacion(this.asignacion);

            this.formPersona.reset();
            this.formUbicacion.reset();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser creado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });

      } else {
        if (!this.persona.fechaNac) {
          this.persona.fechaNac = this.fechaPrevia
        }
        this.persona.ciudadNac = this.ciudadN;
        this.persona.ciudadUbicacion = this.ciudadU;
        this.persona.tipoIdentificacion = this.tipoIdentificacionR;
        this.personaService.actualizarPersona(this.persona)
          .subscribe(funcUpdate => {

            //  this.traerFuncionalidad();
            // this.objFunc[ pos ] = <FuncionalidadModel>funcUpdate ;
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
              timeout: 1000,
              msgStr: 'Proceso Exitoso!'
            });
            this.contrato.idpersona = this.idPersona;
            this.guardarContrato(this.contrato);
            // this.closeModal();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser actualizado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });
      }
    }

  }

  // validarFinish(){
  //   let btn = document.getElementsByClassName('btnFin');
  //   if(this.formContacto.invalid||this.formPersona.invalid||this.formUbicacion.invalid){

  //      btn.disabled=true;
  //   }
  // }

  guardarContrato(contrato: ContratoModel) {
    // alert(personaModel);

    console.log(this.idPersona, 'personaContrato');
    const pos = this.objContrato.indexOf(this.objContrato.find(func => func.id === this.perIdUpdate));
    if (this.formPersona.valid) {
      if (this.contrato.id === null) {

        this.contrato.idpersona = this.idPersona;
        // console.log(this.idPersona,'idpersonaencontrato')
        this.contratoService.crearContrato(contrato)
          .subscribe(newPer => {
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! El registro fue creado con éxito.',
              timeout: 2000,
              msgStr: 'Proceso Exitoso!'
            });
            this.formContacto.reset();
            this.buildForm();
            this.index = 0;
            this.resetWizard();
            // this.traerFuncionalidad();
            // this.objFunc.push(newFunc);
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser creado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });

      } else {
        this.contratoService.actualizarContrato(this.contrato)
          .subscribe(funcUpdate => {
            //  this.traerFuncionalidad();
            // this.objFunc[ pos ] = <FuncionalidadModel>funcUpdate ;
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
              timeout: 1000,
              msgStr: 'Proceso Exitoso!'
            });
            // this.closeModal();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser actualizado.',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });
      }
    }

  }





  //Datos de la persona
  get idtipoIdentificacionField() {
    return this.formPersona.get('tipoIdentificacion');
  }
  get identificacionField() {
    return this.formPersona.get('identificacion');
  }
  get nombre1Field() {
    return this.formPersona.get('nombre1');
  }

  get apellido1Field() {
    return this.formPersona.get('apellido1');
  }

  get idciudadNacField() {
    return this.formPersona.get('ciudadNac');
  }
  get departamentoField() {
    return this.formPersona.get('departamento');
  }
  get fechaNacField() {
    return this.formPersona.get('fechaNac');
  }

  // datos ubicacion
  get ciudadField() {
    return this.formUbicacion.get('ciudad');
  }
  get departamentoUField() {
    return this.formUbicacion.get('departamentoU');
  }
  get direccionField() {
    return this.formUbicacion.get('direccion');
  }
  get emailField() {
    return this.formUbicacion.get('email');
  }
  get celularField() {
    return this.formUbicacion.get('celular');
  }
  get telefonoFijoField() {
    return this.formUbicacion.get('telefonoFijo');

  }
  get idciudadUbicacionField() {
    return this.formUbicacion.get('ciudadUbicacion');
  }

  //datos contrato
  get fechaContratacionField() {
    return this.formContacto.get('fechaContratacion');
  }

  get fechaFinalContratoField() {
    return this.formContacto.get('fechaFinalContrato');
  }
  get idtipoContratoField() {
    return this.formContacto.get('idtipoContrato');
  }
  get observacionField() {
    return this.formContacto.get('observacion');
  }
  get numeroContratoField() {
    return this.formContacto.get('numeroContrato');
  }
  get idrolField() {
    return this.formContacto.get('rol');
  }

  // datos del contrato


  traerCiudades() {
    this.ciudadService.traerCiudades()
      .subscribe(ciudad => {
        this.objCiudadDep = ciudad;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }

  traerTipoId() {
    this.personaService.traerTiposId()
      .subscribe(tipoId => {
        this.objTipoId = tipoId;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }





  traerPersonasById() {
    console.log(document.getElementById("identificacion")['value'], '----***');
    this.personaService.personaByIdentificacion(document.getElementById("identificacion")['value'])
      .subscribe((persona: PersonaModel[]) => {
        // this.objPersonaId = persona;
        // this.persona=persona;
        this.setDate(persona[0].fechaNac);
        this.persona = {

          id: persona[0].id,
          nombre1: persona[0].nombre1,
          apellido1: persona[0].apellido1,
          nombre2: persona[0].nombre2,
          apellido2: persona[0].apellido2,
          fechaNac: persona[0].fechaNac,
          ciudadNac: persona[0].ciudadNac,
          idciudad: 1,
          celular: persona[0].celular,
          direccion: persona[0].direccion,
          email: persona[0].email,
          tipoIdentificacion: persona[0].tipoIdentificacion,
          ciudadUbicacion: persona[0].ciudadUbicacion,
          identificacion: persona[0].identificacion,
          rutaFoto: persona[0].rutaFoto,
          telefonoFijo: persona[0].telefonoFijo,


        };

        // this.formPersona.controls['departamento'].setValue(null);
        console.log(this.departamento = persona[0].ciudadNac.departamento, 'departamento');
        this.departamento = persona[0].ciudadNac.departamento;
        this.traerCiudadesByDep();
        console.log(this.ciudadN = persona[0].ciudadNac, 'ciudad')
        // this.ciudadN=persona[0].ciudadNac;
        Object.assign(this.ciudadN, persona[0].ciudadNac);
        this.departamentoU = persona[0].ciudadUbicacion.departamento;
        this.traerCiudadesByDepU();
        this.ciudadU = persona[0].ciudadUbicacion;
        this.tipoIdentificacionR = persona[0].tipoIdentificacion;
        this.fechaPrevia = persona[0].fechaNac;

        // this.formPersona.controls['departamento'].setValue(persona[0].ciudadNac.departamento);
        // this.setDatosPersona(persona);
        console.log(persona);
        console.log(this.persona, 'llego persona---++++');
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
    // this.traerCiudadesByDepU();
  }

  compare(a: any, b: any) {

    return a && b && a.id === b.id
  }

  traerCiudadesByDep() {
    console.log(document.getElementById("departamento")['value']);
    if (document.getElementById("departamento")['value']) {
      this.ciudadService.ciudadesByDep(this.departamento.id)
        .subscribe((ciudad: CiudadModel[]) => {
          this.objCiudadDep = ciudad;
          console.log(this.objCiudadDep);
        },
          error => {
            this.alerts.push({
              type: 'danger',
              msg: 'Error de conexion',
              timeout: 5000,
              msgStr: 'Ups!'
            });
          });

    } else {
      this.traerCiudades();
    }
  }

  traerCiudadesByDepU() {
    // console.log(document.getElementById("departamentoU")['value'],'id dep');
    // this.ciudadService.ciudadesByDep(document.getElementById("departamentoU")['value'])
    this.ciudadService.ciudadesByDep(this.departamentoU.id)
      .subscribe((ciudad: CiudadModel[]) => {
        this.objCiudadDepU = ciudad;
        console.log(this.objCiudadDep);
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }

  traerDepartamentos() {
    this.departamentoService.traerDepartamentos()
      .subscribe(dep => {
        this.objDep = dep;
        console.log(this.objDep, 'objeto dep');
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }

  traerRol() {
    this.rolService.rolByEstado('ACTIVO', 'LABORAL')
      .subscribe((rol: RolModel[]) => {
        this.objRol = rol;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }

  traerTipoContrato() {

    this.tipoContratoService.traerTipoContrato()
      .subscribe(contrato => {
        this.objTipoContrato = contrato;
        // console.log(this.objTipoContrato);
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });

  }

  private buildForm() {



    this.formPersona = this.formBuilder.group({

      id: [0],
      tipoIdentificacion: ['', [Validators.required]],
      identificacion: ['', [Validators.required]],
      nombre1: ['', [Validators.required]],
      nombre2: ['', []],
      apellido1: ['', [Validators.required]],
      apellido2: ['', []],
      departamento: [null, [Validators.required]],
      ciudadNac: [null, [Validators.required]],
      fechaNac: ['', [Validators.required]],
      rutaFoto: [''],
      // idpersona: ['', [Validators.required]],

    });


    this.formUbicacion = this.formBuilder.group({

      idciudad: ['',],
      celular: ['', [Validators.required]],
      direccion: ['', [Validators.required]],
      email: ['', [Validators.required]],
      telefonoFijo: ['', [Validators.required]],
      ciudadUbicacion: [null, [Validators.required]],
      departamentoU: [null, [Validators.required]],




    });

    this.formContacto = this.formBuilder.group({

      //contrato
      fechaContratacion: ['', [Validators.required]],
      fechaFinalContrato: ['', [Validators.required]],
      idtipoContrato: ['', [Validators.required]],
      observacion: [''],
      numeroContrato: ['', [Validators.required]],
      otrosi: ['', []],
      rol: ['', [Validators.required]],

    });

    this.formContrato = this.formBuilder.group({

      //contrato
      rutaDoc: ['', [Validators.required]],


    });



    // console.log(this.formPersona.valid, '--validper')

    this.formPersona.valueChanges
      .pipe(
        debounceTime(350),
      )
      .subscribe(data => {

        console.log(data, 'kjkjkjk');
      });
  }

  limpiarValidaciones() {

    // this.formPersona.get('idPersona').updateValueAndValidity();
    console.log(this.formPersona.value);
    console.log(this.formUbicacion, 'validaciones');

    // alert();
  }

  onFileChange(files: FileList) {
    console.log(files);
  }

  showPreviousStep(event?: Event) {
    this.ngWizardService.previous();
  }

  showNextStep(event?: Event) {
    alert(event);
    this.ngWizardService.next();

  }

  resetWizard(event?: Event) {

    this.ngWizardService.reset();
  }

  setTheme(theme: THEME) {
    this.ngWizardService.theme(theme);
  }

  stepChanged(args: StepChangedArgs) {

    // console.log(args.step.index)
    this.index = args.step.index;
    // this.buildForm();
  }

}
