import { TestBed } from '@angular/core/testing';

import { AsignacionrolService } from './asignacionrol.service';

describe('AsignacionrolService', () => {
  let service: AsignacionrolService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AsignacionrolService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
