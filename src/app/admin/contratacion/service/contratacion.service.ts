import { Injectable } from '@angular/core';
import { ContratoModel } from '../models/contrato.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { PersonaModel } from '../models/persona.model';

@Injectable({
  providedIn: 'root'
})
export class ContratacionService {
  contrato: ContratoModel;
  url = 'http://localhost:8080';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }


  public traerContrato() {
    // alert('---');
     return this.httpClient.get<ContratoModel[]>(this.url + '/contratos', this.httpOptions)
     .pipe(map(data => data));
  }


  crearContrato(contrato: ContratoModel) {
    console.log(contrato,'----llego contrato---');
    contrato.fechaContratacion=contrato.fechaContratacion["formatted"]
    contrato.fechaFinalContrato=contrato.fechaFinalContrato["formatted"]

    return this.httpClient.post<ContratoModel>(this.url + '/contrato', contrato);
  }


  // eliminarPersona(personaId: number) {
  //   // const url = `${this.path}/${todoId}`;
  //   return this.httpClient.post(this.url + '/persona', personaId, this.httpOptions);
  // }

  actualizarContrato(contrato: ContratoModel) {


    return this.httpClient.put(this.url + '/persona/'+contrato.id, contrato, this.httpOptions);
  }
}
