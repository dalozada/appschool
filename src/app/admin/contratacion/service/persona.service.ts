import { Injectable } from '@angular/core';
import { PersonaModel } from '../models/persona.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { TipoIdentificacionModel } from '../models/tipoIdentificacion.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  persona: PersonaModel;
  url = 'http://localhost:8080';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }


  public traerTiposId() {
    // alert('---');
     return this.httpClient.get<TipoIdentificacionModel[]>(this.url + '/virtualt/tiposidentificacion', this.httpOptions)
     .pipe(map(data => data));
  }


  crearPersona(persona: PersonaModel ) {
    console.log(persona, '----llego---');

    persona.idtipoIdentificacion = persona.tipoIdentificacion.id;
    persona.identificacion = persona.identificacion;
    persona.nombre1 = persona.nombre1.toUpperCase();
    persona.nombre2 = persona.nombre2.toUpperCase();
    persona.apellido1 = persona.apellido1.toUpperCase();
    persona.apellido2 = persona.apellido2.toUpperCase();
    persona.direccion = persona.direccion.toUpperCase();
    persona.idciudadNac = persona.ciudadNac.id;
    persona.idciudad = persona.ciudad.id;
    persona.telefonoFijo = persona.telefonoFijo;
    persona.celular = persona.celular;
    persona.idciudadUbicacion = persona.ciudadUbicacion.id;
    persona.email = persona.email;
    persona.fechaNac = persona.fechaNac[ 'formatted' ];
    console.log(persona);
    // return this.httpClient.post<PersonaModel>(this.url + '/persona', persona);
    return this.httpClient.post<PersonaModel>(this.url + '/virtualt/persona_usuario', persona);
  }


  eliminarPersona(personaId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.post(this.url + '/persona', personaId, this.httpOptions);
  }

  public personaByIdentificacion(identificacion: number) {

    return this.httpClient.get(this.url + '/personas/identificacion/' + identificacion, this.httpOptions)
    .pipe(map(data => data));

  }
  public getAcudiente(idPersona: number) {

    return this.httpClient.get(this.url + '/colegio/acudiente/estudiante/' + idPersona, this.httpOptions)
    .pipe(map(data => data));

  }

  actualizarPersona(persona: PersonaModel) {
console.log(persona.fechaNac,'lol');
    let fechaNac:any;
    if(typeof persona.fechaNac==='string'){

      fechaNac=persona.fechaNac;

    }else{

       fechaNac=persona.fechaNac["formatted"];

    }

    persona.idtipoIdentificacion = persona.tipoIdentificacion.id;
    persona.identificacion = persona.identificacion;
    persona.nombre1 = persona.nombre1.toUpperCase();
    persona.nombre2 = persona.nombre2.toUpperCase();
    persona.apellido1 = persona.apellido1.toUpperCase();
    persona.apellido2 = persona.apellido2.toUpperCase();
    persona.direccion = persona.direccion.toUpperCase();
    persona.idciudadNac = persona.ciudadNac.id;
    // persona.idciudad = persona.ciudad.id;
    persona.telefonoFijo = persona.telefonoFijo;
    persona.celular = persona.celular;
    persona.idciudadUbicacion = persona.ciudadUbicacion.id;
    persona.email = persona.email;
    persona.fechaNac = fechaNac;
    return this.httpClient.put(this.url + '/persona/' + persona.id, persona, this.httpOptions);
  }





}
