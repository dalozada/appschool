import { Injectable } from '@angular/core';
import { AsignacionRolModel } from '../models/asignacionrol.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AsignacionrolService {
  asignacionRol:AsignacionRolModel;
  url = 'http://localhost:8080';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }


  // public traerContrato() {
  //   // alert('---');
  //    return this.httpClient.get<ContratoModel[]>(this.url + '/contratos', this.httpOptions)
  //    .pipe(map(data => data));
  // }


  crearAsignacion(asignacionRol: AsignacionRolModel) {
    console.log(asignacionRol,'----llego signacion---');

    return this.httpClient.post<AsignacionRolModel>(this.url + '/asignacion_rol_persona', asignacionRol);
  }
}
