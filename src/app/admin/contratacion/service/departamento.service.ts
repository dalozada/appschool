import { Injectable } from '@angular/core';
import { DepartamentoModel } from '../models/departamento.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DepartamentoService {
  departamento:DepartamentoModel;
  url = 'http://localhost:8080/virtualt';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerDepartamentos() {
    // alert('---');
     return this.httpClient.get<DepartamentoModel[]>(this.url + '/departamentos', this.httpOptions)
     .pipe(map(data => data));
  }





}
