import { Time } from '@angular/common';

export interface ContratoModel{

  id:number,
  fechaContratacion:Date,
  fechaFinalContrato:Date,
  horaSistema:string,
  idpersona:number,
  perfilProfesional:string,
  otrosi:string,
  idtipoContrato:number,
  numeroContrato:string,
  observacion:string

}
