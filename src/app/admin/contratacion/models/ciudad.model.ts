import { DepartamentoModel } from './departamento.model';

export interface CiudadModel{
  id: number;
  codigo: string;
  departamento: DepartamentoModel;
  descripcion: string;
}
