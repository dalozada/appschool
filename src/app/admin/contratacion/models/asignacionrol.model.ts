export interface AsignacionRolModel{
  fechaInicio: Date,
  fechaFin:Date,
  idestado:number,
  idrol: number,
  idpersona: number
}
