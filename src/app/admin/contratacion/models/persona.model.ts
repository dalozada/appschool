import { CiudadModel } from './ciudad.model';
import { TipoIdentificacionModel } from './tipoIdentificacion.model';

export interface PersonaModel{

    identificacion: string;
    nombre1: string;
    nombre2: string;
    apellido1:string;
    apellido2:string;
    direccion: string;
    idciudadNac ?:number;
    idciudad ?:number;
    idtipoIdentificacion?: number;
    telefonoFijo:string;
    celular: string;
    idciudadUbicacion ?:number;
    email: string;
    id: number;
    fechaNac: Date;
    rutaFoto:string;

    ciudadNac?:CiudadModel;
    ciudad?:CiudadModel;
    ciudadUbicacion?:CiudadModel;
    tipoIdentificacion?:TipoIdentificacionModel;
    // departamento:string



}
