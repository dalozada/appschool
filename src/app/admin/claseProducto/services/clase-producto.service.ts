import { Injectable } from '@angular/core';
import { ClaseProductoModel } from '../models/claseProducto.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ClaseProductoService {
  claseProducto: ClaseProductoModel;
  url = 'http://localhost:8080/inventario';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerClaseProducto() {
    // alert('---');
     return this.httpClient.get<ClaseProductoModel[]>(this.url + '/clasesproductos', this.httpOptions)
     .pipe(map(data => data));
  }




  crearClaseProducto(claseProducto: ClaseProductoModel ) {

    claseProducto.detalle = claseProducto.detalle.toUpperCase();
    claseProducto.descripcion = claseProducto.descripcion.toUpperCase();
    return this.httpClient.post<ClaseProductoModel>(this.url + '/guardarclaseproducto',claseProducto);
  }


  eliminarClaseProducto(claseProductoId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.post(this.url + '/eliminarclaseproducto',claseProductoId, this.httpOptions);
  }

  actualizarClaseProducto(claseProducto: ClaseProductoModel) {
    claseProducto.detalle = claseProducto.detalle.toUpperCase();
    claseProducto.descripcion = claseProducto.descripcion.toUpperCase();
    return this.httpClient.post(this.url + '/actualizarclaseproducto',claseProducto, this.httpOptions);
  }
}
