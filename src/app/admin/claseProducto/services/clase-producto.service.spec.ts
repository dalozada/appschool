import { TestBed } from '@angular/core/testing';

import { ClaseProductoService } from './clase-producto.service';

describe('ClaseProductoService', () => {
  let service: ClaseProductoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ClaseProductoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
