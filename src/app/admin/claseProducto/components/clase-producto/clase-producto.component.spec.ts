import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaseProductoComponent } from './clase-producto.component';

describe('ClaseProductoComponent', () => {
  let component: ClaseProductoComponent;
  let fixture: ComponentFixture<ClaseProductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaseProductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaseProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
