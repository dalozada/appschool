import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ClaseProductoModel } from '../../models/claseProducto.model';
import { ClaseProductoService } from '../../services/clase-producto.service';
import { debounceTime } from 'rxjs/operators';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-clase-producto',
  templateUrl: './clase-producto.component.html',
  styleUrls: ['./clase-producto.component.css']
})
export class ClaseProductoComponent implements OnInit {

  @ViewChild('template') private templatePr: TemplateRef<any>;
  formClase: FormGroup;
  modalRef: BsModalRef;
  claseProducto: ClaseProductoModel;
  objClase: ClaseProductoModel[] = [];
  alerts: any[] = [];
  claseIdUpdate: number;
  constructor(
    private claseProductoService: ClaseProductoService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
  ) {
    this.claseProducto = {
      id: null,
      detalle: '',
      descripcion: '',
    };
    this.buildForm();
  }

  ngOnInit(): void {
    this.claseProductoService.traerClaseProducto()
      .subscribe(clase => {
        this.objClase = clase;
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
        });
  }
  get nameClaseField() {
    return this.formClase.get('detalle');
  }
  get descripcionClase() {
    return this.formClase.get('descripcion');
  }
  openModalClase() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
  closeModal() {
    this.formClase.reset();
    this.modalRef.hide();

  }
  openModalClaseUpdate(clase: ClaseProductoModel) {
    this.claseProducto = {
      id: clase.id,
      detalle: clase.detalle.toUpperCase(),
      descripcion: clase.descripcion,
    };
    this.claseIdUpdate = clase.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formClase = this.formBuilder.group({
      id: [0],
      detalle: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
    });
    this.formClase.valueChanges
      .pipe(
        debounceTime(350),
      )
      .subscribe(data => {
      });
  }

  guardarTipoContrato(clase: ClaseProductoModel) {

    // const pos = this.objClase.indexOf(clase);
    const pos = this.objClase.indexOf(this.objClase.find( clase =>clase.id === this.claseIdUpdate));

    if (this.formClase.valid) {
      if (this.claseProducto.id === null) {

        this.claseProductoService.crearClaseProducto(this.formClase.value)
          .subscribe(newClase => {
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! El registro fue creado con éxito.',
              timeout: 5000,
              msgStr: 'Proceso Exitoso!'
            });
            this.formClase.reset();
            this.objClase.push(newClase);
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser creado.',
                timeout: 5000,
                msgStr: 'Ups!'
              });
            });

      } else {
        this.claseProductoService.actualizarClaseProducto(this.claseProducto)
          .subscribe(claseUpdate => {
            this.objClase[pos] = <ClaseProductoModel>claseUpdate;
            this.alerts.push({
              type: 'success',
              msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
              timeout: 2000,
              msgStr: 'Proceso Exitoso!'
            });
            this.closeModal();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: '¡Ups! El registro no puede ser actualizado',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }



}
