import { Component, OnInit, Output, Input, EventEmitter, ViewChild } from '@angular/core';
import { ClaseProductoModel } from '../../models/claseProducto.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ClaseProductoService } from '../../services/clase-producto.service';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-list-clase-producto',
  templateUrl: './list-clase-producto.component.html',
  styleUrls: ['./list-clase-producto.component.css']
})
export class ListClaseProductoComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<ClaseProductoModel>();
  @Input() claseProducto: ClaseProductoModel[] = [];
  claseIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private claseProductoService: ClaseProductoService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(claseId: number) {
    this.dangerModal.show();
    this.claseIdDelete = claseId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(claseProducto: ClaseProductoModel) {
    this.openModalUpdate.emit(claseProducto);
  }

  onDeleteClase() {

    //  const pos = this.claseProducto.indexOf(claseProducto);
    //  this.claseProductoService.eliminarClaseProducto(claseProducto.id)
    const pos = this.claseProducto.indexOf(this.claseProducto.find(clase => clase.id === this.claseIdDelete));
    this.claseProductoService.eliminarClaseProducto(this.claseIdDelete)
      .subscribe(rta => {
        this.alerts.push({
          type: 'success',
          msg: '¡Proceso exitoso! El registro fue eliminado con éxito.',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.claseProducto.splice(pos, 1);
        this.dangerModal.hide();
      },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: '¡Ups! El registro no puede ser eliminado.',
            timeout: 3000,
            msgStr: 'Ups!'
          });
        });
  }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
