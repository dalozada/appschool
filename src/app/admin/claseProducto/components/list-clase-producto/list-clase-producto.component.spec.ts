import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListClaseProductoComponent } from './list-clase-producto.component';

describe('ListClaseProductoComponent', () => {
  let component: ListClaseProductoComponent;
  let fixture: ComponentFixture<ListClaseProductoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListClaseProductoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListClaseProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
