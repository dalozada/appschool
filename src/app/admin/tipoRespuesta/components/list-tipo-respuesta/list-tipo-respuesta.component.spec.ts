import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListTipoRespuestaComponent } from './list-tipo-respuesta.component';

describe('ListTipoRespuestaComponent', () => {
  let component: ListTipoRespuestaComponent;
  let fixture: ComponentFixture<ListTipoRespuestaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListTipoRespuestaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListTipoRespuestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
