import { Component, OnInit, Output, Input,EventEmitter, ViewChild } from '@angular/core';
import { TipoRespuestaModel } from '../../models/tipoRespuesta.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TipoRespuestaService } from '../../services/tipo-respuesta.service';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-list-tipo-respuesta',
  templateUrl: './list-tipo-respuesta.component.html',
  styleUrls: ['./list-tipo-respuesta.component.css']
})
export class ListTipoRespuestaComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<TipoRespuestaModel>();
  @Input() tipoRespuesta: TipoRespuestaModel[] = [];
  tipoIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private tipoRespuestaService: TipoRespuestaService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(tipoId:number) {
    this.dangerModal.show();
   this.tipoIdDelete = tipoId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(tipoRespuesta:TipoRespuestaModel ) {
    this.openModalUpdate.emit(tipoRespuesta);
  }

  onDeleteTipo() {

    //  const pos = this.tipoRespuesta.indexOf(tipoRespuesta);
    const pos = this.tipoRespuesta.indexOf(this.tipoRespuesta.find( tipo => tipo.id === this.tipoIdDelete));

     this.tipoRespuestaService.eliminarTipoRespuesta(this.tipoIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: '¡Proceso exitoso! El registro fue eliminado con éxito',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.tipoRespuesta.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El tipo Respuesta no puede ser eliminada',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
