import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { TipoRespuestaModel } from '../../models/tipoRespuesta.model';
import { TipoRespuestaService } from '../../services/tipo-respuesta.service';
import { debounceTime } from 'rxjs/operators';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-tipo-respuesta',
  templateUrl: './tipo-respuesta.component.html',
  styleUrls: ['./tipo-respuesta.component.css']
})
export class TipoRespuestaComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formTipo: FormGroup;
  modalRef: BsModalRef;
  tipoRespuesta: TipoRespuestaModel;
  objTipo: TipoRespuestaModel[] = [];
  alerts: any[] = [];
  tipoIdUpdate: number;
  constructor(
    private tipoRespuestaService: TipoRespuestaService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.tipoRespuesta = {
        id: null,
        tipoRespuesta: '',
        descripcion: '',
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.tipoRespuestaService.traerTipoRespuesta()
    .subscribe(tipo => {
      this.objTipo = tipo;
    },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'Error de conexion',
            timeout: 5000,
            msgStr: 'Ups!'
          });
      });


  }
  get nameTipoField() {
    return this.formTipo.get('tipoRespuesta');
  }
  get descripcionTipo() {
    return this.formTipo.get('descripcion');
  }
  openModalTipo() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.formTipo.reset();
  this.modalRef.hide();

}
  openModalTipoUpdate(tipo: TipoRespuestaModel) {
     this.tipoRespuesta = {
      id: tipo.id,
      tipoRespuesta: tipo.tipoRespuesta.toUpperCase(),
      descripcion: tipo.descripcion,
    };
    this.tipoIdUpdate = tipo.id;

    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formTipo = this.formBuilder.group({
      id: [0],
      tipoRespuesta: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
    });
    this.formTipo.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarTipoRespuesta(tipo: TipoRespuestaModel) {

    // const pos = this.objTipo.indexOf(tipo);
    const pos = this.objTipo.indexOf(this.objTipo.find( tip =>tip.id === this.tipoIdUpdate));
    if (this.formTipo.valid) {
      if (this.tipoRespuesta.id === null ) {

        this.tipoRespuestaService.crearTipoRespuesta(this.formTipo.value)
       .subscribe( newClase => {
        this.alerts.push({
          type: 'success',
          msg: '¡Proceso exitoso! El registro fue creado con éxito',
          timeout: 5000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formTipo.reset();
        this.objTipo.push(newClase);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 5000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.tipoRespuestaService.actualizarTipoRespuesta(this.tipoRespuesta)
         .subscribe( claseUpdate => {
          this.objTipo[ pos ] = <TipoRespuestaModel>claseUpdate ;
           this.alerts.push({
            type: 'success',
            msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
            timeout: 2000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();

        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualizacion no pudo completarse',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
