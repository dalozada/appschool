import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoRespuestaComponent } from './tipo-respuesta.component';

describe('TipoRespuestaComponent', () => {
  let component: TipoRespuestaComponent;
  let fixture: ComponentFixture<TipoRespuestaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoRespuestaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoRespuestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
