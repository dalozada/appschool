import { Injectable } from '@angular/core';
import { TipoRespuestaModel } from '../models/tipoRespuesta.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TipoRespuestaService {

  tipoRespuesta: TipoRespuestaModel;
  url = 'http://localhost:8080';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerTipoRespuesta() {
    // alert('---');
     return this.httpClient.get<TipoRespuestaModel[]>(this.url + '/tipo_respuestas', this.httpOptions)
     .pipe(map(data => data));
  }




  crearTipoRespuesta(tipoRespuesta: TipoRespuestaModel ) {

    tipoRespuesta.tipoRespuesta = tipoRespuesta.tipoRespuesta.toUpperCase();
    tipoRespuesta.descripcion = tipoRespuesta.descripcion.toUpperCase();
    return this.httpClient.post<TipoRespuestaModel>(this.url + '/tipo_respuesta',tipoRespuesta);
  }


  eliminarTipoRespuesta(tipoRespuetaId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.delete(this.url + '/tipo_respuesta/'+tipoRespuetaId, this.httpOptions);
  }

  actualizarTipoRespuesta(tipoRespuesta: TipoRespuestaModel) {
    tipoRespuesta.tipoRespuesta = tipoRespuesta.tipoRespuesta.toUpperCase();
    tipoRespuesta.descripcion = tipoRespuesta.descripcion.toUpperCase();
    return this.httpClient.put(this.url + '/tipo_respuesta/'+tipoRespuesta.id,tipoRespuesta, this.httpOptions);
  }



}
