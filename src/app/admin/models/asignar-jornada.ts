import {ProgramaModel} from './programa.model';
import {EstadoModel} from './estado.model';
import {JornadaModel} from './jornada.model';
export interface AsignarJornadaModel {
  id: number;
  idprograma ?: number;
  idjornada ?: number;
  idestado ?: number;
  numeroHoras ?: number;
  descripcion ?: string;

  programa: ProgramaModel;
  jornada: JornadaModel;
  estado: EstadoModel;
  

}
