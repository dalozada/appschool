export interface PersonaModel {
    id: number;
    identificacion: string;
    nombre1: string;
    nombre2: string;
    apellido1: string;
    apellido2: string;
    rutaFoto: string;
}
