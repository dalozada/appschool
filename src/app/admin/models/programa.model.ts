import { EstadoModel } from './../models/estado.model';
import { TipoProgramaModel } from './../models/tipo-programa.model';

export interface ProgramaModel {
  id: number;
  nombrePrograma: string;
  descripcionPrograma: string;
  codigoPrograma: string;
  idtipoPrograma ?: number;
  idestado ?: number;

  estado ?: EstadoModel;
  tipoPrograma ?: TipoProgramaModel;
}
