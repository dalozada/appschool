import {JornadaModel} from './jornada.model';
import {ProgramaModel} from './programa.model';
export interface AsignarJornadaProgramaModel {
  id: number;
  idprograma ?: number;
  idjornada ?: number;

  jonada ?: JornadaModel;
  programa ?: ProgramaModel;
}
