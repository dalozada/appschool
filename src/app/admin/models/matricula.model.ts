import { ProgramaModel } from './programa.model';
import { PersonaModel } from './persona.model';
import { AcudienteModel } from './acudiente.model';
export interface MatriculaModel {
  id: number;
  fecha: string;
  idacudiente ?: number;
  idpersona ?: number;
  idprograma ?: number;

  acudiente ?: AcudienteModel;
  persona ?: PersonaModel;
  programa ?: ProgramaModel;
}
