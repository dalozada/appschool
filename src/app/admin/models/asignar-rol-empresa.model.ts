export interface AsignarRolEmpresaModel {
  id: number;
  idrol: number;
  fechaRegistro: string;
  idestado: number;
  idempresa: number;
}
