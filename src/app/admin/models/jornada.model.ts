export interface JornadaModel {
  id: number;
  nombreJornada: string;
  horaInicial: string;
  horaFinal: string;
  numeroHoras: number;
  descripcion: string;
}
