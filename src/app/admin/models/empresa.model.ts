export interface EmpresaModel {
  id: number;
  razonSocial: string;
  nit: string;
  digitoVerificacion: number;
  representanteLegal: string;
  rutaLogo: string;
}
