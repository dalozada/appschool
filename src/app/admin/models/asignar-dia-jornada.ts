import {JornadaModel} from './jornada.model';
import { DiaModel } from './dia.model';

export interface AsignarDiaJornadaModel {
  id: number;
  iddia ?: number;
  idjornada ?: number;

  dia ?: DiaModel;
  jornada ?: JornadaModel;
}
