import { Injectable } from '@angular/core';
import {TipoPagoModel} from './../models/tipo-pago.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class TipoPagoService {

  tipoPagoModel: TipoPagoModel;
  url = 'http://localhost:8080/inventario';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerTipoPagos() {
     return this.httpClient.get<TipoPagoModel[]>(this.url + '/tipopago', this.httpOptions)
     .pipe(map(data => data));
  }

  crearTipoPago(tPago: TipoPagoModel ) {

    return this.httpClient.post<TipoPagoModel>(this.url + '/guardartipopago', tPago);
  }


  eliminarTipoPago(tPagoId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.post(this.url + '/eliminartipopago', tPagoId, this.httpOptions);
  }
  actualizarTipoPago(tPago: TipoPagoModel) {
    return this.httpClient.post(this.url + '/actualizartipopago', tPago, this.httpOptions);
  }
}
