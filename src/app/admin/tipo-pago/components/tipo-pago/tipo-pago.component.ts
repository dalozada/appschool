import { Component, OnInit, Output, EventEmitter, Input, ViewChild, TemplateRef } from '@angular/core';
import { TipoPagoModel } from '../../models/tipo-pago.model';
import {TipoPagoService} from './../../services/tipo-pago.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';
@Component({
  selector: 'app-tipo-pago',
  templateUrl: './tipo-pago.component.html',
  styleUrls: ['./tipo-pago.component.css']
})
export class TipoPagoComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formTpago: FormGroup;
  modalRef: BsModalRef;
  tpago: TipoPagoModel;
  objTpagos: TipoPagoModel[] = [];
  alerts: any[] = [];
  tPagoIdUpdate: number;
  constructor(
    private tipoPagoService: TipoPagoService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.tpago = {
        id: null,
        detalleTipoPago: '',
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.tipoPagoService.traerTipoPagos()
    .subscribe(tpagos => {
      this.objTpagos = tpagos;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexión',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });
  }
  get detalleTipoPagoField() {
    return this.formTpago.get('detalleTipoPago');
  }
  openModalTipoPago() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.formTpago.reset();
  this.modalRef.hide();

}
  openModalTipoPagoUpdate(tPagoObj: TipoPagoModel) {
     this.tpago = {
      id: tPagoObj.id,
      detalleTipoPago: tPagoObj.detalleTipoPago.toUpperCase(),
    };
    this.tPagoIdUpdate = tPagoObj.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formTpago = this.formBuilder.group({
      id: [0],
      detalleTipoPago: ['', [Validators.required]],
    });
    this.formTpago.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarTipoPago() {
    this.formTpago.value.detalleTipoPago = this.formTpago.value.detalleTipoPago.toUpperCase();
    const pos = this.objTpagos.indexOf(this.objTpagos.find( men => men.id === this.tPagoIdUpdate));
    if (this.formTpago.valid) {
      if (this.tpago.id === null ) {

        this.tipoPagoService.crearTipoPago(this.formTpago.value)
       .subscribe( newTpago => {
        this.alerts.push({
          type: 'success',
          msg: 'El registro fue creado con éxito.',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formTpago.reset();
        this.objTpagos.push(newTpago);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.tipoPagoService.actualizarTipoPago(this.tpago)
         .subscribe( tPagoUpdate => {
          this.objTpagos[ pos ] = <TipoPagoModel>tPagoUpdate ;
           this.alerts.push({
            type: 'success',
            msg: 'La actualización se ha realizado con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualización no pudo completarce',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
