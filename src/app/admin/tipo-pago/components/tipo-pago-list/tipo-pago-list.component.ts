import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { TipoPagoModel } from '../../models/tipo-pago.model';
import {TipoPagoService} from './../../services/tipo-pago.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';
@Component({
  selector: 'app-tipo-pago-list',
  templateUrl: './tipo-pago-list.component.html',
  styleUrls: ['./tipo-pago-list.component.css']
})
export class TipoPagoListComponent implements OnInit {

  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<TipoPagoModel>();
  @Input() tPagos: TipoPagoModel[] = [];
  tPagoIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private tipoPagoService: TipoPagoService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(tPagoId: number) {
    this.dangerModal.show();
    this.tPagoIdDelete = tPagoId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(tPago: TipoPagoModel) {
    this.openModalUpdate.emit(tPago);
  }

  onDeleteTipoPago() {

     const pos = this.tPagos.indexOf(this.tPagos.find( tp => tp.id === this.tPagoIdDelete));
     this.tipoPagoService.eliminarTipoPago(this.tPagoIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El registro fué eliminado exitosamente',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.tPagos.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El registro no puede ser eliminado',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
