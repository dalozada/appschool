import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoPagoListComponent } from './tipo-pago-list.component';

describe('TipoPagoListComponent', () => {
  let component: TipoPagoListComponent;
  let fixture: ComponentFixture<TipoPagoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoPagoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoPagoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
