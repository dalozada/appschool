import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { MenuModel } from '../../models/menu.model';
import { MenuService} from './../../services/menu.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import {ModalDirective} from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.css']
})
export class MenuListComponent implements OnInit {
  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<MenuModel>();
  @Input() menus: MenuModel[] = [];
  menuIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private menuService: MenuService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {

  }
  openModalDelete(menuId: number) {
    this.dangerModal.show();
    this.menuIdDelete = menuId;
  }
  openModalCrear() {
    this.openModalNew.emit();
  }
  openModalActualizar(menu: MenuModel) {
    this.openModalUpdate.emit(menu);
  }

  onDeleteMenu() {

     const pos = this.menus.indexOf(this.menus.find( menu => menu.id === this.menuIdDelete));
     this.menuService.eliminarMenu(this.menuIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El menú fué eliminado exitosamente',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.menus.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El menú no puede ser eliminado',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
