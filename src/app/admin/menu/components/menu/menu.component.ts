import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MenuModel } from './../../models/menu.model';
import { MenuService } from './../../services/menu.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formMenu: FormGroup;
  modalRef: BsModalRef;
  menu: MenuModel;
  objMenus: MenuModel[] = [];
  alerts: any[] = [];
  menuIdUpdate: number;
  constructor(
    private menuService: MenuService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
    ) {
      this.menu = {
        id: null,
        nombreMenu: '',
      };
      this.buildForm();
    }

  ngOnInit(): void {
    this.menuService.traerMenus()
    .subscribe(menus => {
      this.objMenus = menus;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexión',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });
  }
  get nameMenuField() {
    return this.formMenu.get('nombreMenu');
  }
  openModalMenu() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.formMenu.reset();
  this.modalRef.hide();

}
  openModalMenuUpdate(menuObj: MenuModel) {
     this.menu = {
      id: menuObj.id,
      nombreMenu: menuObj.nombreMenu.toUpperCase(),
    };
    this.menuIdUpdate = menuObj.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formMenu = this.formBuilder.group({
      id: [0],
      nombreMenu: ['', [Validators.required]],
    });
    this.formMenu.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  guardarMenu(menu: MenuModel) {

    const pos = this.objMenus.indexOf(this.objMenus.find( men => men.id === this.menuIdUpdate));
    if (this.formMenu.valid) {
      if (this.menu.id === null ) {

        this.menuService.crearMenu(this.formMenu.value)
       .subscribe( newMenu => {
        this.alerts.push({
          type: 'success',
          msg: 'El menú fué creado exitosamente',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formMenu.reset();
        console.log(newMenu);
        this.objMenus.push(newMenu);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error al guardar la información',
          timeout: 2000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.menuService.actualizarMenu(this.menu)
         .subscribe( menuUpdate => {
          this.objMenus[ pos ] = <MenuModel>menuUpdate ;
           this.alerts.push({
            type: 'success',
            msg: 'La actualización se realizó con éxito',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: 'La actualización no pudo completarce',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
