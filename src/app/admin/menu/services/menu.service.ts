import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MenuModel } from '../models/menu.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})



export class MenuService {
  menu: MenuModel;
  url = 'http://localhost:8080/virtualt';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerMenus() {
     return this.httpClient.get<MenuModel[]>(this.url + '/menus', this.httpOptions)
     .pipe(map(data => data));
  }

  public slider() {
    return this.httpClient.get(this.url + '/slider', this.httpOptions)
    .pipe(map(data => data));
  }

  public menusByRolUsuario(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traermenususuario', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public menusByRol(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/traermenus', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  public info(idRol: number) {
    const idrol = idRol;
    this.permisos = idRol;
    return this.httpClient.post(this.url + '/permisos', idrol, this.httpOptions)
    .pipe(map(data => data));
  }

  crearMenu(menu: MenuModel ) {

    menu.nombreMenu = menu.nombreMenu.toUpperCase();
    return this.httpClient.post<MenuModel>(this.url + '/guardarmenu', menu);
  }


  eliminarMenu(menuId: number) {
     
    return this.httpClient.post(this.url + '/eliminarmenu', menuId, this.httpOptions);
  }
  actualizarMenu(menu: MenuModel) {
    menu.nombreMenu = menu.nombreMenu.toUpperCase();
    return this.httpClient.post(this.url + '/actualizarmenu', menu, this.httpOptions);
  }
}
