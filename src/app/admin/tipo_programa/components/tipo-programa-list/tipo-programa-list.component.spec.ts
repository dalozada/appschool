import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipoProgramaListComponent } from './tipo-programa-list.component';

describe('TipoProgramaListComponent', () => {
  let component: TipoProgramaListComponent;
  let fixture: ComponentFixture<TipoProgramaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipoProgramaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipoProgramaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
