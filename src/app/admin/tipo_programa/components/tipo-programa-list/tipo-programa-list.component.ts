import { Component, OnInit, Output, Input, ViewChild, EventEmitter } from '@angular/core';
import { TipoProgramaModel } from '../../models/tipo_programa.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TipoProgramaService } from '../../services/tipo-programa.service';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';
import { TipoProgramaComponent } from '../tipo-programa/tipo-programa.component';

@Component({
  selector: 'app-tipo-programa-list',
  templateUrl: './tipo-programa-list.component.html',
  styleUrls: ['./tipo-programa-list.component.css']
})
export class TipoProgramaListComponent implements OnInit {
  @ViewChild(TipoProgramaComponent) formTp: TipoProgramaComponent;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  objTipos: TipoProgramaModel[] = [];
  tipoProgramaIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  constructor(
    private tiposService: TipoProgramaService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {
    this.tiposService.traerTipo()
    .subscribe(tipos => {
      this.objTipos = tipos;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexion',
          timeout: 5000,
          msgStr: 'Ups!'
        });
      });
  }
  openModalDelete(tipoProgramaId: number) {
    this.dangerModal.show();
    this.tipoProgramaIdDelete = tipoProgramaId;
  }
  openModalCrear() {
    this.formTp.templateTp.show();
  }
  openModalActualizar(tipo: TipoProgramaModel) {
    this.formTp.openModalTipoUpdate(tipo);
  }
  closeModal() {
    this.formTp.closeModal();
  }
  onDeleteTipo() {

    const pos = this.objTipos.indexOf(this.objTipos.find( men => men.id === this.tipoProgramaIdDelete));
    this.tiposService.eliminarTipo(this.tipoProgramaIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: 'El registro fue eliminado con éxito.',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.objTipos.splice(pos, 1);
       this.dangerModal.hide();

      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: 'El registro no puede ser eliminado.',
          timeout: 3000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
