import { Component, OnInit, ViewChild, TemplateRef, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService, ModalDirective } from 'ngx-bootstrap/modal';
import { TipoProgramaModel } from '../../models/tipo_programa.model';
import { TipoProgramaService } from '../../services/tipo-programa.service';
import { debounceTime } from 'rxjs/operators';
import { AlertComponent } from 'ngx-bootstrap/alert/alert.component';

@Component({
  selector: 'app-tipo-programa',
  templateUrl: './tipo-programa.component.html',
  styleUrls: ['./tipo-programa.component.css']
})
export class TipoProgramaComponent implements OnInit {
  @ViewChild('formtemplate') public templateTp: ModalDirective;
  @Input() tipos: TipoProgramaModel[] = [];
  formTipoPrograma: FormGroup;
  modalRef: BsModalRef;
  tipo: TipoProgramaModel;
  alerts: any[] = [];
  tipo_programaIdUpdate: number;

  constructor(
    private tiposService: TipoProgramaService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService
  ) {
    this.tipo = {
      id: null,
      nombreTipoPrograma: '',
      descripcion: '',
    };
    this.buildForm();
  }

  ngOnInit(): void {

  }
  get nameTipoProgramaField() {
    return this.formTipoPrograma.get('nombreTipoPrograma');
  }
  get descripcionTipoProgramaField() {
    return this.formTipoPrograma.get('descripcion');
  }
  openModalTipo() {
    this.templateTp.show();
  }
  closeModal() {
    this.formTipoPrograma.reset();
    this.templateTp.hide();

  }
  openModalTipoUpdate(tipoObj: TipoProgramaModel) {
    this.tipo = {
      id: tipoObj.id,
      nombreTipoPrograma: tipoObj.nombreTipoPrograma.toUpperCase(),
      descripcion: tipoObj.descripcion.toUpperCase(),
    };
    this.tipo_programaIdUpdate = tipoObj.id;
    this.templateTp.show();
  }
  private buildForm() {

    this.formTipoPrograma = this.formBuilder.group({
      id: [0],
      nombreTipoPrograma: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
    });
    this.formTipoPrograma.valueChanges
      .pipe(
        debounceTime(350),
      )
      .subscribe(data => {
      });
  }

  guardarTipo(tipo: TipoProgramaModel) {

    const pos = this.tipos.indexOf(this.tipos.find( tip => tip.id === this.tipo_programaIdUpdate));
    if (this.formTipoPrograma.valid) {
      if (this.tipo.id === null) {

        this.tiposService.crearTipo(this.formTipoPrograma.value)
          .subscribe(newTipo => {
            this.alerts.push({
              type: 'success',
              msg: 'El registro fue creado con éxito.',
              timeout: 2000,
              msgStr: 'Proceso Exitoso!'
            });
            this.tipos.push(newTipo);
            this.formTipoPrograma.reset();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: 'Error al guardar la información',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });

      } else {
        this.tiposService.actualizarTipo(this.tipo)
          .subscribe(tipoUpdate => {
            this.tipos[pos] = <TipoProgramaModel>tipoUpdate;
            this.alerts.push({
              type: 'success',
              msg: '¡La actualización se ha realizado con éxito.',
              timeout: 1000,
              msgStr: 'Proceso Exitoso!'
            });
            this.closeModal();
          },
            error => {
              this.alerts.push({
                type: 'danger',
                msg: 'La actualización no puedo completarse',
                timeout: 2000,
                msgStr: 'Ups!'
              });
            });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }


}
