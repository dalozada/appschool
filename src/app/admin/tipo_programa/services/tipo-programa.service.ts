import { Injectable } from '@angular/core';
import { TipoProgramaModel } from '../models/tipo_programa.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TipoProgramaService {
  tipo: TipoProgramaModel;
  url = 'http://localhost:8080';
  permisos: number;
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };
  constructor(
    private httpClient: HttpClient
  ) { }

  public traerTipo() {
     return this.httpClient.get<TipoProgramaModel[]>(this.url + '/tipo_programas', this.httpOptions)
     .pipe(map(data => data));
  }

  crearTipo(tipo: TipoProgramaModel) {

    tipo.nombreTipoPrograma = tipo.nombreTipoPrograma.toUpperCase();
    tipo.descripcion = tipo.descripcion.toUpperCase();
    return this.httpClient.post<TipoProgramaModel>(this.url + '/tipo_programa', tipo);
  }


  eliminarTipo(TipoId: number) {
    return this.httpClient.delete(this.url + '/tipo_programa/' + TipoId);
  }

  actualizarTipo(tipo: TipoProgramaModel) {
    tipo.nombreTipoPrograma = tipo.nombreTipoPrograma.toUpperCase();
    return this.httpClient.put<TipoProgramaModel>(this.url + '/tipo_programa/' + tipo.id, tipo);
  }

}
