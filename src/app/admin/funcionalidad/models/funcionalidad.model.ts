import { ModuloModel } from '../../modulo/components/models/modulo.model';
import { MenuModel } from '../../menu/models/menu.model';

export interface FuncionalidadModel {

  id:number
  nombreSubmenu: string
  url:string,
  idmodulo:number,
  idmenu:number

}
