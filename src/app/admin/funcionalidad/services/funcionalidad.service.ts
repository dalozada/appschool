import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { FuncionalidadModel } from '../models/funcionalidad.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FuncionalidadService {

  url = 'http://localhost:8080/';
  funcionalidad:FuncionalidadModel;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
     'Access-Control-Allow-Origin': '*',
    })
  };

  constructor(
    private httpClient: HttpClient

  ) { }

  /**
//    * en este metodo se consule el servicio de funcionalidades,
//    * que corresponde a una lista de todas las funcionalidades registradas en el sistema
//    * las rutas que se ven (virtualt/funcionalidades) son proporcionadas por el backend
//    */
  // public traerFuncionalidad() {
  //   return this.httpClient.get(this.url + '/funcionalidades', {
  //   }).pipe(map((res: Response) => {
  //     return res
  //   }));
  // }

  public traerFuncionalidad() {
    return this.httpClient.get<FuncionalidadModel[]>(this.url + '/virtualt/funcionalidades', this.httpOptions)
    .pipe(map(data => data));
 }

  crearFuncionalidad(funcModel: FuncionalidadModel ) {

    funcModel.nombreSubmenu = funcModel.nombreSubmenu.toUpperCase();
    funcModel.url = funcModel.url;
    funcModel.idmenu = funcModel.idmenu;
    funcModel.idmodulo =funcModel.idmodulo;
    return this.httpClient.post<FuncionalidadModel>(this.url + '/funcionalidad', funcModel);
  }


  eliminarFuncionalidad(menuId: number) {
    // const url = `${this.path}/${todoId}`;
    return this.httpClient.post(this.url + '/virtualt/eliminarfuncionalidad', menuId, this.httpOptions);
  }

  actualizarFuncionalidad(funcModel: FuncionalidadModel) {
    funcModel.nombreSubmenu = funcModel.nombreSubmenu.toUpperCase();
    funcModel.url = funcModel.url;
    funcModel.idmenu =funcModel.idmenu;
    funcModel.idmodulo = funcModel.idmodulo;
    return this.httpClient.put(this.url + '/funcionalidad/'+funcModel.id, funcModel, this.httpOptions);
  }

}
