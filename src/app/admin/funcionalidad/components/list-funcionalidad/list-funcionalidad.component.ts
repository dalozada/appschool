import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { FuncionalidadModel } from '../../models/funcionalidad.model';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FuncionalidadService } from '../../services/funcionalidad.service';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';

@Component({
  selector: 'app-list-funcionalidad',
  templateUrl: './list-funcionalidad.component.html',
  styleUrls: ['./list-funcionalidad.component.css']
})
export class ListFuncionalidadComponent implements OnInit {

  @Output() openModalNew = new EventEmitter();
  @Output() openModalUpdate = new EventEmitter<FuncionalidadModel>();
  @Input() funcionalidad: FuncionalidadModel[] = [];
  funcIdDelete: number;
  pageActual: number = 1;
  numReg: number;
  alerts: any[] = [];
  dismissible = true;
  @ViewChild('dangerModal') public dangerModal: ModalDirective;
  constructor(
    private funcService: FuncionalidadService,
  ) {
    this.enviarNumeroRegistros(10);
  }

  ngOnInit(): void {
  }
  openModalDelete(funcId: number) {
    this.dangerModal.show();
    this.funcIdDelete = funcId;
  }
  openModalCrear() {
    console.log(this.funcionalidad)

    this.openModalNew.emit();
  }
  openModalActualizar(func: FuncionalidadModel) {
    console.log(func);
    func.idmenu=func['menu']['id'];
    func.idmodulo=func['modulo']['id'];
    this.openModalUpdate.emit(func);
  }

  onDeleteFunc() {

     const pos = this.funcionalidad.indexOf(this.funcionalidad.find( func => func.id === this.funcIdDelete));
     this.funcService.eliminarFuncionalidad(this.funcIdDelete)
     .subscribe(rta => {
      this.alerts.push({
        type: 'success',
        msg: '¡Proceso exitoso! El registro fue eliminado con éxito.',
        timeout: 2000,
        msgStr: 'Proceso Exitoso!'
      });
       this.funcionalidad.splice(pos, 1);
       this.dangerModal.hide();
      },
       error => {
        this.alerts.push({
          type: 'danger',
          msg: '¡Ups! El registro no puede ser eliminado.',
          timeout: 2000,
          msgStr: 'Ups!'
        });
       });
      }

  enviarNumeroRegistros(num: number) {
    this.numReg = num;
  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }
}
