import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListFuncionalidadComponent } from './list-funcionalidad.component';

describe('ListFuncionalidadComponent', () => {
  let component: ListFuncionalidadComponent;
  let fixture: ComponentFixture<ListFuncionalidadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListFuncionalidadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListFuncionalidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
