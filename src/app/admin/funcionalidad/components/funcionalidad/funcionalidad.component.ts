import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FuncionalidadModel } from '../../models/funcionalidad.model';
import { FuncionalidadService } from '../../services/funcionalidad.service';
import { debounceTime } from 'rxjs/operators';
import { AlertComponent } from 'ngx-bootstrap/alert/public_api';
import { MenuModel } from '../../../menu/models/menu.model';
import { ModuloModel } from '../../../modulo/components/models/modulo.model';
import { MenuService } from '../../../menu/services/menu.service';
import { ModuloService } from '../../../modulo/services/modulo.service';


@Component({
  selector: 'app-funcionalidad',
  templateUrl: './funcionalidad.component.html',
  styleUrls: ['./funcionalidad.component.css']
})
export class FuncionalidadComponent implements OnInit {
  @ViewChild('template') private templatePr: TemplateRef<any>;
  formFunc: FormGroup;
  modalRef: BsModalRef;
  funcionalidad: FuncionalidadModel;
  objFunc: FuncionalidadModel[] = [];
  objMenu: MenuModel[] = [];
  objMod: ModuloModel[] = [];
  alerts: any[] = [];
  funcIdUpdate: number;
  constructor(
    private menuService: MenuService,
    private formBuilder: FormBuilder,
    private modalService: BsModalService,
    private funcService: FuncionalidadService,
    private moduloService: ModuloService,

    ) {
      this.funcionalidad = {
        id: null,
        nombreSubmenu: '',
        url: '',
        idmenu: null,
        idmodulo:null,
      };
      this.buildForm();
    }

  ngOnInit(): void {

    this.traerMenus();
    this.traerModulos();
    this.traerFuncionalidad();
  }
  get nameSubmenuField() {
    return this.formFunc.get('nombreSubmenu');
  }
  get urlField() {
    return this.formFunc.get('url');
  }
  get menuField() {
    return this.formFunc.get('idmenu');
  }
  get moduloField() {
    return this.formFunc.get('idmodulo');
  }
  openModalFunc() {
    this.modalRef = this.modalService.show(this.templatePr);
  }
closeModal() {
  this.formFunc.reset();
  this.modalRef.hide();

}

traerFuncionalidad(){
  this.funcService.traerFuncionalidad()
    .subscribe(func => {
      this.objFunc = func;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexion',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });
}
  openModalFuncUpdate(funcObj: FuncionalidadModel) {
     this.funcionalidad = {
      id: funcObj.id,
      nombreSubmenu: funcObj.nombreSubmenu.toUpperCase(),
      url: funcObj.url,
      idmenu: funcObj.idmenu,
      idmodulo: funcObj.idmodulo,
    };
    this.funcIdUpdate = funcObj.id;
    this.modalRef = this.modalService.show(this.templatePr);
  }
  private buildForm() {

    this.formFunc = this.formBuilder.group({
      id: [0],
      nombreSubmenu: ['', [Validators.required]],
      url: ['', [Validators.required]],
      idmenu: ['', [Validators.required]],
      idmodulo: ['', [Validators.required]],
    });
    this.formFunc.valueChanges
    .pipe(
      debounceTime(350),
    )
    .subscribe(data => {
    });
  }

  traerModulos(){
    this.moduloService.traerModulo()
    .subscribe(mod => {
      this.objMod = mod;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexion',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });
  }

  traerMenus(){
    this.menuService.traerMenus()
    .subscribe(menu => {
      this.objMenu = menu;
    },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: 'Error de conexion',
          timeout: 5000,
          msgStr: 'Ups!'
        });
    });

  }

  guardarFuncionalidad(funcModel: FuncionalidadModel) {

    const pos = this.objFunc.indexOf(this.objFunc.find( func => func.id === this.funcIdUpdate));
    if (this.formFunc.valid) {
      if (this.funcionalidad.id === null ) {

        this.funcService.crearFuncionalidad(this.formFunc.value)
       .subscribe( newFunc => {
        this.alerts.push({
          type: 'success',
          msg: '¡Proceso exitoso! El registro fue creado con éxito.',
          timeout: 2000,
          msgStr: 'Proceso Exitoso!'
        });
        this.formFunc.reset();
        this.traerFuncionalidad();
        // this.objFunc.push(newFunc);
      },
      error => {
        this.alerts.push({
          type: 'danger',
          msg: '¡Ups! El registro no puede ser creado.',
          timeout: 2000,
          msgStr: 'Ups!'
        });
      });

      } else {
         this.funcService.actualizarFuncionalidad(this.funcionalidad)
         .subscribe( funcUpdate => {
           this.traerFuncionalidad();
          // this.objFunc[ pos ] = <FuncionalidadModel>funcUpdate ;
           this.alerts.push({
            type: 'success',
            msg: '¡Proceso exitoso! La actualización se ha realizado con éxito.',
            timeout: 1000,
            msgStr: 'Proceso Exitoso!'
          });
          this.closeModal();
        },
        error => {
          this.alerts.push({
            type: 'danger',
            msg: '¡Ups! El registro no puede ser actualizado.',
            timeout: 2000,
            msgStr: 'Ups!'
          });
        });
      }
    }

  }
  onClosed(dismissedAlert: AlertComponent): void {
    this.alerts = this.alerts.filter(alert => alert !== dismissedAlert);
  }

}
