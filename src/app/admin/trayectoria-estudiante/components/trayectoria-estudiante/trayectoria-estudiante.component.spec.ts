import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrayectoriaEstudianteComponent } from './trayectoria-estudiante.component';

describe('TrayectoriaEstudianteComponent', () => {
  let component: TrayectoriaEstudianteComponent;
  let fixture: ComponentFixture<TrayectoriaEstudianteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrayectoriaEstudianteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrayectoriaEstudianteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
