import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PerfectScrollbarModule, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts';
import { AlertModule } from 'ngx-bootstrap/alert';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import {  NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import {NgxPaginationModule} from 'ngx-pagination';
import { ModalComponent } from '../bootstrap/modal/modal.component';

import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};
@NgModule({
  declarations: [ModalComponent],
  imports: [
    CommonModule,
    PerfectScrollbarModule,
    TabsModule,
    ChartsModule,
    AlertModule,
    TabsModule.forRoot(),
    ChartsModule,
    AlertModule.forRoot(),
    BsDropdownModule.forRoot(),
    NgbModule,
    ModalModule.forRoot(),
    NgxPaginationModule,
  ],
  exports: [
    PerfectScrollbarModule,
    TabsModule,
    ChartsModule,
    AlertModule,
    TabsModule,
    ChartsModule,
    AlertModule,
    BsDropdownModule,
    NgbModule,
    ModalModule,
    NgxPaginationModule,
    ModalComponent
  ]
})
export class BootstrapModule { }
