import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Inicio',
    url: 'principal',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    title: true,
    name: 'Administración'
  },
  {
    name: 'menus',
    url: 'add-menu',
    icon: 'icon-drop'
  },
  {
    name: 'permisos',
    url: 'permisos',
    icon: 'icon-drop'
  },
  {
  name: 'tipo contrato',
    url: 'tipo-contrato',
    icon: 'icon-drop'
  },
  {
    name: 'tipo actividad',
    url: 'tipo-actividad',
    icon: 'icon-drop'
  },
  {
    name: 'clase producto',
    url: 'clase-producto',
    icon: 'icon-drop'
  },
  {
    name: 'tipo producto',
    url: 'tipo-producto',
    icon: 'icon-drop'
  },
  {
    name: 'tipo respuesta',
    url: 'tipo-respuesta',
    icon: 'icon-drop'
  },
  {
    name: 'periodo matricula',
    url: 'periodo-matricula',
    icon: 'icon-drop'
  },
  {
    name: 'funcionalidad',
    url: 'funcionalidad',
    icon: 'icon-drop'
  },
  {
    name: 'Medios de Pago',
    url: 'gestion-mediopago',
    icon: 'icon-drop'
  },
  {
    name: 'Clasificación School',
    url: 'gestion-clasificacion',
    icon: 'icon-drop'
  },
  {
    name: 'Gestionar Materia',
    url: 'gestion-materia',
    icon: 'icon-drop'
  },
  {
    name: 'Tipos de Pago',
    url: 'gestion-tipopago',
    icon: 'icon-drop'
  },
  {
    name: 'Infraestructura',
    url: 'gestion-infraestructura',
    icon: 'icon-drop'
  },
  {
    name: 'roles',
    url: 'add-roles',
    icon: 'icon-pencil'
  },
  {
    name: 'modulo',
    url: 'add-modulo',
    icon: 'icon-pencil'
  },
  {
      name: 'proceso',
      url: 'add-proceso',
      icon: 'icon-pencil'
  },
  {
        name: 'tipo rol',
        url: 'add-tipo_rol',
        icon: 'icon-pencil'
  },
  {
        name: 'tipo transaccion',
        url: 'add-tipo_transaccion',
        icon: 'icon-pencil'
  },
  {
    name: 'contratación',
    url: 'contratacion',
    icon: 'icon-pencil'

},
  {
    name: 'matricula',
    url: 'matricula',
    icon: 'icon-pencil'

},
  {
        name: 'documento matricula',
        url: 'add-documento_matricula',
        icon: 'icon-pencil'
  },
  {
       name: 'documento actividad',
       url: 'add-documento_actividad',
       icon: 'icon-pencil'
  },
  {
        name: 'Configuración inicial',
        url: 'add-gestion-empresa',
        icon: 'icon-pencil'
  },
  {
        name: 'tipo programa',
        url: 'add-tipo_programa',
        icon: 'icon-pencil'

  },
  {
    title: true,
    name: 'Docente'
  },
  {
    name: 'Base',
    url: 'base',
    icon: 'icon-puzzle',
    children: [
      {
        name: 'Cards',
        url: 'base/cards',
        icon: 'icon-puzzle'
      },
      {
        name: 'Carousels',
        url: 'base/carousels',
        icon: 'icon-puzzle'
      },
      {
        name: 'Collapses',
        url: 'base/collapses',
        icon: 'icon-puzzle'
      },
      {
        name: 'Forms',
        url: 'base/forms',
        icon: 'icon-puzzle'
      },
      {
        name: 'Navbars',
        url: 'base/navbars',
        icon: 'icon-puzzle'

      },
      {
        name: 'Pagination',
        url: 'base/paginations',
        icon: 'icon-puzzle'
      },
      {
        name: 'Popovers',
        url: 'base/popovers',
        icon: 'icon-puzzle'
      },
      {
        name: 'Progress',
        url: 'base/progress',
        icon: 'icon-puzzle'
      },
      {
        name: 'Switches',
        url: 'base/switches',
        icon: 'icon-puzzle'
      },
      {
        name: 'Tables',
        url: 'base/tables',
        icon: 'icon-puzzle'
      },
      {
        name: 'Tabs',
        url: 'base/tabs',
        icon: 'icon-puzzle'
      },
      {
        name: 'Tooltips',
        url: 'base/tooltips',
        icon: 'icon-puzzle'
      }
    ]
  },
  {
    name: 'Buttons',
    url: 'buttons',
    icon: 'icon-cursor',
    children: [
      {
        name: 'Buttons',
        url: 'buttons/buttons',
        icon: 'icon-cursor'
      },
      {
        name: 'Dropdowns',
        url: 'buttons/dropdowns',
        icon: 'icon-cursor'
      },
      {
        name: 'Brand Buttons',
        url: 'buttons/brand-buttons',
        icon: 'icon-cursor'
      }
    ]
  },
  {
    title: true,
    name: 'Estudiante',
  },
  {
    name: 'Charts',
    url: 'charts',
    icon: 'icon-pie-chart'
  },
  {
    name: 'Icons',
    url: 'icons',
    icon: 'icon-star',
    children: [
      {
        name: 'CoreUI Icons',
        url: 'icons/coreui-icons',
        icon: 'icon-star',
        badge: {
          variant: 'success',
          text: 'NEW'
        }
      },
      {
        name: 'Flags',
        url: 'icons/flags',
        icon: 'icon-star'
      },
      {
        name: 'Font Awesome',
        url: 'icons/font-awesome',
        icon: 'icon-star',
        badge: {
          variant: 'secondary',
          text: '4.7'
        }
      },
      {
        name: 'Simple Line Icons',
        url: 'icons/simple-line-icons',
        icon: 'icon-star'
      }
    ]
  },
  {
    name: 'Notifications',
    url: 'notifications',
    icon: 'icon-bell',
    children: [
      {
        name: 'Alerts',
        url: 'notifications/alerts',
        icon: 'icon-bell'
      },
      {
        name: 'Badges',
        url: 'notifications/badges',
        icon: 'icon-bell'
      },
      {
        name: 'Modals',
        url: 'notifications/modals',
        icon: 'icon-bell'
      }
    ]
  },
  {
    name: 'Widgets',
    url: 'widgets',
    icon: 'icon-calculator',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    divider: true
  },
  {
    title: true,
    name: 'Estudiante',
  },
  {
    name: 'Pages',
    url: 'pages',
    icon: 'icon-star',
    children: [
      {
        name: 'Login',
        url: 'login',
        icon: 'icon-star'
      },
      {
        name: 'Register',
        url: 'register',
        icon: 'icon-star'
      },
      {
        name: 'Error 404',
        url: '404',
        icon: 'icon-star'
      },
      {
        name: 'Error 500',
        url: '500',
        icon: 'icon-star'
      }
    ]
  },
  {
    title: true,
    name: 'Coordinacion',
  },
  {
    name: 'Configurar Pensum',
    url: 'gestion-pensum',
    icon: 'icon-drop'
  },
  {
    name: 'Matricula Estudiantil',
    url: 'gestion-matricula-estudiante',
    icon: 'icon-drop'
  },
  {
    name: 'Trayectoria Estudiante',
    url: 'gestion-trayectoria-estudiante',
    icon: 'icon-drop'
  },
  {
    title: true,
    name: 'Rectoria',
  },
];
